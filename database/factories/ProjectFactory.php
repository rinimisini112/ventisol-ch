<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->sentence(),
            'location' => fake()->city(),
            'company' => fake()->company(),
            'company_url' => fake()->url(),
            'content' => fake()->paragraph(),
            'images' => \json_encode([
                'gallery1.jpg',
                'gallery2.jpg',
                'gallery3.jpg',
                'gallery4.jpg',
                'gallery5.jpg',
            ]),
            'finished_at' => fake()->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
