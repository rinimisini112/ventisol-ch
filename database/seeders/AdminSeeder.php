<?php

namespace Database\Seeders;

use App\Models\VentAdmin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        VentAdmin::create([
            'name' => 'vent_admin_1',
            'email' => 'misinirini@gmail.com',
            'password' => Hash::make('ventisol951admin'),
            // Add any other fields you may have in your users table
        ]);
    }
}
