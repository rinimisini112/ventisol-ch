gsap.registerPlugin(ScrollTrigger)

document.addEventListener('DOMContentLoaded', function () {
  const horizontalScrollTitles = document.querySelectorAll(
    '.horizontal-scroll-title'
  )
  const horizontalScrollImages = document.querySelectorAll(
    '.horizontal-scroll-image'
  )
  const horizontalScrollTexts = document.querySelectorAll(
    '.horizontal-scroll-text'
  )

  // Set initial state
  horizontalScrollTitles.forEach(title =>
    gsap.set(title, { x: -100, opacity: 0 })
  )
  horizontalScrollImages.forEach(image =>
    gsap.set(image, { x: -100, opacity: 0 })
  )
  horizontalScrollTexts.forEach(text => gsap.set(text, { x: -100, opacity: 0 }))

  // Animate on scroll
  horizontalScrollTitles.forEach(title => {
    gsap.to(title, {
      x: 0,
      opacity: 1,
      duration: 0.6,
      scrollTrigger: {
        trigger: title,
        once: true,
        start: 'top 30%'
      }
    })
  })

  horizontalScrollImages.forEach(image => {
    gsap.to(image, {
      x: 0,
      opacity: 1,
      duration: 0.6,
      scrollTrigger: {
        trigger: image,
        start: 'top 50%',
        once: true
      }
    })
  })

  horizontalScrollTexts.forEach(text => {
    gsap.to(text, {
      x: 0,
      opacity: 1,
      duration: 0.7,
      scrollTrigger: {
        trigger: text,
        once: true,
        start: 'top 60%'
      }
    })
  })
  baguetteBox.run('.gallery', {
    async: false,
    buttons: true
  })

  const ventAdvantagesTitle = document.getElementById('vent-advantages-title')
  const ventContent1 = document.getElementById('vent-content-1')
  const ventContent2 = document.getElementById('vent-content-2')
  const ventBenefitsTitle = document.getElementById('vent-benefits-title')
  const airConditionerContent1 = document.getElementById(
    'conditioner-content-1'
  )
  const airConditionerContent2 = document.getElementById(
    'conditioner-content-2'
  )
  const airConditionerContent3 = document.getElementById(
    'conditioner-content-3'
  )
  const maintenanceTitle = document.getElementById('maintenance-title')
  const maintenanceImage = document.getElementById('maintenance-img')
  const maintenanceWhyTitle = document.getElementById('maintenance-why-title')
  const airConditionerDidYouTitle = document.getElementById(
    'conditioner-did-you-title'
  )
  const airTransparentImage = document.getElementById('air')
  const airConditionerAdvantagesTitle = document.getElementById(
    'conditioner-advantages-title'
  )
  const airConditionerTitleInside = document.getElementById(
    'air-conditioner-title-inside'
  )

  gsap.from(ventAdvantagesTitle, {
    opacity: 0,
    y: 50,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.1,
    scrollTrigger: {
      trigger: ventAdvantagesTitle,
      start: 'top 70%',
      once: true
    }
  })

  gsap.from(maintenanceTitle, {
    opacity: 0,
    y: 50,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.1,
    scrollTrigger: {
      trigger: maintenanceTitle,
      start: 'top 70%',
      once: true
    }
  })
  gsap.from(maintenanceImage, {
    opacity: 0,
    y: -100,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.1,
    scrollTrigger: {
      trigger: maintenanceImage,
      start: 'top 70%',
      once: true
    }
  })
  const benefitListItems = document.querySelectorAll('.benefitListItem')

  // Set up the animation for each benefitListItem
  benefitListItems.forEach((item, index) => {
    gsap.from(item, {
      opacity: 0,
      y: 50,
      duration: 1,
      scrollTrigger: {
        trigger: item,
        start: 'top 80%' // Adjust the start position as needed
      },
      stagger: 0.3 * index // Adjust the stagger amount as needed
    })
  })
  gsap.from(maintenanceWhyTitle, {
    opacity: 0,
    y: -100,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.1,
    scrollTrigger: {
      trigger: maintenanceWhyTitle,
      start: 'top 100%',
      once: true
    }
  })
  gsap.from(ventBenefitsTitle, {
    opacity: 0,
    y: 50,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.1,
    scrollTrigger: {
      trigger: ventBenefitsTitle,
      start: 'top 70%',
      once: true
    }
  })

  gsap.from(ventContent1, {
    opacity: 0,
    y: 50,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.2,
    scrollTrigger: {
      trigger: ventContent1,
      start: 'top 70%',
      once: true
    }
  })

  gsap.from(ventContent2, {
    opacity: 0,
    y: 50,
    duration: 0.5,
    ease: 'power2.out',
    delay: 0.2,
    scrollTrigger: {
      trigger: ventContent2,
      start: 'top 70%',
      once: true
    }
  })
  gsap.from(airConditionerTitleInside, {
    opacity: 0,
    y: 50,
    duration: 0.7,
    ease: 'power2.out',
    delay: 0.2,
    scrollTrigger: {
      trigger: airConditionerTitleInside,
      start: 'top 80%',
      once: true
    }
  })
  gsap.from(airTransparentImage, {
    opacity: 0,
    height: 0, // Set the initial height to 0
    duration: 0.7,
    ease: 'power2.out',
    delay: 0.2,
    scrollTrigger: {
      trigger: airTransparentImage,
      start: 'top 60%',
      once: true
    }
  })

  gsap.from(airConditionerAdvantagesTitle, {
    opacity: 0,
    y: 70,
    duration: 0.7,
    ease: 'power2.out',
    scrollTrigger: {
      trigger: airConditionerAdvantagesTitle,
      start: 'top 70%',
      once: true
    }
  })
  gsap.from(airConditionerContent1, {
    opacity: 0,
    y: 70,
    duration: 0.7,
    ease: 'power2.out',
    scrollTrigger: {
      trigger: airConditionerContent1,
      start: 'top 70%',
      once: true
    }
  })
  gsap.from(airConditionerContent2, {
    opacity: 0,
    y: 70,
    duration: 0.7,
    ease: 'power2.out',
    scrollTrigger: {
      trigger: airConditionerContent2,
      start: 'top 70%',
      once: true
    }
  })
  gsap.from(airConditionerContent3, {
    opacity: 0,
    y: 70,
    duration: 0.7,
    ease: 'power2.out',
    scrollTrigger: {
      trigger: airConditionerContent3,
      start: 'top 70%',
      once: true
    }
  })
  gsap.from(airConditionerDidYouTitle, {
    opacity: 0,
    y: 70,
    duration: 0.7,
    ease: 'power2.out',
    scrollTrigger: {
      trigger: airConditionerDidYouTitle,
      start: 'top 70%',
      once: true
    }
  })
  const benefitsListItems = document.querySelectorAll('#vent-benefits-list li')

  benefitsListItems.forEach((item, index) => {
    gsap.set(item, { opacity: 0, y: 70 })

    ScrollTrigger.create({
      trigger: item,
      start: 'top 65%',
      once: true,
      onEnter: () => {
        gsap.to(item, {
          opacity: 1,
          y: 0,
          duration: 0.5,
          ease: 'power2.out',
          delay: 0.1 * index
        })
      }
    })
  })
})

window.addEventListener('load', function () {
  const loadingOverlay = document.getElementById('loading-overlay')
  loadingOverlay.style.display = 'none'

  document.body.style.overflow = 'visible'
  gsap.from('#vent-duct', {
    opacity: 0,
    x: 100,
    duration: 1.3,
    ease: 'power2.out'
  })
  gsap.from('#vent-blur', {
    opacity: 0,
    y: -300,
    duration: 0.9,
    ease: 'power2.out'
  })

  gsap.from('#vent-title', {
    opacity: 0,
    y: 80,
    duration: 1.2,
    ease: 'power2.out'
  })
})
document.addEventListener('DOMContentLoaded', function () {
  const sidenavMenu = document.getElementById('phone-nav')

  const phoneMenuIcon = document.getElementById('phone-menu-icon')

  const sidenavPcMenu = document.getElementById('side-nav')
  const sidenavPcitems = document.getElementById('nav-items')
  sidenavPcMenu.style.transform = 'translateX(0)'
  sidenavPcitems.style.color = 'white'

  gsap.set(sidenavMenu, { width: '0' })
  gsap.set(sidenavMenu.children, { opacity: 0, x: -20, stagger: 0.1 })

  function toggleSideNav () {
    gsap.to(sidenavMenu, {
      width: '100%',
      duration: 0.5,
      ease: 'power2.inOut',
      onStart: function () {
        showX()
        phoneMenuIcon.classList.remove('bg-black')
      }
    })
    gsap.to(sidenavMenu.children, {
      opacity: 1,
      x: 0,
      duration: 0.5,
      stagger: 0.2
    })
  }

  function closeSideNav () {
    gsap.to(sidenavMenu, { width: '0%', duration: 0.5, ease: 'power2.inOut' })
    gsap.to(sidenavMenu.children, {
      opacity: 0,
      x: -20,
      duration: 0.5,
      stagger: 0.2,
      onStart: function () {
        showBurgerMenu()
      },
      onComplete: function () {
        phoneMenuIcon.classList.add('bg-black')
      }
    })
  }
  function showX () {
    let lines = document.querySelectorAll('.menu_line')

    gsap.to(lines[1], {
      duration: 0.2,
      rotate: 45
    })
    gsap.to(lines[0], {
      duration: 0.2,
      x: 45,
      opacity: 5
    })
    gsap.to(lines[2], {
      duration: 0.2,
      rotate: -45
    })
  }
  function showBurgerMenu () {
    let lines = document.querySelectorAll('.menu_line')

    gsap.to(lines[1], {
      duration: 0.2,
      rotate: 0
    })
    gsap.to(lines[0], {
      duration: 0.2,
      x: 0,
      opacity: 1
    })
    gsap.to(lines[2], {
      duration: 0.2,
      rotate: 0
    })
  }
  // Event listener for phone menu icon
  phoneMenuIcon.addEventListener('click', function () {
    // Check if the side navigation is open or closed
    const isOpen = sidenavMenu.style.width === '100%'

    // Toggle side navigation based on its current state
    if (isOpen) {
      closeSideNav()
    } else {
      toggleSideNav()
    }
  })
  ScrollTrigger.create({
    trigger: '#footer',
    start: 'top 80%',
    end: 'bottom center',
    onEnter: () => {
      // Animate the nav-links to move left and out of the DOM
      gsap.to('#nav-links', {
        x: '-120%',
        duration: 0.2, // Adjust the duration as needed
        ease: 'power2.inOut' // Choose an easing function
      })
    },
    onLeaveBack: () => {
      // Animate the nav-links back to its original position
      gsap.to('#nav-links', {
        x: 0,
        duration: 0.2, // Adjust the duration as needed
        ease: 'power2.inOut' // Choose an easing function
      })
    }
    // Uncomment the following line if you want the animation to happen every time the user scrolls back up
    // toggleActions: "restart reverse restart reverse",
  })
  const listItems = document.querySelectorAll('.list-group')

  listItems.forEach(item => {
    item.addEventListener('mouseenter', () => {
      listItems.forEach(otherItem => {
        if (otherItem !== item) {
          otherItem.classList.add('text-neutral-300', 'scale-95', 'opacity-50')
        }
      })
    })

    item.addEventListener('mouseleave', () => {
      listItems.forEach(otherItem => {
        if (otherItem !== item) {
          otherItem.classList.remove(
            'text-neutral-300',
            'scale-95',
            'opacity-50'
          )
        }
      })
    })
  })
})
