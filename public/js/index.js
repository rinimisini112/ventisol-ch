window.addEventListener('load', function () {
  const loadingOverlay = document.getElementById('loading-overlay')
  loadingOverlay.style.display = 'none'

  document.body.style.overflow = 'visible'
  const layer2 = document.getElementById('layer2')
  const layer1 = document.getElementById('layer1')
  gsap.fromTo(
    [layer2, layer1],
    {
      translateX: 200,
      scale: 1.4
    },
    {
      translateX: 0,
      scale: 1,
      duration: 1.2,
      ease: 'power2.out',
      onComplete: function () {
        // Animation starts, you can add your code to animate the nav element
        gsap.to('#side-nav', {
          translateX: 0
        })
      }
    }
  )
})
document.addEventListener('DOMContentLoaded', function () {
  const sidenavMenu = document.getElementById('phone-nav')
  const phoneMenuIcon = document.getElementById('phone-menu-icon')

  gsap.set(sidenavMenu, { width: '0' })
  gsap.set(sidenavMenu.children, { opacity: 0, x: -20, stagger: 0.1 })

  function toggleSideNav () {
    gsap.to(sidenavMenu, {
      width: '100%',
      duration: 0.5,
      ease: 'power2.inOut',
      onStart: function () {
        showX()
        phoneMenuIcon.classList.remove('bg-black')
      }
    })
    gsap.to(sidenavMenu.children, {
      opacity: 1,
      x: 0,
      duration: 0.5,
      stagger: 0.2
    })
  }

  function closeSideNav () {
    gsap.to(sidenavMenu, { width: '0%', duration: 0.5, ease: 'power2.inOut' })
    gsap.to(sidenavMenu.children, {
      opacity: 0,
      x: -20,
      duration: 0.5,
      stagger: 0.2,
      onStart: function () {
        showBurgerMenu()
      },
      onComplete: function () {
        phoneMenuIcon.classList.add('bg-black')
      }
    })
  }
  function showX () {
    let lines = document.querySelectorAll('.menu_line')

    gsap.to(lines[1], {
      duration: 0.2,
      rotate: 45
    })
    gsap.to(lines[0], {
      duration: 0.2,
      x: 45,
      opacity: 5
    })
    gsap.to(lines[2], {
      duration: 0.2,
      rotate: -45
    })
  }
  function showBurgerMenu () {
    let lines = document.querySelectorAll('.menu_line')

    gsap.to(lines[1], {
      duration: 0.2,
      rotate: 0
    })
    gsap.to(lines[0], {
      duration: 0.2,
      x: 0,
      opacity: 1
    })
    gsap.to(lines[2], {
      duration: 0.2,
      rotate: 0
    })
  }

  phoneMenuIcon.addEventListener('click', function () {
    const isOpen = sidenavMenu.style.width === '100%'

    if (isOpen) {
      closeSideNav()
    } else {
      toggleSideNav()
    }
  })
  var tl = gsap.timeline()

  function handleClick (boxId) {
    let contentId = 'content' + boxId.slice(-1)
    let titleId = 'title' + boxId.slice(-1)
    let subtitleId = 'subtitle' + boxId.slice(-1)
    let imageId = 'image' + boxId.slice(-1)

    // Hide all titles, subtitles, and contents
    tl.to(['#image1', '#image2', '#image3', '#image4'], {
      opacity: 0,
      x: 20,
      display: 'none',
      duration: 0.1
    })
    tl.to(
      [
        '#title1',
        '#title2',
        '#title3',
        '#title4',
        '#subtitle1',
        '#subtitle2',
        '#subtitle3',
        '#subtitle4',
        '#content1',
        '#content2',
        '#content3',
        '#content4'
      ],
      { opacity: 0, x: 20, duration: 0.2 }
    )

    // Hide all images

    // Show the selected title, subtitle, content, and image
    tl.to(`#${titleId}, #${subtitleId}, #${contentId}, #${imageId}`, {
      opacity: 1,
      x: 0,
      display: 'block',
      duration: 0.3
    })

    // Apply styling directly to the clicked box
    let clickedBox = document.getElementById(boxId)
    clickedBox.style.boxShadow = '0 0 5px #03045e'
    clickedBox.style.transform = 'scale(1.1)'
    clickedBox.classList.remove('hover:scale-110')

    // Reset styling for all other boxes
    ;['box1', 'box2', 'box3', 'box4'].forEach(function (otherBox) {
      if (otherBox !== boxId) {
        var otherBoxElement = document.getElementById(otherBox)
        otherBoxElement.style.boxShadow = '0 25px 50px -12px rgb(0 0 0 / 0.25)'
        otherBoxElement.style.transform = 'none'
        clickedBox.classList.add('hover:scale-110')
      }
    })
  }
  document
    .getElementById('container')
    .addEventListener('click', function (event) {
      if (event.target.id && event.target.id.startsWith('box')) {
        handleClick(event.target.id)
      }
    })

  ScrollTrigger.create({
    trigger: '#footer',
    start: 'top 80%',
    end: 'bottom center',
    onEnter: () => {
      // Animate the nav-links to move left and out of the DOM
      gsap.to('#nav-links', {
        x: '-120%',
        duration: 0.2, // Adjust the duration as needed
        ease: 'power2.inOut' // Choose an easing function
      })
    },
    onLeaveBack: () => {
      // Animate the nav-links back to its original position
      gsap.to('#nav-links', {
        x: 0,
        duration: 0.2, // Adjust the duration as needed
        ease: 'power2.inOut' // Choose an easing function
      })
    }
  })
})

const navContentWrapper = document.getElementById('nav-content-wrapper')
const topContentWrapper = document.getElementById('top-content-wrapper')
const skewSepparator = document.querySelector('.skewed')
const welcomeQuote = document.getElementById('welcome-quote')
gsap.to('#nav-items', {
  scrollTrigger: {
    trigger: '#content-box',
    start: 'top 60%',
    end: '20% 20%',
    scrub: true
  },
  color: '#ffffff',
  onStart: function () {
    navContentWrapper.classList.add('w-1/5')
    navContentWrapper.classList.remove('w-0')
    welcomeQuote.classList.remove('translate-x-full', 'opacity-0')
    welcomeQuote.classList.add('translate-x-0', 'opacity-100')
    topContentWrapper.classList.add('opacity-100', 'translate-x-0')
    topContentWrapper.classList.remove('opacity-0', '-translate-x-1/4')
  }
})
const listItems = document.querySelectorAll('.list-group')

listItems.forEach(item => {
  item.addEventListener('mouseenter', () => {
    listItems.forEach(otherItem => {
      if (otherItem !== item) {
        otherItem.classList.add('text-neutral-300', 'opacity-50', 'scale-95')
      }
    })
  })

  item.addEventListener('mouseleave', () => {
    listItems.forEach(otherItem => {
      if (otherItem !== item) {
        otherItem.classList.remove('text-neutral-300', 'opacity-50', 'scale-95')
      }
    })
  })
})
