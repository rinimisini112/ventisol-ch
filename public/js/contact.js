$(document).ready(function () {
  // Hide success notification on SVG click
  $(".success-notification svg").click(function () {
    $(".success-notification").hide();
  });

  // Hide error notification on SVG click
  $(".error-notification svg").click(function () {
    $(".error-notification").hide();
  });
});
$(document).ready(function () {
  $.validator.addMethod(
    "valueNotEquals",
    function (value, element, arg) {
      return arg !== value;
    },
    "Ce champ est obligatoire"
  );
  $.validator.addMethod(
    "validPhoneNumber",
    function (value, element) {
      // Use a regular expression to allow digits and optional whitespace
      return /^[0-9\s]+$/.test(value);
    },
    "Exemple 021 559 58 72"
  );

  $("#contact-form").validate({
    rules: {
      name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      address: {
        required: true,
      },
      postal_code: {
        required: true,
      },
      city: {
        required: true,
      },
      telephone: {
        required: true,
        validPhoneNumber: true,
      },
      email: {
        required: true,
      },
      subject: {
        required: true,
      },
      message: {
        required: true,
      },
      issue: {
        valueNotEquals: "0",
      },
      region: {
        valueNotEquals: "0",
      },
    },
    messages: {
      name: {
        required: "Veuillez saisir votre prénom",
      },
      last_name: {
        required: "Veuillez saisir votre nom de famille",
      },
      address: {
        required: "L'adresse est obligatoire",
      },
      postal_code: {
        required: "Exigée",
      },
      city: {
        required: "Exigée",
      },
      telephone: {
        required: "Telephone is required",
      },
      email: {
        required: "Le téléphone est obligatoire",
      },
      subject: {
        required: "Le sujet est obligatoire",
      },
      message: {
        required: "Le message est obligatoire",
      },
      issue: {
        required: "Please select an issue",
      },
      region: {
        required: "Select your region",
      },
    },
    invalidHandler: function () {
      // Revert button content to its original state if the form is invalid
      $("#submitButton").html("<span>Envoyer</span>");
    },

    errorPlacement: function (error, element) {
      // Add a class to the input when there is an error
      element.addClass("error_effect");

      // Add the specified classes for error state
      element.addClass("placeholder:text-red-500 shadow-red-500 shadow-round");
      element.removeClass("focus:shadow-blue-600 focus:shadow-round-big");

      // For non-required fields, display the error message normally
      error.insertAfter(element);
    },
    success: function (label, element) {
      // Remove the error class when the input is valid
      $(element).removeClass("error_effect");

      // Remove the specified classes for non-error state
      $(element).removeClass(
        "placeholder:text-red-500 shadow-red-500 shadow-round"
      );
      $(element).addClass("focus:shadow-blue-600 focus:shadow-round-big");
    },
  });
  $("#submitButton").click(function () {
    if ($("#contact-form").valid()) {
      // If the form is valid, change button content to the spinner icon
      $(this).html('<i class="bx bx-loader-alt animate-spin"></i>');
      // Disable the button to prevent multiple submissions
      $(this).prop("disabled", true);
      // Submit the form
      $("#contact-form").submit();
    }
  });
});

function changeTextColor(selectElement) {
  var selectedOption = selectElement.options[selectElement.selectedIndex];
  selectElement.classList.toggle("text-[#999]", selectedOption.value === "#");
  selectElement.classList.toggle("text-black", selectedOption.value !== "#");
}
window.addEventListener("load", function () {
  var loadingOverlay = document.getElementById("loading-overlay");
  loadingOverlay.style.display = "none";

  document.body.style.overflow = "visible";
});
var sidenavPcMenu = document.getElementById("side-nav");
var sidenavPcitems = document.getElementById("nav-items");
sidenavPcMenu.style.transform = "translateX(0)";
sidenavPcitems.style.color = "white";

document.addEventListener("DOMContentLoaded", function () {
  var sidenavMenu = document.getElementById("phone-nav");

  var phoneMenuIcon = document.getElementById("phone-menu-icon");

  gsap.set(sidenavMenu, { width: "0" });
  gsap.set(sidenavMenu.children, { opacity: 0, x: -20, stagger: 0.1 });

  function toggleSideNav() {
    gsap.to(sidenavMenu, {
      width: "100%",
      duration: 0.5,
      ease: "power2.inOut",
      onStart: function () {
        showX();
        phoneMenuIcon.classList.remove("bg-black");
      },
    });
    gsap.to(sidenavMenu.children, {
      opacity: 1,
      x: 0,
      duration: 0.5,
      stagger: 0.2,
    });
  }

  function closeSideNav() {
    gsap.to(sidenavMenu, { width: "0%", duration: 0.5, ease: "power2.inOut" });
    gsap.to(sidenavMenu.children, {
      opacity: 0,
      x: -20,
      duration: 0.5,
      stagger: 0.2,
      onStart: function () {
        showBurgerMenu();
      },
      onComplete: function () {
        phoneMenuIcon.classList.add("bg-black");
      },
    });
  }
  function showX() {
    let lines = document.querySelectorAll(".menu_line");

    gsap.to(lines[1], {
      duration: 0.2,
      rotate: 45,
    });
    gsap.to(lines[0], {
      duration: 0.2,
      x: 45,
      opacity: 5,
    });
    gsap.to(lines[2], {
      duration: 0.2,
      rotate: -45,
    });
  }
  function showBurgerMenu() {
    let lines = document.querySelectorAll(".menu_line");

    gsap.to(lines[1], {
      duration: 0.2,
      rotate: 0,
    });
    gsap.to(lines[0], {
      duration: 0.2,
      x: 0,
      opacity: 1,
    });
    gsap.to(lines[2], {
      duration: 0.2,
      rotate: 0,
    });
  }
  // Event listener for phone menu icon
  phoneMenuIcon.addEventListener("click", function () {
    // Check if the side navigation is open or closed
    var isOpen = sidenavMenu.style.width === "100%";

    // Toggle side navigation based on its current state
    if (isOpen) {
      closeSideNav();
    } else {
      toggleSideNav();
    }
  });
  ScrollTrigger.create({
    trigger: "#footer",
    start: "top 100%",
    end: "bottom center",
    onEnter: () => {
      // Animate the nav-links to move left and out of the DOM
      gsap.to("#nav-links", {
        x: "-120%",
        duration: 0.2, // Adjust the duration as needed
        ease: "power2.inOut", // Choose an easing function
      });
    },
    onLeaveBack: () => {
      // Animate the nav-links back to its original position
      gsap.to("#nav-links", {
        x: 0,
        duration: 0.2, // Adjust the duration as needed
        ease: "power2.inOut", // Choose an easing function
      });
    },
    // Uncomment the following line if you want the animation to happen every time the user scrolls back up
    // toggleActions: "restart reverse restart reverse",
  });
  const listItems = document.querySelectorAll(".list-group");

  listItems.forEach((item) => {
    item.addEventListener("mouseenter", () => {
      listItems.forEach((otherItem) => {
        if (otherItem !== item) {
          otherItem.classList.add("text-neutral-300");
          otherItem.classList.add("opacity-50");
          otherItem.classList.add("scale-95");
        }
      });
    });

    item.addEventListener("mouseleave", () => {
      listItems.forEach((otherItem) => {
        if (otherItem !== item) {
          otherItem.classList.remove("text-neutral-300");
          otherItem.classList.remove("opacity-50");
          otherItem.classList.remove("scale-95");
        }
      });
    });
  });
});
