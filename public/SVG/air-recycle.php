<svg width='120px' height='120px' version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
    <polygon style="fill:#354CA0;stroke:#FFFFFF;stroke-miterlimit:10;" points="58,510.4 57.3,100.5 278.4,51.6 278,560.2 " />
    <polygon style="fill:#d4e0f9f9;stroke:#FFFFFF;stroke-miterlimit:10;" points="158.3,489 100.9,476.4 100.9,315.8 158.3,315.8 " />
    <polygon style="fill:#d4e0f9f9;stroke:#FFFFFF;stroke-miterlimit:10;" points="177.4,315.8 234.8,315.8 234.8,505.9 177.4,493 " />
    <polygon style="fill:#d4e0f9f9;stroke:#FFFFFF;stroke-miterlimit:10;" points="177.2,119 177.6,297.3 234.8,297.3 234.8,105.2 " />
    <polygon style="fill:#d4e0f9f9;stroke:#FFFFFF;stroke-miterlimit:10;" points="158.5,122.1 100.5,134.7 101.2,297.4 158.1,297.1 " />
    <polyline style="fill:#FFFFFF;stroke:#3C5CAA;stroke-width:8;stroke-miterlimit:10;" points="278.9,101.1 430.6,100.9 431,168.2 412.8,173.6 
	412.5,119 278.9,119 " />
    <polyline style="fill:#FFFFFF;stroke:#3C5CAA;stroke-width:8;stroke-miterlimit:10;" points="278.4,493.3 412.1,492.7 412.3,440.5 430.8,427 
	430.8,510.7 278.4,510.7 " />
    <path style="fill:#FFFFFF;stroke:#3C5CAA;stroke-miterlimit:10;" d="M234.8,120.2" />
    <g>

        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="443.5972" y1="316.1774" x2="465.8969" y2="316.1774" gradientTransform="matrix(1.0806 -2.327194e-03 -2.327194e-03 1.0681 -21.7365 -14.044)">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="7.380247e-03" style="stop-color:#FFF5F0" />
            <stop offset="4.767627e-02" style="stop-color:#FCD1BE" />
            <stop offset="9.308982e-02" style="stop-color:#F9AF94" />
            <stop offset="0.1419" style="stop-color:#F69474" />
            <stop offset="0.195" style="stop-color:#F47D5B" />
            <stop offset="0.2537" style="stop-color:#F26847" />
            <stop offset="0.32" style="stop-color:#F15537" />
            <stop offset="0.3974" style="stop-color:#EF442D" />
            <stop offset="0.4931" style="stop-color:#EF3727" />
            <stop offset="0.6274" style="stop-color:#EE2F25" />
            <stop offset="1" style="stop-color:#EE2C24" />
        </linearGradient>
        <path style="fill:url(#SVGID_1_);" d="M456.9,339.9c4.5-0.3,8.8-0.3,12.8-0.2l11.3-34.5l-23,4.1
		C459.4,321.2,458.7,330.4,456.9,339.9z" />

        <linearGradient id="SVGID_00000067956835290401266250000013608862814428946343_" gradientUnits="userSpaceOnUse" x1="430.7038" y1="269.3216" x2="464.1705" y2="269.3216" gradientTransform="matrix(1.0806 -2.327194e-03 -2.327194e-03 1.0681 -21.7365 -14.044)">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="3.967583e-03" style="stop-color:#FFFBF9" />
            <stop offset="8.483291e-02" style="stop-color:#FCD1BD" />
            <stop offset="0.1697" style="stop-color:#F9AC90" />
            <stop offset="0.2591" style="stop-color:#F68F6E" />
            <stop offset="0.3525" style="stop-color:#F37654" />
            <stop offset="0.4511" style="stop-color:#F15F3F" />
            <stop offset="0.5566" style="stop-color:#F04B31" />
            <stop offset="0.6723" style="stop-color:#EF3B28" />
            <stop offset="0.8061" style="stop-color:#EE3025" />
            <stop offset="1" style="stop-color:#EE2C24" />
        </linearGradient>
        <path style="fill:url(#SVGID_00000067956835290401266250000013608862814428946343_);" d="M454.4,288.9l24.8-4.5l-21.7-28.3
		c-4.4,2-9.2,3.8-14.4,5.4C447.9,269.7,451.6,277.7,454.4,288.9z" />

        <linearGradient id="SVGID_00000143577956297817909280000010922205871065335981_" gradientUnits="userSpaceOnUse" x1="350.0716" y1="295.2245" x2="450.2717" y2="295.2245" gradientTransform="matrix(1.0806 -2.327194e-03 -2.327194e-03 1.0681 -21.7365 -14.044)">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="9.290285e-02" style="stop-color:#BBC0D4" />
            <stop offset="0.1858" style="stop-color:#8C98B8" />
            <stop offset="0.2759" style="stop-color:#6A7DA5" />
            <stop offset="0.3622" style="stop-color:#4F6B98" />
            <stop offset="0.4438" style="stop-color:#396090" />
            <stop offset="0.5188" style="stop-color:#29598B" />
            <stop offset="0.5809" style="stop-color:#22578A" />
        </linearGradient>
        <path style="fill:url(#SVGID_00000143577956297817909280000010922205871065335981_);" d="M358.3,321.4l83.8,85.4
		c2.3,2.3,6.5,2.4,8.8-4.9l13.2-43.5c-4.5,0-9.3,0.3-14.5,1c-3.6,5.4-8.1,8.9-13.3,9.8c-17.6,3.2-37-23.1-43.4-58.6
		c-6.4-35.5,2.7-66.9,20.3-70.1c5.6-1,11.4,1,16.9,5.3c6-1.4,11.4-3.2,16.2-5.2l-29.5-40.5c-2.4-3.3-11.6-14.1-16.5-2.5l-43.6,114.8
		C356.5,312.5,354.1,317.1,358.3,321.4z" />
        <path style="fill:#E52325;" d="M472.5,243.2l5.9,9.3c19.2-22.1,18.9-37.7,18.9-37.7c-16.9,7-36.5,10.2-36.5,10.2l5.5,8.6l-1.5,1.5
		c0,0-17.1,15.3-57.2,18.2c0,0-5.2,8.2-5,13.1c0,0,39.5-2.1,69.2-22.5L472.5,243.2z" />
        <path style="fill:#E42527;" d="M491.2,347.4l2.3-10.8c25.7,14.1,30.8,28.8,30.8,28.8c-18.3-0.7-37.7,3.1-37.7,3.1l2.2-10l-1.9-0.8
		c0,0-21.3-8.4-59.9,2.8c0,0-7.8-5.9-9.2-10.6c0,0,37.8-11.7,72.7-2.9L491.2,347.4z" />
        <path style="fill:#22578A;" d="M371,261.6l5-13.1c-14.9-0.8-21.8-6.1-23.5-7.6l5.9-7.6c0,0-22.4-4.1-29.1-8.4
		c0,0,3.7,20.6,9.4,33.9l6-7.8C351.2,258.2,371,261.6,371,261.6z" />
        <path style="fill:#22578A;" d="M390.9,363.7l10.7,9.5c-13.7,6-19.8,14.4-20.8,16.4l8.2,5.1c0,0-19.6,11.7-24.4,18
		c0,0-3.7-20.6-3-35l8.3,5.2C373.6,373.8,390.9,363.7,390.9,363.7z" />
        <path style="fill:#22578A;" d="M333.5,325.9l18.1-3.2c-1.1-0.6-2-2.1-2.3-3.9c-0.4-2.1,0.1-4,1.1-4.8l-18.6,3.3l-2.8-14l-33,25.4
		l40.3,11.2L333.5,325.9z" />
        <path style="fill:#E42527;" d="M503.7,297.4l2.5,14l35.8-27.8l-43.1-12.8l2.5,14l-98.1,17.6l-0.3-0.1c-0.6,1.9-0.8,4.4-0.3,6.9
		c0.4,2.4,1.3,4.5,2.5,6l0-0.2L503.7,297.4z" />
    </g>
</svg>