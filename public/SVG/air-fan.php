<svg width='120px' height='120px' version="1.1" id="_x32_" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 800 800" style="enable-background:new 0 0 800 800;" xml:space="preserve">
    <g>
        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="191.8107" y1="325.6101" x2="281.7942" y2="325.6101">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="4.114965e-02" style="stop-color:#D1DCE7" />
            <stop offset="8.502942e-02" style="stop-color:#A9BDD1" />
            <stop offset="0.133" style="stop-color:#85A2BD" />
            <stop offset="0.1847" style="stop-color:#668AAD" />
            <stop offset="0.241" style="stop-color:#4D77A0" />
            <stop offset="0.3041" style="stop-color:#3A6996" />
            <stop offset="0.3776" style="stop-color:#2D5F8E" />
            <stop offset="0.471" style="stop-color:#25598A" />
            <stop offset="0.6516" style="stop-color:#235789" />
        </linearGradient>
        <path style="fill:url(#SVGID_1_);" d="M224.4,385.9c0.7,0.4,1.6,0.9,2.6,1.6c5.7-4,12.7-6.3,20.2-6.3c4.7,0,9.1,0.9,13.2,2.6
		c4.1-17.3,12.7-47.5,20.1-73.5c7.7-26.9-18.2-59-51.9-41.4c-21.8,11-36.7,33.6-36.7,59.7C191.8,352.9,204.9,374.2,224.4,385.9z" />

        <linearGradient id="SVGID_00000142167476023582896380000014489565285556682375_" gradientUnits="userSpaceOnUse" x1="103.4796" y1="390.847" x2="222.8168" y2="390.847">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="4.726594e-03" style="stop-color:#F5F7FA" />
            <stop offset="2.756483e-02" style="stop-color:#C9D6E2" />
            <stop offset="5.280787e-02" style="stop-color:#A1B8CD" />
            <stop offset="7.994851e-02" style="stop-color:#7F9DBA" />
            <stop offset="0.1095" style="stop-color:#6287AB" />
            <stop offset="0.1423" style="stop-color:#4B769E" />
            <stop offset="0.1797" style="stop-color:#396895" />
            <stop offset="0.2245" style="stop-color:#2C5E8E" />
            <stop offset="0.2842" style="stop-color:#25598A" />
            <stop offset="0.419" style="stop-color:#235789" />
        </linearGradient>
        <path style="fill:url(#SVGID_00000142167476023582896380000014489565285556682375_);" d="M222.8,391.5c-14.8-9.8-40.3-28.1-62.2-44
		c-22.6-16.4-61.7-3-56.7,34.6c2.9,24.3,19,46,43.5,54.9c22.9,8.4,47.5,3.4,65.1-11c0.6-0.5,1.4-1.2,2.3-1.9
		C212,412.9,214.8,400.5,222.8,391.5z" />

        <linearGradient id="SVGID_00000082350851289113484180000011695832024840673715_" gradientUnits="userSpaceOnUse" x1="141.2791" y1="486.7071" x2="251.7314" y2="486.7071">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="1.435967e-02" style="stop-color:#E9EEF3" />
            <stop offset="4.480092e-02" style="stop-color:#C0CFDD" />
            <stop offset="7.894322e-02" style="stop-color:#9AB2C9" />
            <stop offset="0.1161" style="stop-color:#7A99B7" />
            <stop offset="0.1572" style="stop-color:#5E84A9" />
            <stop offset="0.2036" style="stop-color:#48749D" />
            <stop offset="0.2578" style="stop-color:#376794" />
            <stop offset="0.3248" style="stop-color:#2C5E8E" />
            <stop offset="0.4188" style="stop-color:#25588A" />
            <stop offset="0.6798" style="stop-color:#235789" />
        </linearGradient>
        <path style="fill:url(#SVGID_00000082350851289113484180000011695832024840673715_);" d="M217.6,431
		c-13.9,11.1-39.2,29.6-61.1,45.6c-22.6,16.5-22,57.7,15.4,64.6c24,4.7,49.7-3.9,65.7-24.4c15-19.2,17.9-44.1,9.7-65.3
		c-0.3-0.8-0.7-1.7-1.1-2.8C234.5,447.9,223.6,441.4,217.6,431z" />

        <linearGradient id="SVGID_00000167393468329950243330000005930893258948090507_" gradientUnits="userSpaceOnUse" x1="256.4107" y1="481.228" x2="360.0864" y2="481.228">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="5.826759e-03" style="stop-color:#F5F7FA" />
            <stop offset="3.398084e-02" style="stop-color:#C9D6E2" />
            <stop offset="6.509946e-02" style="stop-color:#A1B8CD" />
            <stop offset="9.855737e-02" style="stop-color:#7F9DBA" />
            <stop offset="0.135" style="stop-color:#6287AB" />
            <stop offset="0.1754" style="stop-color:#4B769E" />
            <stop offset="0.2216" style="stop-color:#396895" />
            <stop offset="0.2768" style="stop-color:#2C5E8E" />
            <stop offset="0.3503" style="stop-color:#25598A" />
            <stop offset="0.5165" style="stop-color:#235789" />
        </linearGradient>
        <path style="fill:url(#SVGID_00000167393468329950243330000005930893258948090507_);" d="M256.4,448.4c8,15.9,20.8,44.5,31.9,69.2
		c11.4,25.5,51.9,33.5,66.4-1.6c9.6-22.5,6.5-49.4-10.2-69.3c-15.7-18.7-39.4-26.6-61.9-23c-0.8,0.1-1.8,0.3-2.9,0.5
		C276.5,435.4,267.9,444.7,256.4,448.4z" />

        <linearGradient id="SVGID_00000155123277399581184770000011114881912161891775_" gradientUnits="userSpaceOnUse" x1="262.8276" y1="374.6958" x2="383.1612" y2="374.6958">
            <stop offset="0" style="stop-color:#FFFFFF" />
            <stop offset="2.959764e-02" style="stop-color:#E1E8EF" />
            <stop offset="7.899476e-02" style="stop-color:#B5C6D7" />
            <stop offset="0.1321" style="stop-color:#8DA8C2" />
            <stop offset="0.1881" style="stop-color:#6C8FB0" />
            <stop offset="0.248" style="stop-color:#527BA2" />
            <stop offset="0.3131" style="stop-color:#3D6B97" />
            <stop offset="0.3861" style="stop-color:#2E608F" />
            <stop offset="0.4733" style="stop-color:#26598A" />
            <stop offset="0.6124" style="stop-color:#235789" />
        </linearGradient>
        <path style="fill:url(#SVGID_00000155123277399581184770000011114881912161891775_);" d="M311.4,335.2
		c-23.2,7.5-39.5,26.6-44.5,48.7c-0.2,0.8-0.4,1.8-0.7,2.9c0.7,0.5,1.4,1.1,2.1,1.8c8.4,7.6,12.4,18.3,11.7,28.9
		c17.7-1.5,49.1-2.7,76.1-3.6c27.9-1,50.5-35.6,23.4-62.2C362.3,334.4,336.2,327.1,311.4,335.2z" />
        <path style="fill:#235789;" d="M247.1,387.4c-15.4,0-27.8,12.5-27.8,27.8c0,15.4,12.5,27.8,27.8,27.8c15.4,0,27.8-12.5,27.8-27.8
		C274.9,399.9,262.5,387.4,247.1,387.4z M247.1,427c-6.5,0-11.7-5.2-11.7-11.7c0-6.5,5.2-11.7,11.7-11.7c6.5,0,11.7,5.2,11.7,11.7
		C258.8,421.7,253.6,427,247.1,427z" />
        <g>
            <path style="fill:#235789;" d="M437.5,415.3c0.9,75.4-47.5,147.8-117.5,176c-125.1,52-263.6-40.6-263.4-176
			C56.6,280,195,187.6,319.9,239.5C389.8,267.6,438.3,339.9,437.5,415.3L437.5,415.3z M415.2,415.3c0.8-66.6-41.9-130.6-103.7-155.4
			c-110.3-45.7-232.6,36-232.4,155.4c-0.2,119.1,122.3,200.9,232.3,155.2C373.2,545.7,415.9,481.8,415.2,415.3L415.2,415.3z" />
        </g>
    </g>
    <path style="fill:none;stroke:#235789;stroke-width:22;stroke-miterlimit:10;" d="M721.6,648.4H80c-28.6,0-51.8-23.2-51.8-51.8
	V242.4c0-28.6,23.2-51.8,51.8-51.8h641.5c28.6,0,51.8,23.2,51.8,51.8v354.2C773.4,625.2,750.2,648.4,721.6,648.4z" />
    <line style="fill:none;stroke:#235789;stroke-width:22;stroke-miterlimit:10;" x1="470.8" y1="279.5" x2="709.2" y2="280.5" />
    <linearGradient id="SVGID_00000132777361515303017540000014620073930899142047_" gradientUnits="userSpaceOnUse" x1="469.3995" y1="345" x2="710.6005" y2="345">
        <stop offset="0.1216" style="stop-color:#235789" />
        <stop offset="0.3171" style="stop-color:#FFFFFF" />
    </linearGradient>
    <line style="fill:url(#SVGID_00000132777361515303017540000014620073930899142047_);stroke:#235789;stroke-width:22;stroke-miterlimit:10;" x1="469.4" y1="345.5" x2="710.6" y2="344.5" />
    <line style="fill:none;stroke:#235789;stroke-width:22;stroke-miterlimit:10;" x1="470.1" y1="410.5" x2="709.9" y2="409.5" />
    <line style="fill:none;stroke:#235789;stroke-width:22;stroke-miterlimit:10;" x1="470.1" y1="475" x2="709.9" y2="475" />
    <line style="fill:none;stroke:#235789;stroke-width:22;stroke-miterlimit:10;" x1="470.1" y1="540" x2="709.9" y2="540" />
    <path id="primary" style="opacity:0.7;" d="M135.3,400.7c5.9,1.8,14.8,3.7,25.6,2.9c20-1.5,33.8-11.4,39.4-15.9
	c4.9-0.9,9.6,2.1,11,6.7c1.3,4.4-0.8,9.2-5,11.3c-7.4,4.4-20.3,10.8-37.4,13c-12.3,1.6-22.8,0.4-30.1-0.9c-4.9,1.4-10-1.8-11-6.7
	C126.8,406.4,130.2,401.7,135.3,400.7z M106.8,590.8c15.6,17.3,42.6,18.5,60.2,2.7c3.9-3.5,4.3-9.5,0.8-13.4
	c-3.5-3.9-9.5-4.1-13.4-0.6c-9.8,8.8-24.8,8.2-33.5-1.5c-8.7-9.6-7.8-24.6,2-33.4c13-16.7,29.9-35.7,51.4-54.5
	c18.1-15.8,35.7-28.3,51.5-38.1c4-3.2,4.4-9.4,0.8-13.4c-3.5-3.9-9.5-4.1-13.4-0.6c-15.3,9.3-32.4,21.1-50,36.3
	c-22.6,19.5-39.9,39.2-52.9,56.4C93.2,548.1,92.8,575.3,106.8,590.8z" />
    <path id="secondary" style="opacity:0.7;fill:#2CA9BC;" d="M199.7,520.2c2.5-7.9,6.2-17.2,11.6-27.2c7.1-13,15-23.1,21.8-30.5
	c2.1-4.7,7.8-6.5,12-4.1c4,2.3,5.1,7.9,2.4,12.4c-5.9,5.5-13.4,13.6-20.1,24.7c-7.6,12.7-11.4,24.6-13.3,32.9
	c-0.8,8.9,0.9,11.5,2.4,12.4c4,2.3,9.3,0.5,12-4.1c2.6-4.5,8-6.4,12-4.1c4,2.3,5.1,7.9,2.4,12.4c-7.9,13.6-24,19.1-35.9,12.2
	C195.1,550.5,191.8,533.9,199.7,520.2z M53,468.2c7.4,15.8,27.4,22.1,44.7,14c14.5-2.9,33-8,53.2-17.3c20.8-9.6,37-20.8,48.7-30.2
	c0.5-0.2,3.2-1.5,4.6-4.7c0.8-2,0.9-4.2-0.1-6.1c-1.8-4-6.8-5.5-11.2-3.5c-11.7,9.5-28.2,21.1-49.6,30.8
	c-19.9,9.1-38.1,14-52.4,16.7c-0.9,0-19,0.3-22.4-7c-3.7-7.9,0.3-17.6,9-21.6c4.3-2,6.3-6.9,4.5-10.8c-1.8-4-6.8-5.5-11.2-3.5
	C53.7,433,45.6,452.4,53,468.2z" />
    <path d="M196.5,396.8" />
    <path d="M201.9,397" />
    <path d="M165,402.6" />
    <path d="M159,402.1" />
    <path style="fill:#FFFFFF;" d="M345,320.1c-6.4,0-6.4,10,0,10C351.4,330.1,351.4,320.1,345,320.1z" />
    <path style="fill:#FFFFFF;" d="M299.2,325.1c-6.4,0-6.4,10,0,10C305.6,335.1,305.6,325.1,299.2,325.1z" />
    <path style="fill:#FFFFFF;" d="M169.8,328.2c-6.4,0-6.4,10,0,10C176.2,338.2,176.2,328.2,169.8,328.2z" />
    <circle style="stroke:#000000;stroke-miterlimit:10;" cx="727.1" cy="232.9" r="7.7" />
    <circle style="stroke:#000000;stroke-miterlimit:10;" cx="734.9" cy="604.6" r="7.7" />
    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="719.8" y1="230.5" x2="734.5" y2="235.2" />
    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="729.3" y1="233" x2="728.4" y2="232.7" />
    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="728.3" y1="600.5" x2="741.5" y2="608.6" />
    <circle style="stroke:#000000;stroke-miterlimit:10;" cx="67.9" cy="232.9" r="7.7" />
    <circle style="stroke:#000000;stroke-miterlimit:10;" cx="67.9" cy="604.6" r="7.7" />
    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="719.2" y1="230.7" x2="734" y2="235.4" />
    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="60.1" y1="232.9" x2="75.6" y2="232.9" />
    <line style="fill:none;stroke:#FFFFFF;stroke-miterlimit:10;" x1="67.9" y1="596.8" x2="67.9" y2="611.6" />
</svg>