@include('partials._head')
<style>
  /* Styles for Swiper Navigation */
  .swiper-button-next,
  .swiper-button-prev {
    width: 60px;
    height: 60px;
    background-color: #235889a5;
    /* Background color */
    color: #fff;
    /* Text color */
    border-radius: 50%;
    /* Rounded corners for a circular button */
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 20px;
    cursor: pointer;
    transition: background-color 0.3s ease;
    /* Smooth transition on hover */
  }

  /* Hover effect */
  .swiper-button-next:hover,
  .swiper-button-prev:hover {
    background-color: #235789;
  }

  /* Additional styles for pagination */
  .swiper-pagination {
    position: absolute;
    bottom: 10px;
    left: 50%;
    transform: translateX(-50%);
  }

  .swiper-pagination-bullet {
    width: 10px;
    height: 10px;
    background-color: #777;
    opacity: 0.7;
    margin: 0 8px;
    border-radius: 50%;
    cursor: pointer;
    transition: background-color 0.3s ease;
    /* Smooth transition on hover */
  }

  .swiper-pagination-bullet-active {
    background-color: #fff;
    /* Active bullet color */
  }
</style>

<body style="overflow: hidden;">
  @include('partials._navbar')
  <div class="relative h-[50vh] w-full overflow-clip md:h-[70vh] lg:h-screen" id="parallax-container"
    style="background-image: url('{{ asset('images/main-background.jpg') }}">
    <img class="absolute bottom-0 left-0 z-10 aspect-auto h-full w-full lg:h-auto lg:object-cover" id="layer2"
      src="{{ asset('images/layer-2-removebg1.png') }}" alt="" loading="lazy">
    <img
      class="absolute bottom-0 right-0 z-20 aspect-auto w-full bg-gradient-to-br from-transparent via-transparent to-white object-cover lg:w-1/2"
      id="layer1" src="{{ asset('images/layer1-removebg.png') }}" alt="" loading="lazy">
  </div>
  <div
    class="skewed bottom-[40%] flex h-[15%] items-center justify-center text-2xl font-bold tracking-wide md:bottom-[20%] md:text-4xl lg:bottom-[-8rem] lg:h-1/5 lg:text-5xl"
    style="background-image: url('{{ asset('images/batthern.png') }}');">
    <p class="fira-sans-bold translate-x-full text-white opacity-0 delay-300 duration-300 lg:pl-16" id="welcome-quote">
      Donner vie aux espaces.</p>
  </div>
  <main class="block h-auto w-full bg-venti-blue bg-opacity-20 lg:flex lg:h-[115vh]" id="content-box">
    <div class="z-40 hidden h-full w-0 bg-venti-blue delay-200 duration-200 lg:block" id="nav-content-wrapper"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <div
      class="flex w-full -translate-x-1/4 flex-col items-center justify-center gap-16 pt-32 opacity-0 duration-300 lg:w-4/5 lg:pt-16"
      id="top-content-wrapper" style="background-image:url('{{ asset('/images/soft-wallpaper.png') }}')">
      <div class="px-0 md:px-6 lg:px-0">
        <p class="fira-sans-bold cursor-default pb-3 text-center text-2xl font-medium text-venti-blue">Respirez en toute
          sécurité avec <span
            class="bg-opacity-85 bg-venti-blue px-0.5 font-bold tracking-wide text-white">Confiance</span>.</p>
        <p class="fira-sans-bold pb-8 text-center text-3xl font-bold lg:pb-6">Nous faisons les choses différemment,
          Inspirez la différence.</p>
        <p class="mx-auto w-[90%] pb-8 text-lg text-neutral-700 lg:w-[65%] lg:text-center">Nous voulons vivre dans un
          monde où une bonne qualité d'air est accessible à tous. Chez <span class="font-bold">Ventisol</span>, nous
          nous efforçons de fournir des solutions de ventilation écoénergétiques, faciles à installer, fiables et
          durables pour garantir la qualité de l'air dans tous les espaces intérieurs que nous habitons et traversons.
        </p>
      </div>
      <div class="flex w-[90%] flex-col items-center justify-center gap-24 lg:flex-row lg:gap-6">
        <div class="relative h-auto w-full rounded-xl bg-white shadow-2xl md:w-3/4 lg:h-[300px] lg:w-[45%]"
          id="air-fan-box">
          <div class="flip-card relative z-10">
            <div class="flip-card-inner">
              <div class="flip-card-front rounded-2xl bg-white shadow-2xl"
                style="background-image: url('{{ asset('images/soft-wallpaper.png') }}')">
                <img class="w-4/5" src="{{ asset('images/production-items.png') }}" alt="">
              </div>
              <div class="flip-card-back rounded-lg bg-white shadow-2xl"
                style="background-image: url('{{ asset('images/cubes.png') }}')">
                <a class="group relative w-3/4 cursor-pointer rounded-2xl bg-gradient-to-bl from-venti-blue via-[#3783cae6] to-[#3c8edbea] py-2 font-bold text-white duration-200 hover:scale-110"
                  href="{{ route('about.production') }}">Voir plus
                  <svg
                    class="feather feather-chevron-right invisible absolute right-4 top-[0.6rem] opacity-0 duration-200 group-hover:visible group-hover:translate-x-3 group-hover:opacity-100"
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <polyline points="9 18 15 12 9 6"></polyline>
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div class="relative z-10 -translate-y-1/4 cursor-default">
            <h2 class="roboto-medium pb-4 text-center text-xl text-venti-blue">Production commerciale</h2>
            <p class="roboto px-4 pb-2 text-neutral-800 lg:px-8">Ventisol est fière de son site de production dédié, où
              nous fabriquons des conduits et des évents de qualité supérieure pour toute une série d'applications
              commerciales.</p>
            <a class="mx-auto flex w-1/2 translate-y-full cursor-pointer items-center justify-center rounded-2xl bg-gradient-to-bl from-venti-blue via-[#3783cae6] to-[#3c8edbea] py-2 font-bold text-white duration-200 hover:scale-110 lg:hidden"
              href="key_advantages.php">Voir plus
              <svg class="feather feather-chevron-right" xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none" stroke="white" stroke-width="2" stroke-linecap="round"
                stroke-linejoin="round">
                <polyline points="9 18 15 12 9 6"></polyline>
              </svg>
            </a>
          </div>
          <div class="absolute left-0 top-0 h-full w-full overflow-clip rounded-2xl bg-white"
            style="background-image: url('{{ asset('/images/soft-wallpaper.png') }}')">
            <div
              class="opacity-15 absolute -top-4 left-0 h-[45px] w-full -skew-y-[18deg] bg-gradient-to-l from-venti-blue via-[#3783cae6] to-[#3c8edbea]">
            </div>
            <div
              class="opacity-15 absolute -bottom-10 left-0 h-[70%] w-full -skew-y-[18deg] bg-gradient-to-t from-venti-blue via-[#3783cae6] to-[#3c8edbea]">
            </div>
          </div>
        </div>
        <div class="relative h-auto w-full rounded-xl shadow-2xl md:w-3/4 lg:h-[300px] lg:w-[45%]" id="conditioner-box">
          <div class="flip-card2 z-20">
            <div class="flip-card-inner2 z-20">
              <div class="flip-card-front2 z-20 overflow-clip rounded-2xl bg-white shadow-2xl"
                style="background-image:url('{{ asset('images/soft-wallpaper.png') }}')">
                <img src="{{ asset('images/vent-fixing-high.png') }}" alt="">
              </div>
              <div class="flip-card-back2 z-20 rounded-lg bg-white shadow-2xl"
                style="background-image: url('{{ asset('images/cubes.png') }}')">
                <a class="group relative w-3/4 cursor-pointer rounded-2xl bg-gradient-to-bl from-venti-blue via-[#3783cae6] to-[#3c8edbea] py-2 font-bold text-white duration-200 hover:scale-110"
                  href="{{ route('about.installation') }}">Voir plus
                  <svg
                    class="feather feather-chevron-right invisible absolute right-4 top-[0.6rem] opacity-0 duration-200 group-hover:visible group-hover:translate-x-3 group-hover:opacity-100"
                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <polyline points="9 18 15 12 9 6"></polyline>
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div class="relative z-10 -translate-y-1/4 text-white">
            <h2 class="roboto-medium pb-2 text-center text-xl">Ventisol : Experts en ventilation industrielle.</h2>
            <p class="roboto px-4 pb-2 lg:px-2">Chez Ventisol, nous nous spécialisons dans l'installation de systèmes
              de ventilation de qualité commerciale. Grâce à notre engagement indéfectible envers la qualité et la
              précisio...</p>
            <a class="mx-auto flex w-1/2 translate-y-full cursor-pointer items-center justify-center rounded-2xl bg-gradient-to-bl from-white via-[#eff8ffea] to-[#d7ecffe6] py-2 font-bold text-venti-blue duration-200 hover:scale-110 lg:hidden"
              href="key_advantages.php">Voir plus
              <svg class="feather feather-chevron-right" xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none" stroke="#235789" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round">
                <polyline points="9 18 15 12 9 6"></polyline>
              </svg>
            </a>
          </div>
          <div
            class="absolute left-0 top-0 -z-10 h-full w-full overflow-clip rounded-2xl bg-gradient-to-bl from-venti-blue via-[#3176b6] to-[#3783ca]">
            <div
              class="absolute -top-4 left-0 h-[45px] w-full skew-y-[18deg] bg-gradient-to-l from-venti-blue via-[#3783cae6] to-[#3c8edbea] opacity-50">
            </div>
            <div
              class="absolute -bottom-10 left-0 h-[70%] w-full skew-y-[18deg] bg-gradient-to-t from-venti-blue via-[#3783cae6] to-[#3c8edbea] opacity-50">
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <div class="flex h-[300px] w-full items-start bg-venti-blue bg-opacity-20 py-0 md:py-6 lg:h-[400px] lg:py-0">
    <div class="hidden h-full w-1/5 bg-venti-blue lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">

    </div>
    <section
      class="relative mt-6 flex h-full w-full items-center overflow-clip bg-venti-blue pl-3 lg:mt-0 lg:w-4/5 lg:pl-6"
      id="wide-section-banner">
      <div class="swiper-container relative h-[95%] w-[95%] lg:w-full"
        style="background-image: url('{{ asset('images/batthern.png') }}');">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <div class="relative h-[95%] w-full flex-shrink-0 overflow-clip bg-venti-blue shadow-2xl duration-300">
              <img class="aspect-auto h-full w-full object-cover" src="{{ asset('images/gallery1.jpg') }}"
                alt="" loading="lazy">
            </div>
          </div>
          <div class="swiper-slide">
            <div class="relative h-[95%] w-full flex-shrink-0 overflow-clip bg-venti-blue shadow-2xl duration-300">
              <img class="aspect-auto h-full w-full object-cover" src="{{ asset('images/gallery2.jpg') }}"
                alt="" loading="lazy">
            </div>
          </div>
          <div class="swiper-slide">
            <div class="relative h-[95%] w-full flex-shrink-0 overflow-clip bg-venti-blue shadow-2xl duration-300">
              <img class="aspect-auto h-full w-full object-cover" src="{{ asset('images/gallery3.jpg') }}"
                alt="" loading="lazy">
            </div>
          </div>
          <div class="swiper-slide">
            <div class="relative h-[95%] w-full flex-shrink-0 overflow-clip bg-venti-blue shadow-2xl duration-300">
              <img class="aspect-auto h-full w-full object-cover" src="{{ asset('images/gallery4.jpg') }}"
                alt="" loading="lazy">
            </div>
          </div>
          <div class="swiper-slide">
            <div class="relative h-[95%] w-full flex-shrink-0 overflow-clip bg-venti-blue shadow-2xl duration-300">
              <img class="aspect-auto h-full w-full object-cover" src="{{ asset('images/gallery5.jpg') }}"
                alt="" loading="lazy">
            </div>
          </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
      </div>
    </section>
  </div>
  <section
    class="relative flex h-auto w-full overflow-clip bg-venti-blue bg-opacity-20 py-8 lg:h-screen lg:min-h-screen lg:py-0"
    id="">
    <div class="hidden h-full w-1/5 bg-venti-blue lg:block"
      style="background-image:url('{{ asset('images/batthern.png') }}')">

    </div>
    <div class="relative flex h-full w-full items-center justify-center lg:w-4/5"
      style="background-image: url('{{ asset('images/soft-wallpaper.png') }}')">
      <div class="absolute -bottom-[45%] right-0 -z-10 h-3/5 w-3/5 -skew-y-[40deg] bg-venti-blue bg-opacity-25">
      </div>
      <div class="mx-auto flex h-auto w-[95%] items-center justify-start md:w-[90%] lg:mx-0 lg:w-full">
        <div class="rounded-2xl bg-white shadow-2xl lg:ml-12 lg:h-[85%] lg:w-[85%]"
          style="background-image:url('{{ asset('images/cubes.png') }}')">
          <div class="flex h-auto w-full flex-col-reverse items-center py-8 lg:flex-row lg:py-16">
            <div
              class="flex h-auto w-full flex-shrink-0 flex-col-reverse justify-between px-4 md:flex-row-reverse md:gap-4 lg:w-3/5 lg:flex-col lg:gap-4 lg:pl-8 lg:pr-12">
              <div class="md:px-8 md:pt-12 lg:px-0 lg:pt-0">
                <div class="relative mt-2 pb-2 text-lg text-venti-blue lg:mt-0 lg:text-xl">
                  <h2 class="font-sans font-normal" id="subtitle1">Maximiser
                    l'efficacité, minimiser les coûts.</h2>
                  <h2 class="absolute left-0 top-0 font-sans font-normal" id="subtitle2"
                    style="opacity: 0;transform:translateX(10px);">Faire progresser les ventilations industrielles
                    respectueuses de l'environnement.</h2>
                  <h2 class="absolute left-0 top-0 font-sans font-normal" id="subtitle3"
                    style="opacity: 0;transform:translateX(10px);">Assurance qualité pour des solutions optimales en
                    matière d'air</h2>
                  <h2 class="absolute left-0 top-0 font-sans font-normal" id="subtitle4"
                    style="opacity: 0;transform:translateX(10px);">Un soutien fiable pour un confort d'air
                    ininterrompu.</h2>
                </div>
                <div class="relative text-2xl">
                  <h1 class="title pb-8 font-sans font-bold" id="title1">Efficacité pour des solutions
                    rentables</h1>
                  <h1 class="title absolute left-0 top-7 pb-8 font-sans font-bold" id="title2"
                    style="opacity: 0;transform:translateX(10px);">
                    Ventilations vertes : Des solutions respectueuses de l'environnement !</h1>
                  <h1 class="title absolute left-0 top-7 pb-8 font-sans font-bold" id="title3"
                    style="opacity: 0;transform:translateX(10px);">Ventilations de précision : Élever les normes de
                    l'industrie</h1>
                  <h1 class="title absolute left-0 top-5 pb-8 font-sans font-bold lg:top-2" id="title4"
                    style="opacity: 0;transform:translateX(10px);">Maintenance proactive : Assurer des performances de
                    pointe</h1>
                </div>
                <div class="relative pb-4 text-base">
                  <p class="content text-neutral-700" id="content1">Découvrez les avantages économiques de
                    nos systèmes de ventilation, car ils sont conçus non seulement pour améliorer l'environnement de
                    travail, mais aussi pour contribuer à des économies substantielles en termes de coûts
                    d'exploitation. En mettant l'accent sur la durabilité, nos installations visent à créer un espace de
                    travail sain et productif tout en garantissant une utilisation optimale des ressources.</p>
                  <p class="content absolute left-0 top-10 py-4 text-neutral-700" id="content2"
                    style="opacity: 0;transform:translateX(10px);">Explorez les avantages offerts par nos ventilations
                    écologiques, conçues pour minimiser l'impact sur l'environnement tout en garantissant une qualité
                    optimale de l'air intérieur. En tant que champions des pratiques écologiques, nos systèmes de
                    ventilation avancés sont conçus pour être efficaces sur le plan énergétique, réduisant ainsi
                    l'empreinte carbone des opérations industrielles.</p>
                  <p class="content absolute left-0 top-10 text-neutral-700" id="content3"
                    style="opacity: 0;transform:translateX(10px);">Découvrez l'avant-garde de l'innovation en matière
                    de chauffage, de ventilation et de climatisation grâce à nos systèmes conçus par des experts, où la
                    technologie de pointe rencontre des matériaux de qualité supérieure. Chez Ventisol,
                    nous donnons la priorité à l'assurance qualité, en veillant à ce que chaque unité quittant notre
                    chaîne de production réponde aux normes les plu s strictes pour des solutions d'air optimales.</p>
                  <p class="content absolute left-0 top-4 text-neutral-700" id="content4"
                    style="opacity: 0;transform:translateX(10px);">Chez Ventisol, nous comprenons que la clé d'une
                    performance CVC durable réside dans une maintenance proactive et experte. Notre engagement pour
                    votre confort va au-delà de l'installation - nous offrons des services de maintenance complets pour
                    garantir que vos systèmes CVC fonctionnent avec une efficacité maximale.</p>
                </div>
              </div>
              <div
                class="flew-row flex w-full items-center justify-between pt-5 md:flex-col md:gap-4 lg:flex-row lg:gap-0">
                <div
                  class="flex h-[70px] w-1/5 cursor-pointer items-center justify-center rounded-2xl bg-gradient-to-br from-venti-blue via-[#2a68a3] to-[#357cbe] shadow-2xl duration-200 hover:scale-110 md:h-[100px] md:w-[90%] lg:h-[90px] lg:w-1/5"
                  id="box1">
                  <img src="{{ asset('SVG/money-saving.svg') }}" alt="Vector Image of a piggy bank">
                </div>
                <div
                  class="h-[70px] w-1/5 cursor-pointer rounded-2xl bg-gradient-to-b from-venti-blue via-[#2a68a3] to-[#357cbe] shadow-2xl duration-200 hover:scale-110 md:h-[100px] md:w-[90%] lg:h-[90px] lg:w-1/5"
                  id="box2">
                  <img src="{{ asset('SVG/energy-efficient.svg') }}"
                    alt="Vector Image of a energy cord connected to a leaf">
                </div>
                <div
                  class="flex h-[70px] w-1/5 cursor-pointer items-center justify-center rounded-2xl bg-gradient-to-br from-venti-blue via-[#2a68a3] to-[#357cbe] shadow-2xl duration-200 hover:scale-110 md:h-[100px] md:w-[90%] lg:h-[90px] lg:w-1/5"
                  id="box3">
                  <img src="{{ asset('SVG/trusted-quality.svg') }}"
                    alt="Vector Image of a checkbox that looks like a fan">
                </div>
                <div
                  class="flex h-[70px] w-1/5 cursor-pointer items-center justify-center rounded-2xl bg-gradient-to-br from-venti-blue via-[#2a68a3] to-[#357cbe] shadow-2xl duration-200 hover:scale-110 md:h-[100px] md:w-[90%] lg:h-[90px] lg:w-1/5"
                  id="box4">
                  <img class="-translate-x-1" src="{{ asset('SVG/mainenance.svg') }}"
                    alt="Vector Image of a van with cogs in it">
                </div>
              </div>
            </div>
            <div
              class="relative mx-auto mb-6 flex h-[300px] w-[90%] flex-shrink-0 items-center justify-center rounded-2xl bg-gradient-to-bl from-venti-blue via-[#2b6ba7] to-[#2d73b4] shadow-inner drop-shadow-xl md:h-[350px] md:w-3/4 lg:mx-0 lg:-mr-12 lg:mb-0 lg:h-[400px] lg:w-1/2">
              <img
                class="aspect-auto w-full translate-x-3 skew-x-6 object-cover duration-300 lg:-ml-14 lg:h-full lg:w-auto lg:scale-[1.15]"
                id="image1" src="{{ asset('images/saveMoney.png') }}" alt="">
              <img class="w-full object-cover duration-300 lg:-ml-14 lg:h-auto lg:w-auto lg:scale-105" id="image2"
                src="{{ asset('images/theworld.png') }}" alt="" style="opacity: 0;display:none;">
              <img class="aspect-auto w-full object-cover duration-300 lg:h-auto lg:w-auto" id="image3"
                src="{{ asset('images/quality-check-image') }}.png" alt="" style="opacity: 0;display:none;">
              <img class="aspect-auto w-full object-cover duration-300 lg:-ml-14 lg:h-auto lg:w-auto lg:scale-105"
                id="image4" src="{{ asset('images/maintenance.png') }}" alt=""
                style="opacity: 0;display:none;">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section
    class="relative flex h-auto w-full overflow-clip bg-venti-blue bg-opacity-20 py-8 lg:h-auto lg:min-h-screen lg:py-0"
    id="" style="background-image: url('{{ asset('images/soft-wallpaper.png') }}')">
    <div class="hidden w-1/5 bg-venti-blue lg:block"
      style="background-image:url('{{ asset('images/batthern.png') }}')">
    </div>
    <div class="relative w-full lg:w-4/5">
      <div
        class="absolute -bottom-[55%] right-0 -z-10 h-3/5 w-3/5 -skew-y-[40deg] bg-venti-blue bg-opacity-20 lg:-bottom-[45%]">
      </div>
      <div class="absolute -top-[45%] left-0 -z-10 h-3/5 w-3/5 -skew-y-[40deg] bg-venti-blue bg-opacity-20">
      </div>
      <div class="w-full bg-venti-blue py-8" style="background-image:url('{{ asset('images/batthern.png') }}')">
      </div>
      <div class="mx-auto flex w-9/10 flex-col items-center p-4 lg:flex-row lg:p-8">
        <div class="w-full lg:w-3/5">
          <h1 class='roboto-medium p-6 text-5xl text-venti-blue'>À propos de Ventisol</h1>
          <div class="w-full skew-x-6 bg-venti-blue bg-opacity-75">
            <div class="relative w-full -skew-x-6 py-3">
              <div
                class="absolute left-0 top-0 z-10 h-full w-full bg-gradient-to-tr from-venti-blue via-[#23588961] to-transparent">
              </div>
              <img class="aspect-auto w-full object-cover shadow-2xl" src="{{ asset('images/gallery3.jpg') }}"
                alt="">
            </div>
          </div>
        </div>
        <div class="w-full lg:w-2/5 lg:pl-12">
          <p class="roboto-medium pb-4 pt-4 text-center text-2xl text-venti-blue lg:pt-6 lg:text-left">Définir la
            norme en matière
            de ventilation.</p>
          <p class="roboto text-lg lg:pt-4">Bienvenue chez Ventisol, où nous redéfinissons les références en matière de
            solutions de ventilation. Grâce
            à notre engagement en faveur d'une fabrication de précision, d'une production de qualité professionnelle,
            d'instal...</p>
          <a class="roboto-medium group mt-6 flex w-1/2 items-center justify-center rounded-xl bg-venti-blue px-2 py-2 text-white shadow-xl duration-150 hover:scale-105 active:scale-90 active:shadow-round active:shadow-venti-blue"
            href="{{ route('about') }}">Voir
            Plus
            <svg
              class="feather feather-chevron-right -translate-x-6 opacity-0 duration-150 group-hover:translate-x-2 group-hover:opacity-100"
              xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
              stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <polyline points="9 18 15 12 9 6"></polyline>
            </svg></a>
        </div>
      </div>
    </div>
  </section>
  @include('partials._footer')
  <script src="{{ asset('js/index.js') }}" defer></script>
  <script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>
  <script>
    var mySwiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      coverflowEffect: {
        rotate: 30,
        stretch: -20,
        depth: 120,
        modifier: 1,
        slideShadows: true,
      },
      lazy: {
        loadPrevNext: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        500: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 2,
        },
      },
    });
  </script>
</body>

</html>
