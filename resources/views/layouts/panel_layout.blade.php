@include('partials._head')
@livewireStyles

<body class="" style="overflow: hidden">
  <main class="roboto w-full bg-slate-300 antialiased">
    <div class="fixed left-0 top-0 z-50 flex h-full w-full items-center justify-center bg-venti-dark-tint"
      id="admin-loading-overlay">
      <div class="flex flex-col items-center">
        <h1 class="pb-2 text-5xl font-bold text-white">VENTISOL</h1>
        <p class="translate-y-2 text-xl font-normal text-indigo-400">Loading Admin Panel</p>
        <div class="ls-facebook">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
    @yield('content')
  </main>
  @livewireScripts
  <script>
    var isSidebarOpen = true;

    document.getElementById('toggle-side-panel').addEventListener('click', function() {
      isSidebarOpen = !isSidebarOpen;

      gsap.to('#content', {
        width: isSidebarOpen ? '80%' : '100%',
        duration: 0.15,
        ease: "back.in(4)",
      });
      gsap.to('#sidebar', {
        width: isSidebarOpen ? '20%' : '0%',
        duration: 0.15,
        ease: "back.in(4)",
      });
    });

    // Mobile menu
    var isMobileSideBarOpen = false;
    document.getElementById('toggle-mobile-side-panel').addEventListener('click', function() {
      isMobileSideBarOpen = !isMobileSideBarOpen;

      gsap.to('#sidebar', {
        width: isMobileSideBarOpen ? '85%' : '0%',
        duration: 0.15,
        ease: "back.in(4)",
      });
    });
    document.getElementById('close-mobile-panel').addEventListener('click', function() {
      isMobileSideBarOpen = !isMobileSideBarOpen;

      gsap.to('#sidebar', {
        width: isMobileSideBarOpen ? '85%' : '0%',
        duration: 0.15,
        ease: "back.in(4)",
      });
    });
    window.addEventListener("load", function() {
      var loadingOverlay = document.getElementById("admin-loading-overlay");
      loadingOverlay.style.display = "none";

      document.body.style.overflow = "visible";
    });
  </script>
</body>

</html>
