@include('partials._head')

<body>
  @include('partials._navbar')
  <main class="flex w-full">
    <div class="relative z-30 hidden w-1/5 bg-venti-blue shadow-2xl lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <section class="w-full bg-venti-blue bg-opacity-25 lg:w-4/5" id="project-section"
      style="background-image: url('{{ asset('images/batthern.png') }}');background-attachment: fixed;">
      <h1
        class="roboto-medium group flex w-full items-center bg-venti-blue py-2 pl-4 text-3xl text-white shadow-2xl md:text-4xl"
        style="background-image: url('{{ asset('images/batthern.png') }}')"><a
          class="flex items-center gap-1 duration-100 active:text-venti-blue" href="{{ url()->previous() }}">
          <svg
            class="feather feather-arrow-left duration-200 group-hover:-translate-x-2 group-hover:shadow-2xl group-active:scale-90 group-active:opacity-40"
            xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none"
            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <line x1="19" y1="12" x2="5" y2="12"></line>
            <polyline points="12 19 5 12 12 5"></polyline>
          </svg>Retour</a> <span class="roboto px-3">/</span>
        Projet Nr.{{ $project->id }}</h1>
      <div class="mx-auto w-full lg:w-4/5">
        <div
          class="mx-auto flex h-full w-9/10 flex-col bg-white bg-opacity-5 p-4 py-4 shadow-2xl backdrop-blur-sm lg:w-full">
          <div class="gallery h-[250px] w-full md:h-[450px]">
            <div class="swiper-container mx-auto h-[250px] w-full overflow-hidden md:h-[450px] md:w-[95%]">
              <div class="swiper-wrapper h-full w-full md:w-3/4">
                @php
                  $imageUrls = json_decode($project->images, true);
                @endphp

                @foreach ($imageUrls as $imageUrl)
                  <div class="swiper-slide h-full">
                    <a class="inline-block h-full w-full" href="{{ asset($imageUrl) }}">
                      <img class="h-full w-full object-cover shadow-2xl" src="{{ asset($imageUrl) }}" alt="">
                    </a>
                  </div>
                @endforeach
              </div>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>

          <div class="flex w-[9/10] flex-col justify-between px-2 pt-6 md:px-8">
            <div class="items-center justify-between md:flex">
              <p class="roboto-bold text-3xl text-venti-blue">{{ $project->title }}</p>
              <p class="flex items-center gap-2 pt-2 text-xl">
                <svg class="feather feather-map-pin flex-shrink-0" xmlns="http://www.w3.org/2000/svg" width="20"
                  height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                  stroke-linecap="round" stroke-linejoin="round">
                  <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                  <circle cx="12" cy="10" r="3"></circle>
                </svg>{{ $project->location }}
              </p>
            </div>
            <div class="flex h-full flex-col justify-between pt-4">
              <p class="flex items-center gap-2 text-xl">
                <svg class="feather feather-briefcase flex-shrink-0" xmlns="http://www.w3.org/2000/svg" width="20"
                  height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                  stroke-linecap="round" stroke-linejoin="round">
                  <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
                  <path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
                </svg>{{ $project->company }}
              </p>
              <p class="pt-10">{{ $project->content }}</p>
              <div class="items-center justify-between pt-10 md:flex">
                <p class="mb-6 flex items-center gap-2 text-xl text-venti-blue md:mb-0">
                  <svg class="feather feather-calendar flex-shrink-0" xmlns="http://www.w3.org/2000/svg" width="20"
                    height="20" viewBox="0 0 24 24" fill="none" stroke="black" stroke-width="2"
                    stroke-linecap="round" stroke-linejoin="round">
                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                    <line x1="16" y1="2" x2="16" y2="6"></line>
                    <line x1="8" y1="2" x2="8" y2="6"></line>
                    <line x1="3" y1="10" x2="21" y2="10"></line>
                  </svg>{{ \Carbon\Carbon::parse($project->finished_at)->format('d-m-Y') }}
                </p>
                <a class="roboto-medium rounded-3xl bg-venti-blue px-8 py-2 text-lg text-white md:my-0"
                  href="{{ $project->company_url }}"
                  style="background-image:url('{{ asset('images/worn-dots.png') }}')" target="_blank">Voir le site de
                  l'entreprise</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  @include('partials._footer')
  <script src="{{ asset('js/projects.js') }}"></script>
  <script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var mySwiper = new Swiper('.swiper-container', {
        slidesPerView: 1.2,
        spaceBetween: 12,
        effect: 'coverflow',
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        breakpoints: {
          500: {
            slidesPerView: 1,
          },
          768: {
            slidesPerView: 1.1,
          },
          1024: {
            slidesPerView: 1.2,
          },
        },
      });
    });
  </script>
</body>

</html>
