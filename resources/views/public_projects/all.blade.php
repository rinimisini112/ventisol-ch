@include('partials._head')

<body>
  @include('partials._navbar')
  <main class="flex w-full">
    <div class="relative z-30 hidden w-1/5 bg-venti-blue shadow-2xl lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <section class="w-full bg-venti-blue bg-opacity-25 lg:w-4/5" id="project-section"
      style="background-image: url('{{ asset('images/batthern.png') }}');background-attachment: fixed;">
      <div class="flex w-full items-center justify-between bg-venti-blue py-2"
        style="background-image: url('{{ asset('images/batthern.png') }}')">
        <h1 class="roboto-medium flex items-center justify-between pl-4 text-4xl text-white shadow-2xl"><a
            class="group flex items-center" href="/projects">
            <svg
              class="feather feather-arrow-left duration-200 group-hover:-translate-x-2 group-hover:shadow-2xl group-active:scale-90 group-active:opacity-40"
              xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none"
              stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <line x1="19" y1="12" x2="5" y2="12"></line>
              <polyline points="12 19 5 12 12 5"></polyline>
            </svg>Retour</a> <span class="roboto px-3">/</span>
          Tous les projets</h1>
        <form class="mr-6 w-2/5" action="/projects/search">
          <label class="relative flex w-full items-center" for="search">
            <input class="w-3/4 rounded-3xl border-none py-1 pl-3 text-venti-blue outline-none" id="search"
              name="q" type="text" placeholder="Recherche de projets">
            <button
              class="relative z-10 flex w-[30%] -translate-x-8 items-center rounded-3xl border-4 border-venti-blue bg-white px-4 py-1 text-venti-blue duration-150 hover:scale-105 active:scale-90">Recherche<svg
                class="feather feather-search translate-y-px" xmlns="http://www.w3.org/2000/svg" width="20"
                height="20" viewBox="0 0 24 24" fill="none" stroke="#235789" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round">
                <circle cx="11" cy="11" r="8"></circle>
                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
              </svg></button>
          </label>
        </form>
      </div>
      <div class="mx-auto w-full lg:w-9/10">
        <div class="gallery">
          @foreach ($projects as $index => $project)
            @if ($index % 2 != 0)
              <x-project-card :project="$project" flexDirectionStyle="md:flex-row-reverse" />
            @else
              <x-project-card :project="$project" />
            @endif
          @endforeach
        </div>
      </div>
      {{ $projects->links() }}
    </section>
  </main>
  @include('partials._footer')
  <script src="{{ asset('js/projects.js') }}"></script>
  <script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>
</body>

</html>
