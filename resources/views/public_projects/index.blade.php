@include('partials._head')
<style>
  /* Styles for Swiper Navigation */
  .swiper-button-next,
  .swiper-button-prev {
    width: 60px;
    height: 60px;
    background-color: #235889a5;
    /* Background color */
    color: #fff;
    /* Text color */
    border-radius: 50%;
    /* Rounded corners for a circular button */
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 20px;
    cursor: pointer;
    transition: background-color 0.3s ease;
    /* Smooth transition on hover */
  }

  /* Hover effect */
  .swiper-button-next:hover,
  .swiper-button-prev:hover {
    background-color: #235789;
  }

  /* Additional styles for pagination */
  .swiper-pagination {
    position: absolute;
    bottom: 10px;
    left: 50%;
    transform: translateX(-50%);
  }

  .swiper-pagination-bullet {
    width: 10px;
    height: 10px;
    background-color: #777;
    opacity: 0.7;
    margin: 0 8px;
    border-radius: 50%;
    cursor: pointer;
    transition: background-color 0.3s ease;
    /* Smooth transition on hover */
  }

  .swiper-pagination-bullet-active {
    background-color: #fff;
    /* Active bullet color */
  }
</style>

<body>
  @include('partials._navbar')
  <main class="flex w-full">
    <div class="relative z-30 hidden w-1/5 bg-venti-blue shadow-2xl lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <section class="w-full bg-venti-blue bg-opacity-25 lg:w-4/5" id="project-section"
      style="background-image: url('{{ asset('images/batthern.png') }}');background-attachment: fixed;">
      <h1 class="roboto-medium w-full bg-venti-blue py-2 pl-4 text-4xl text-white shadow-2xl"
        style="background-image: url('{{ asset('images/batthern.png') }}')">Références <span class="roboto">/</span>
        Projets récents</h1>
      <div class="mx-auto w-full lg:w-9/10">
        <div class="gallery">
          @foreach ($projects as $index => $project)
            @if ($index === 1)
              <x-project-card :project="$project" flexDirectionStyle="md:flex-row-reverse" />
            @else
              <x-project-card :project="$project" />
            @endif
          @endforeach
        </div>
        <div class="flex w-full flex-col items-center justify-center gap-5 py-12 text-2xl lg:flex-row">
          <p class="roboto-medium text-4xl text-venti-blue">Il y en a d'autres!</p>
          <a class="roboto-medium opacity-85 rounded-3xl bg-venti-blue px-12 py-2 text-white duration-100 ease-linear hover:scale-105 hover:opacity-100"
            href="{{ route('projects.all') }}"
            style="background-image: url('{{ asset('images/white-diamond.png') }}')">Tout voir</a>
        </div>
      </div>
      <div class="flex h-[350px] w-full flex-col bg-venti-blue bg-opacity-25 pb-6 backdrop-blur-sm">
        <h1 class="roboto-bold mb-2 h-[20%] py-4 text-center text-5xl text-venti-blue">Nos clients</h1>
        <div class="swiper-container mx-auto h-[80%] w-[95%] overflow-hidden">
          <div class="swiper-wrapper flex h-full items-center pb-4">
            @forelse ($references as $reference)
              <a class="swiper-slide" href="{{ $reference->company_website_url }}" target="_blank">
                <div
                  class="group relative flex h-full w-full items-end justify-center overflow-clip rounded-3xl bg-white shadow-2xl backdrop-blur-sm">
                  <img
                    class="absolute left-0 top-0 h-full w-full object-cover duration-300 ease-out group-hover:scale-110"
                    src="{{ asset($reference->company_logo) }}" alt="Logo of referencing company">
                  <div
                    class="absolute left-0 top-0 z-10 h-full w-full bg-gradient-to-b from-transparent via-[#00000056] to-[#000000ba] duration-300 ease-in group-hover:opacity-0">
                  </div>
                  <div class="relative z-10 pb-3 text-xl text-white">{{ $reference->company_name }}</div>
                </div>
              </a>
            @empty
              <p>No references added yet, Stay tuned</p>
            @endforelse

          </div>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
      </div>
    </section>
  </main>
  @include('partials._footer')
  <script src="{{ asset('js/projects.js') }}"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        slidesPerView: 3,
        spaceBetween: 16,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        breakpoints: {
          320: {
            watchSlidesProgress: true,
            slidesPerView: "1.2",
            shortSwipes: false
          },
          500: {
            watchSlidesProgress: true,
            slidesPerView: "2.3",
            shortSwipes: false
          },
          1024: {
            preventInteractionOnTransition: true,
            watchSlidesProgress: true,
            slidesPerView: "2.6",
            shortSwipes: false
          },
        },
      });
    });
  </script>
</body>

</html>
