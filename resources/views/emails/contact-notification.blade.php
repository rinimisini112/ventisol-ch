<p style="font-size: 24px; font-weight:700;font-family:Arial, Helvetica, sans-serif;">Nouveau message reçu dans Ventisol
</p>
<p style="padding-top:5px">Message de {{ $contact->first_name . ' ' . $contact->last_name }}</p>
<p><strong>Sujet: </strong> {{ $contact->subject }} </p>
<p><strong>Téléphone: </strong> {{ $contact->phone_number }} </p>
<br>
<br>
<p>Vous pouvez vous connecter au panneau d'administration et afficher le message complet.</p>
<a href="{{ route('index') }}">Voir le message</a>
