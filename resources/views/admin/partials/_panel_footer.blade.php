<footer class="flex w-full items-center justify-between bg-venti-dark-tint px-8 py-4 text-gray-300" id="footer"
  style="font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;">
  <h1>Logged in as : Admin</h1>
  <p>Ventisol&trade; 2024 &copy;All Rights Reserved</p>
</footer>
<script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('baguettebox.js/dist/baguetteBox.min.js') }}"></script>
