@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'messages'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/messages'" :breadcrumbSlot="'Messages'" :breadcrumbFinal="''">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/messages'" :breadcrumbSlot="'Messages'" :breadcrumbFinal="''">
      </x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-9/10 rounded-md">
        @livewire('message-contact-search')
      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>

  <script>
    var isSidebarOpen = true;

    document.getElementById('toggle-side-panel').addEventListener('click', function() {
      isSidebarOpen = !isSidebarOpen;

      gsap.to('#content', {
        width: isSidebarOpen ? '80%' : '100%',
        duration: 0.15,
        ease: "back.in(4)",
      });
      gsap.to('#sidebar', {
        width: isSidebarOpen ? '20%' : '0%',
        duration: 0.15,
        ease: "back.in(4)",
      });
    });
  </script>
@endsection
