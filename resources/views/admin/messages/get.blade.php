@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'messages'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/messages'" :breadcrumbSlot="'Messages'" :breadcrumbFinal="'Message'"></x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/messages'" :breadcrumbSlot="'Messages'"
        :breadcrumbFinal="'Message'"></x-mobile-panel-navbar>

      <div
        class="relative z-20 mx-auto my-8 min-h-screen w-[95%] rounded-md bg-neutral-200 text-venti-dark shadow-xl md:w-[85%] lg:w-4/5">
        <div
          class="flex flex-col items-center justify-between gap-4 rounded-t-md border-b-2 border-venti-dark bg-venti-dark-tint p-4 text-2xl text-white shadow-xl md:flex-row lg:gap-0">
          <p class="">Message From : {{ $contact->first_name . ' ' . $contact->last_name }}</p>
          <p class="text-xl">
            Recieved on : <span class="text-green-400">{{ $contact->created_at }}</span>
          </p>
        </div>
        <p class="roboto-bold pt-6 text-center text-2xl">Contact Information</p>
        <div class="px-4 pb-6 pt-10 text-xl">
          <div class="flex items-center gap-2 lg:px-4">
            <svg class="feather feather-alert-circle" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
              viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round">
              <circle cx="12" cy="12" r="10"></circle>
              <line x1="12" y1="8" x2="12" y2="12"></line>
              <line x1="12" y1="16" x2="12.01" y2="16"></line>
            </svg>
            <p class="flex flex-col items-start gap-y-2 lg:block">
              Issue client selected : <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->issue->issue_name }}</span>
            </p>
          </div>
          <div class="flex items-center gap-2 py-6 lg:px-4">
            <svg class="feather feather-map-pin" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
              viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round">
              <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
              <circle cx="12" cy="10" r="3"></circle>
            </svg>
            <p class="flex flex-col items-start lg:block">Region of client : <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->region->region_short }}</span>
              --
              <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->region->region_long }}</span>
            </p>
          </div>
          <div class="flex items-center gap-2 pb-6 lg:px-4">
            <svg class="feather feather-user" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
              viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round">
              <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
              <circle cx="12" cy="7" r="4"></circle>
            </svg>
            <p class="flex flex-col items-start lg:block">Client's Full Name : <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->first_name . ' ' . $contact->last_name }}</span>
            </p>
          </div>
          <div class="flex items-center gap-2 pb-6 lg:px-4">
            <svg class="feather feather-briefcase" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
              viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round">
              <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
              <path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
            </svg>
            <p class="flex flex-col items-start lg:block">Client's Company / <span class="text-venti-dark-tint">if
                specified :</span><span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner drop-shadow-sm">
                @if ($contact->company)
                  {{ $contact->company }}
                @else
                  N/A - Not specified
                @endif
              </span></p>
          </div>
          <div class="flex items-center gap-2 pb-6 lg:px-4">
            <svg class="feather feather-home" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
              viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round">
              <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
              <polyline points="9 22 9 12 15 12 15 22"></polyline>
            </svg>
            <p class="flex flex-col items-start lg:block">Client's Address : <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->address }}</span>
            </p>
          </div>
          <div class="flex items-center gap-2 pb-6 lg:px-4">
            <?xml version="1.0" encoding="utf-8"?>
            <!-- Generator: Adobe Illustrator 28.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg id="Layer_1" style="enable-background:new 0 0 44.9 38.2;" width='29' height='29'
              version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
              y="0px" viewBox="0 0 44.9 38.2" xml:space="preserve">
              <g>
                <path style="fill:none;stroke:gray;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
                  d="M29.4,4.8h9.3
                                                                                          c2.2,0,4.1,1.8,4.1,4.1v24.4c0,2.2-1.8,4.1-4.1,4.1H6.1c-2.2,0-4.1-1.8-4.1-4.1V8.9c0-2.2,1.8-4.1,4.1-4.1h9.5" />
                <polyline style="fill:none;stroke:gray;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
                  points="42.7,8.9 
                    22.4,23.1 2,8.9 	" />
                <rect style="fill:none;" x="15.3" y="2.5" width="14.3" height="13.5" />

                <text
                  style="stroke:gray;stroke-width:0.25;stroke-miterlimit:10; font-family:'MyriadPro-Regular'; font-size:6px;"
                  transform="matrix(1 0 0 1 16.2761 11.7175)">XXX</text>
              </g>
              <circle style="fill:none;stroke:gray;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;"
                cx="22.5" cy="9.8" r="8.2" />
              <rect style="fill:none;" x="7" y="26.3" width="30.9" height="7.6" />
              <text
                style="stroke:gray;stroke-width:0.25;stroke-miterlimit:10; font-family:'MyriadPro-Regular'; font-size:9px;"
                transform="matrix(1 0 0 1 15.6075 33.3148)">ZIP</text>
            </svg>

            <p class="flex flex-col items-start lg:block">Client's Postal Code and Locality : <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->postal_code . ' - ' . $contact->city }}</span>
            </p>
          </div>
          <div class="flex items-center gap-2 pb-6 lg:px-4">
            <svg class="feather feather-mail" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
              viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round">
              <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
              <polyline points="22,6 12,13 2,6"></polyline>
            </svg>
            <p class="flex flex-col items-start lg:block">E-Mail : <a
                class="roboto-medium rounded-xl bg-neutral-300 px-3 py-1 text-blue-500 shadow-inner duration-200 hover:text-blue-600 hover:underline hover:underline-offset-2"
                href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></p>
          </div>
          <div class="flex items-center gap-2 pb-6 lg:px-4">
            <svg class="feather feather-phone-forwarded" xmlns="http://www.w3.org/2000/svg" width="30"
              height="30" viewBox="0 0 24 24" fill="none" stroke="gray" stroke-width="2"
              stroke-linecap="round" stroke-linejoin="round">
              <polyline points="19 1 23 5 19 9"></polyline>
              <line x1="15" y1="5" x2="23" y2="5"></line>
              <path
                d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z">
              </path>
            </svg>
            <p class="flex flex-col items-start lg:block">Phone Number : <span
                class="roboto-medium rounded-xl bg-neutral-300 px-4 py-2 shadow-inner">{{ $contact->phone_number }}</span>
            </p>
          </div>
          <div class="px-4 py-2">
            <p class="roboto-medium rounded-xl bg-slate-300 p-4 shadow-inner">Subject : <span
                class="roboto block pl-3 pt-1">•{{ $contact->subject }}</span></p>
          </div>
          <div class="px-4 py-2">
            <p class="roboto-medium rounded-xl bg-slate-300 p-4 shadow-inner">Message : <span
                class="roboto block pl-3 pt-1">•{{ $contact->message }}
              </span></p>
          </div>
        </div>
      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>

  <script>
    var isSidebarOpen = true;

    document.getElementById('toggle-side-panel').addEventListener('click', function() {
      isSidebarOpen = !isSidebarOpen;

      gsap.to('#content', {
        width: isSidebarOpen ? '80%' : '100%',
        duration: 0.15,
        ease: "back.in(4)",
      });
      gsap.to('#sidebar', {
        width: isSidebarOpen ? '20%' : '0%',
        duration: 0.15,
        ease: "back.in(4)",
      });
    });
  </script>
@endsection
