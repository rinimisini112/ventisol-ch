@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'dashboard'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="''" :breadcrumbSlot="''" :breadcrumbFinal="''">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="''" :breadcrumbSlot="''" :breadcrumbFinal="''">
      </x-mobile-panel-navbar>

      <div class="relative z-10 flex items-center justify-end">
        <form class="p-6" action="{{ route('admin.dashboard') }}" method="post">
          @csrf
          <label for="timePeriod">Select Time Period:</label>
          <select id="timePeriod" name="timePeriod">
            <option value="7" @if (old('timePeriod', 7) == 7) selected @endif>Past 7 days</option>
            <option value="30" @if (old('timePeriod', 7) == 30) selected @endif>Past month</option>
            <option value="180" @if (old('timePeriod', 7) == 180) selected @endif>Past 6 months</option>
          </select>
          <button class="rounded-lg bg-venti-dark px-2 py-1.5 text-white" type="submit">Get Data</button>
        </form>
      </div>

      <div class="relative z-10 mb-8 mt-4 flex w-full flex-col items-start justify-start gap-6 px-6 lg:flex-row">
        <div
          class="w-full rounded-2xl bg-gradient-to-bl from-indigo-700 via-indigo-600 to-indigo-500 px-2 py-4 shadow-2xl lg:w-[35%]">
          <h1 class="py-1 pl-4 text-white">Total Visitors : {{ $totalVisitors }}</h1>
          <canvas id="visitorsChart"></canvas>
        </div>
        <div
          class="w-full rounded-2xl bg-gradient-to-tl from-red-700 via-red-600 to-red-500 px-2 py-4 shadow-2xl lg:w-[35%]">
          <h1 class="py-1 pl-4 text-white">Page Views
          </h1>
          <canvas id="pageViewsChart"></canvas>
        </div>
        <div
          class="w-full rounded-2xl bg-gradient-to-tl from-cyan-700 via-cyan-600 to-cyan-500 px-2 py-4 shadow-2xl lg:w-1/4">
          <h1 class="py-1 pl-4 text-white">New vs Returning Users</h1>
          <canvas id="newVsReturningChart"></canvas>
        </div>
      </div>
      <div class="flex w-full justify-center px-5 lg:px-0">
        <div
          class="w-full rounded-2xl bg-gradient-to-tl from-violet-600 via-violet-500 to-violet-400 px-2 py-4 shadow-2xl lg:w-3/5">
          <h1 class="py-1 pl-4 text-white">Visitors by country</h1>
          <canvas id="countriesChart"></canvas>
        </div>
        <div class="hidden pl-4 lg:block">
          <h2 class="roboto-medium text-3xl text-venti-dark-tint">Showing visitors countries</h2>
          @foreach ($countries as $country)
            <p class="py-2 text-venti-dark">Country and views - {{ $country['country'] }} :
              {{ $country['screenPageViews'] }} Views</p>
          @endforeach
        </div>
      </div>

      <div
        class="chart-container mx-auto my-8 w-full rounded-xl bg-gradient-to-tl from-venti-blue via-[#235889bc] to-[#235889b8] p-6 lg:w-4/5">
        <canvas id="analyticsChart"></canvas>
      </div>
      @include('admin.partials._panel_footer')
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var newVsReturningData = @json($newVsReturningUsers);

      var ctx = document.getElementById('newVsReturningChart').getContext('2d');

      var chart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ['New Users', 'Returning Users'],
          datasets: [{
            data: [newVsReturningData[0]['activeUsers'], newVsReturningData[1]['activeUsers']],
            backgroundColor: ['rgba(255, 99, 133, 0.8)', 'rgba(54, 235, 75, 0.7)'],
            borderColor: ['rgba(255, 99, 133, 1)', 'rgba(54, 235, 75, 1)'],
            borderWidth: 1
          }]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              labels: {
                color: 'rgb(255, 255, 255)',
                font: {
                  size: 16
                },
              }
            }
          }
        }
      });
    });

    var countries = @json($countries);

    var countryLabels = countries.map(country => country['country']);
    var pageViewsData = countries.map(country => country['screenPageViews']);


    var ctx = document.getElementById('countriesChart').getContext('2d');

    var chart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: countryLabels,
        datasets: [{
          label: 'Screen Page Views',
          data: pageViewsData,
          backgroundColor: 'rgba(255, 255, 255, 0.3)',
          borderColor: 'white', // Adjust alpha for borderColor
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        plugins: {
          legend: {
            labels: {
              color: 'rgb(255, 255, 255)',
              font: {
                size: 16
              },
            }
          }
        },
        scales: {
          x: {
            ticks: {
              color: 'rgba(255, 255, 255, 1)'
            }
          },
          y: {
            ticks: {
              color: 'rgba(255, 255, 255, 1)'
            }
          }
        }
      }
    });
    document.addEventListener('DOMContentLoaded', function() {
      var dailyAnalyticsData = @json($analyticsData);
      // Extract data for each day
      var visitorsData = dailyAnalyticsData.map(item => item['activeUsers']);
      var pageViewsData = dailyAnalyticsData.map(item => item['screenPageViews']);
      var labels = dailyAnalyticsData.map(item => new Date(item['date']).toLocaleDateString('en-US', {
        month: '2-digit',
        day: '2-digit'
      }));

      initializeLineChart('visitorsChart', 'Total Visitors', labels, visitorsData);
      initializeLineChart('pageViewsChart', 'Total Page Views', labels, pageViewsData);

      function initializeLineChart(chartId, label, labels, data) {
        var ctx = document.getElementById(chartId).getContext('2d');

        var chartData = Array.isArray(data) ? data : [data]; // Ensure data is an array

        var chart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: labels, // Use formatted date values as labels
            datasets: [{
              label: label,
              data: chartData,
              borderColor: 'rgba(255, 255, 255, 1)',
              borderWidth: 1,
              pointBackgroundColor: 'rgba(255, 255, 255, 1)',
              pointBorderColor: 'rgba(255, 255, 255, 1)',
              pointHoverBackgroundColor: 'rgba(255, 255, 255, 1)',
              pointHoverBorderColor: 'rgba(255, 255, 255, 1)'
            }]
          },
          options: {
            plugins: {
              legend: {
                labels: {
                  color: 'rgb(255, 255, 255)',
                  font: {
                    size: 16
                  },
                }
              }
            },
            scales: {
              x: {
                ticks: {
                  color: 'rgba(255, 255, 255, 1)'
                }
              },
              y: {
                ticks: {
                  color: 'rgba(255, 255, 255, 1)'
                }
              }
            }
          }
        });
      }
    });
    document.addEventListener('DOMContentLoaded', function() {
      var ctx = document.getElementById('analyticsChart').getContext('2d');
      var analyticsData = @json($pagesData); // Assuming $analyticsData is the data from your controller

      var chart = new Chart(ctx, {
        type: 'bar', // Choose the chart type (bar, line, pie, etc.)
        data: {
          labels: analyticsData.map(item => item.fullPageUrl),
          datasets: [{
            label: 'Top 5 most viewed pages',
            data: analyticsData.map(item => item.screenPageViews),
            backgroundColor: 'rgba(255, 255, 255, 0.4)', // Customize colors
            borderColor: 'rgb(255, 255, 255)',
            borderWidth: 1
          }]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              labels: {
                color: 'rgb(255, 255, 255)',
                font: {
                  size: 16
                },
              }
            }
          },
          scales: {
            x: {
              ticks: {
                color: 'rgba(255, 255, 255, 1)'
              }
            },
            y: {
              ticks: {
                color: 'rgba(255, 255, 255, 1)'
              }
            }
          }
        }
      });
    });
  </script>
@endsection
