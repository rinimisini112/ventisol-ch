@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'projects'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-notification-popup>Project edited succesfully!</x-notification-popup>
      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/projects'" :breadcrumbSlot="'Projects'" :breadcrumbFinal="'View Project'">
        @forelse ($messageNotifications as $notification)
          @if ($notification->is_viewed === 0)
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-700 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have a new message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @else
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-500 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have read this message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @endif
        @empty
          <p class="text-gray-300">No new notifications</p>
        @endforelse
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/projects'" :breadcrumbSlot="'Projects'" :breadcrumbFinal="'View Project'">
        @forelse ($messageNotifications as $notification)
          @if ($notification->is_viewed === 0)
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-700 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have a new message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @else
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-500 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have read this message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @endif
        @empty
          <p class="text-gray-300">No new notifications</p>
        @endforelse
      </x-mobile-panel-navbar>
      <div class="relative z-20 mx-auto my-8 w-9/10 rounded-xl bg-neutral-300 py-8 shadow-xl">
        <div class="flex flex-col items-center justify-between px-8 lg:flex-row">
          <h1 class="roboto-medium text-2xl text-venti-black">Viewing project Nr.{{ $project->id }}</h1>
          <div class="flex flex-col items-center gap-4 text-lg text-white lg:flex-row">
            <a class="rounded-xl bg-indigo-500 px-12 py-2 shadow-lg duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
              href="/dashboard/projects/{{ $project->id }}/edit">Edit Project</a>
            @livewire('delete-confirm-modal', ['projectId' => $project->id])
          </div>
        </div>
        <div class="flex flex-col gap-4 px-6 pt-8 lg:flex-row lg:gap-0">
          <form class="w-full text-venti-dark lg:w-3/5 lg:pr-6">
            @csrf
            <div class="mt-2 w-full">
              <label class="text-xl" for="title">Project Title :
                <x-text-input-white class="mt-0.5 block w-full" id="title" name='title' type='text'
                  value="{{ $project->title }}" :disabled='true' placeholder='Enter project title'></x-text-input-white>
                @error('title')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
            <div class="mt-2 w-full">
              <label class="text-xl" for="location">Location
                <x-text-input-white class="mt-0.5 block w-full lg:w-4/5" id="location" name='location' type='text'
                  value="{{ $project->location }}" :disabled='true' placeholder="Project location"
                  autofocus></x-text-input-white>
                @error('location')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
            <div class="mt-2 w-full">
              <label class="text-xl" for="company">Company / Client name
                <x-text-input-white class="mt-0.5 block w-full lg:w-4/5" id="company" name='company' type='text'
                  value="{{ $project->company }}" :disabled='true' placeholder="Project company / client"
                  autofocus></x-text-input-white>
                @error('company')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
            <div class="mt-2 w-full">
              <label class="text-xl" for="company_url">Company Website Link / Url
                <x-text-input-white class="mt-0.5 block w-full lg:w-4/5" id="company_url" name='company_url'
                  type='text' value="{{ $project->company_url }}" :disabled='true'
                  placeholder="Company website link" autofocus></x-text-input-white>
                @error('company_url')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
            <div class="mt-2 w-full">
              <label class="text-xl" for="finished_at">Project finished at :
                <x-text-input-white class="mt-0.5 block w-full border border-indigo-400 pl-2 text-base lg:w-4/5"
                  id="finished_at" name='finished_at' type='datetime-local' value="{{ $project->finished_at }}"
                  :disabled='true' autofocus></x-text-input-white>
                @error('finished_at')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
            <div class="mt-2 w-full">
              <label class="text-xl" for="content">Content
                <textarea
                  class="rounded-md' min-h-40 block w-full min-w-full max-w-full border-indigo-400 bg-gray-300 text-venti-dark shadow-inner duration-200 focus:border-indigo-400 focus:ring-indigo-400 dark:focus:border-indigo-400 dark:focus:ring-indigo-400"
                  id="content" name="content" disabled placeholder="Project content">{{ $project->content }}</textarea>
                @error('content')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
          </form>
          <div
            class="bg-opacity-65 flex min-h-screen w-full flex-col items-center gap-4 rounded-xl bg-neutral-400 pb-8 pt-4 shadow-inner drop-shadow-lg lg:w-2/5">
            @php
              $images = json_decode($project->images);
            @endphp
            @foreach ($images as $index => $image)
              <p class="text-venti-dark">Preview of image {{ $index + 1 }}</p>
              <img class="max-h-[250px] w-9/10 object-cover" src="{{ asset($image) }}" alt="Image 1 Preview">
            @endforeach
          </div>

        </div>

      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
