@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'projects'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/projects'" :breadcrumbSlot="'Projects'" :breadcrumbFinal="'Add Project'">
        @forelse ($messageNotifications as $notification)
          @if ($notification->is_viewed === 0)
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-700 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have a new message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @else
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-500 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have read this message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @endif
        @empty
          <p class="text-gray-300">No new notifications</p>
        @endforelse
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/projects'" :breadcrumbSlot="'Projects'" :breadcrumbFinal="'Add Project'">
        @forelse ($messageNotifications as $notification)
          @if ($notification->is_viewed === 0)
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-700 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have a new message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @else
            <a href="/dashboard/message/{{ $notification->id }}">
              <div class="border-b border-b-neutral-300 bg-neutral-500 px-4 py-2 duration-100 hover:bg-neutral-600">
                <span class="flex items-center justify-between text-gray-300"><span>You have read this message</span>
                  •{{ $notification->created_at->diffForHumans() }}</span>
                <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
                <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
              </div>
            </a>
          @endif
        @empty
          <p class="text-gray-300">No new notifications</p>
        @endforelse
      </x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-9/10 rounded-xl bg-neutral-300 py-8 shadow-xl">
        <div class="flex flex-col-reverse items-center justify-between gap-2 px-14 lg:flex-row lg:gap-0">
          <h1 class="roboto-medium text-2xl text-venti-black">Create a new Project</h1>
          <a class="rounded-xl bg-indigo-500 px-14 py-3 text-white duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
            href="{{ route('projects.index') }}">Go Back</a>
        </div>
        @livewire('image-preview-box')

      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
