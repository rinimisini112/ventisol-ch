@extends('layouts.panel_layout')
@section('content')
  <div>

    <x-admin-side-panel :activeLink="'regions'"></x-admin-side-panel>

    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-notification-popup>Region form submited succesfully!</x-notification-popup>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('regions.index')" :breadcrumbSlot="'Regions'" :breadcrumbFinal="'Region'"></x-panel-navbar>

      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('regions.index')" :breadcrumbSlot="'Regions'"
        :breadcrumbFinal="'Region'"></x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-4/5 rounded-xl bg-neutral-300 py-8 shadow-xl lg:w-3/5">

        <div class="flex flex-col items-center justify-between gap-4 px-4 lg:flex-row lg:gap-0 lg:px-8">
          <h1 class="roboto-medium text-2xl text-venti-black">Viewing Region</h1>
          <div class="flex flex-col items-center gap-3 text-lg text-white lg:flex-row lg:gap-4">
            <a class="rounded-xl bg-indigo-500 px-12 py-2 shadow-lg duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
              href="{{ route('regions.edit', [$region->id]) }}">Edit Region</a>
            @livewire('confirm-region-deletion', ['regionId' => $region->id])
          </div>
        </div>

        <div class="flex px-6 pt-8">
          <form class="mx-auto w-9/10 py-8 text-venti-dark lg:w-3/4 lg:pr-6">
            @csrf
            <div class="mt-2 w-full">
              <label class="text-xl" for="region_long">Region Long Name :
                <x-text-input-white class="mt-0.5 block w-full" id="region_long" name='region_long' type='text'
                  value="{{ $region->region_long }}" readonly
                  placeholder='Example - Neuchâtel-Jura-Fribourg'></x-text-input-white>
                @error('region_long')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
            <div class="mt-4 w-full">
              <label class="text-xl" for="region_short">Region Short Name :
                <x-text-input-white class="mt-0.5 block w-full" id="region_short" name='region_short' type='text'
                  value="{{ $region->region_short }}" placeholder="Example - NE-JU-FR" readonly
                  autofocus></x-text-input-white>
                @error('region_short')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
          </form>
        </div>

      </div>

      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
