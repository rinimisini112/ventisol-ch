@extends('layouts.panel_layout')
@section('content')
  <div>

    <x-admin-side-panel :activeLink="'regions'"></x-admin-side-panel>

    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('regions.index')" :breadcrumbSlot="'Regions'" :breadcrumbFinal="'Add Region'"></x-panel-navbar>

      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('regions.index')" :breadcrumbSlot="'Regions'"
        :breadcrumbFinal="'Add Region'"></x-mobile-panel-navbar>

      <div class="flex flex-col gap-4 px-4 lg:flex-row lg:px-8">

        <div class="relative z-20 mx-auto my-8 w-full rounded-xl bg-neutral-300 py-8 shadow-xl lg:w-1/2">

          <div class="flex flex-col-reverse items-center justify-between gap-4 px-4 lg:flex-row lg:gap-0 lg:px-14">
            <h1 class="roboto-medium text-2xl text-venti-black">Add a new Region</h1>
            <a class="place-self-end rounded-xl bg-indigo-500 px-14 py-3 text-white duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500 lg:place-self-auto"
              href="{{ route('regions.index') }}">Go Back</a>
          </div>

          <div class="flex px-6 pt-8">
            <form class="mx-auto w-9/10 text-venti-dark lg:pr-6" action="{{ route('regions.index') }}" method="POST">
              @csrf
              <div class="mt-2 w-full">
                <label class="text-xl" for="region_long">Region Long Name :
                  <x-text-input-white class="mt-0.5 block w-full" id="region_long" name='region_long' type='text'
                    :value="old('region_long')" placeholder='Example - Neuchâtel-Jura-Fribourg'></x-text-input-white>
                  @error('region_long')
                    <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                  @enderror
                </label>
              </div>
              <div class="mt-4 w-full">
                <label class="text-xl" for="region_short">Region Short Name :
                  <x-text-input-white class="mt-0.5 block w-full" id="region_short" name='region_short' type='text'
                    placeholder="Example - NE-JU-FR" :value="old('region_short')" autofocus></x-text-input-white>
                  @error('region_short')
                    <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                  @enderror
                </label>
              </div>
              <div class="my-8 flex w-full items-center justify-end">
                <button
                  class="rounded-xl bg-indigo-500 px-14 py-3 text-white shadow-xl duration-150 hover:bg-indigo-400 active:scale-90 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
                  type="submit">Add Region</button>
              </div>
            </form>
          </div>

        </div>

        <img class="opacity-85 relative z-20 aspect-auto w-full object-cover lg:w-1/2"
          src="{{ asset('images/tutorial-image-region.png') }}" alt="">

      </div>

      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
