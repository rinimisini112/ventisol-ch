@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="''"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-notification-popup> Profile Updated Succesfully</x-notification-popup>
      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/profile'" :breadcrumbSlot="'Profile'" :breadcrumbFinal="''">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="''" :breadcrumbSlot="''" :breadcrumbFinal="''">
      </x-mobile-panel-navbar>

      <div class="relative z-10 min-h-screen">
        <form
          class="mx-auto my-16 flex w-9/10 flex-col items-center rounded-2xl bg-neutral-400 py-8 shadow-xl md:w-3/4 lg:w-1/2">
          @csrf
          <h2 class="roboto-medium pb-6 text-3xl">Admin Profile</h2>
          <label class="mx-auto flex w-9/10 flex-col md:w-4/5 lg:w-3/5" for="name"> Name -
            <x-text-input-white class="w-full" id="name" name='name' type="text" value="{{ $admin->name }}"
              readonly></x-text-input-white>
          </label>
          <label class="mx-auto mt-4 flex w-9/10 flex-col md:w-4/5 lg:w-3/5" for="email"> Email -
            <x-text-input-white class="w-full" id="email" name='email' type="text" value="{{ $admin->email }}"
              readonly></x-text-input-white>
          </label>
          <div class="my-6 flex w-3/5 justify-end">
            <a class="rounded-2xl bg-fuchsia-600 px-10 py-3 text-white shadow-xl drop-shadow-sm"
              href="{{ route('admin.edit') }}">Edit
              Credentials</a>
          </div>
        </form>
        @include('admin.partials._panel_footer')
      </div>
    </div>
  @endsection
