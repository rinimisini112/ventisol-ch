@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="''"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="''" :breadcrumbSlot="''" :breadcrumbFinal="''">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="''" :breadcrumbSlot="''" :breadcrumbFinal="''">
      </x-mobile-panel-navbar>

      <div class="relative z-10 min-h-screen">
        <form class="mx-auto my-16 flex w-1/2 flex-col items-center rounded-2xl bg-neutral-400 py-8 shadow-xl"
          method="POST" action="{{ route('admin.update') }}">
          @csrf
          @method('PUT')
          <h2 class="roboto-medium pb-6 text-3xl">Admin Profile</h2>
          <label class="mx-auto flex w-3/5 flex-col" for="name"> Name -
            <x-text-input-white class="w-full" id="name" name='name' type="text"
              value="{{ $admin->name }}"></x-text-input-white>
          </label>
          <label class="mx-auto mt-4 flex w-3/5 flex-col" for="email"> Email -
            <x-text-input-white class="w-full" id="email" name='email' type="text"
              value="{{ $admin->email }}"></x-text-input-white>
          </label>
          <div class="my-6 flex w-4/5 justify-center gap-6">
            <a class="rounded-2xl bg-sky-600 px-10 py-3 text-white shadow-xl drop-shadow-sm duration-150 hover:bg-sky-500 active:bg-sky-600 active:shadow-round active:shadow-sky-500"
              type="submit" href="{{ route('admin.password') }}">Change Password</a>
            <button
              class="rounded-2xl bg-fuchsia-600 px-10 py-3 text-white shadow-xl drop-shadow-sm duration-150 hover:bg-fuchsia-500 active:bg-fuchsia-600 active:shadow-round active:shadow-fuchsia-500"
              type="submit">Edit
              Profile</button>

          </div>
        </form>
        @include('admin.partials._panel_footer')
      </div>
    </div>
  @endsection
