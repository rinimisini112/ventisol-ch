@extends('layouts.panel_layout')
@section('content')
  <div>

    <x-admin-side-panel :activeLink="'issues'"></x-admin-side-panel>

    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-notification-popup>Region form submited succesfully!</x-notification-popup>
      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/issues'" :breadcrumbSlot="'Issues'" :breadcrumbFinal="''">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/issues'" :breadcrumbSlot="'Issues'" :breadcrumbFinal="''">
      </x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-9/10 rounded-md lg:w-3/4">
        <div>
          <div
            class="flex w-full flex-col items-center justify-between gap-4 rounded-xl bg-white px-4 py-5 shadow-2xl lg:flex-row lg:gap-0">
            <div>
              <p class="roboto-medium text-lg">Issue name appears on the Contact Form!</p>
            </div>
            <div class="w-full lg:w-auto">
              <a class="block w-full rounded-lg bg-green-500 px-10 py-3 text-center text-white duration-150 hover:bg-green-400 active:bg-green-600 active:shadow-round active:shadow-green-500 lg:inline lg:w-auto"
                href="{{ route('issues.create') }}">+ Add Issue</a>
            </div>

          </div>
          <div class="relative mb-8 mt-6 overflow-x-scroll rounded-xl shadow-xl lg:overflow-clip">
            <table class="w-full overflow-x-scroll whitespace-nowrap lg:table-fixed">
              <thead class="roboto-medium bg-venti-dark text-lg text-white">
                <td class="py-3 pl-3">Issues ID</td>
                <td class="py-3 pl-3">Issue Name</td>
                <td class="bg-venti-blue py-3 pl-3 text-center">View</td>
              </thead>
              <tbody class="rounded-xl">
                @forelse ($issues as $issue)
                  <tr
                    class="{{ $loop->iteration % 2 == 0 ? 'bg-neutral-200' : 'bg-white' }} w-full rounded-xl px-4 py-2 text-venti-black shadow-2xl">
                    <td class="py-3 pl-8">{{ $issue->id }}
                    </td>
                    <td class="py-3 pl-3">{{ $issue->issue_name }}</td>
                    <td class="py-3 pl-3 text-right">
                      <a class="group flex items-center justify-center gap-1 duration-150 hover:text-green-600"
                        href="{{ route('issues.show', [$issue->id]) }}">View<svg
                          class="feather feather-eye duration-150 group-hover:stroke-green-600"
                          xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                          fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                          stroke-linejoin="round">
                          <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                          <circle class="duration-150 group-hover:fill-green-600" cx="12" cy="12" r="3"
                            fill='black'></circle>
                        </svg>
                      </a>
                    </td>
                  </tr>
                @empty
                  <div class="w-full bg-venti-dark-tint px-4 py-3 text-white shadow-2xl">
                    <p class="pl-3">No Issues currently found</p>
                  </div>
                  <tr class="w-full rounded-xl bg-white px-4 py-2 shadow-2xl">
                    <td class="py-6 pl-3"></td>
                    <td class="py-6 pl-3"></td>
                    <td class="py-6 pl-3"></td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot class="bg-venti-dark text-venti-dark">
                <tr class="">
                  <td class="py-8"></td>
                  <td class="py-8"></td>
                  <td class="py-8"></td>
                </tr>
              </tfoot>
            </table>
            <div class="absolute bottom-3 left-0 flex w-full items-center justify-between px-5 text-white">
              <p>Click 'Edit' to edit existing Issue, Click 'Add' to add a new issue!</p>
              <div>
                {{ $issues->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
