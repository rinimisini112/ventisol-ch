@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'issues'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-notification-popup>Issue form submited succesfully!</x-notification-popup>
      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('issues.index')" :breadcrumbSlot="'Issues'" :breadcrumbFinal="'Issue'">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('issues.index')" :breadcrumbSlot="'Issues'" :breadcrumbFinal="'Issue'">
      </x-mobile-panel-navbar>
      <div class="relative z-20 mx-auto my-8 w-9/10 rounded-xl bg-neutral-300 py-8 shadow-xl lg:w-3/5">
        <div class="flex flex-col-reverse items-center justify-between gap-3 px-4 lg:flex-row lg:gap-0 lg:px-8">
          <h1 class="roboto-medium text-2xl text-venti-black">Viewing Issue</h1>
          <div class="flex w-full flex-col items-center gap-4 text-lg text-white lg:w-auto lg:flex-row">
            <a class="w-full rounded-xl bg-indigo-500 px-12 py-2 text-center shadow-lg duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500 lg:w-auto"
              href="{{ route('issues.edit', [$issue->id]) }}">Edit Issue</a>
            @livewire('confirm-issue-deletion', ['issueId' => $issue->id])
          </div>
        </div>
        <div class="flex px-6 pt-8">

          <form class="mx-auto w-3/4 py-8 pr-6 text-venti-dark">
            @csrf
            <div class="mt-2 w-full">
              <label class="text-xl" for="issue_name">Issue Name :
                <x-text-input-white class="mt-0.5 block w-full" id="issue_name" name='issue_name' type='text'
                  value="{{ $issue->issue_name }}" readonly
                  placeholder='Example - Neuchâtel-Jura-Fribourg'></x-text-input-white>
                @error('issue_name')
                  <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                @enderror
              </label>
            </div>
          </form>
        </div>

      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
