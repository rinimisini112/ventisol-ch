@extends('layouts.panel_layout')
@section('content')
  <div>

    <x-admin-side-panel :activeLink="'issues'"></x-admin-side-panel>

    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('issues.index')" :breadcrumbSlot="'Issues'" :breadcrumbFinal="'Add Issue'"></x-panel-navbar>

      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('issues.index')" :breadcrumbSlot="'Issues'"
        :breadcrumbFinal="'Add Issue'"></x-mobile-panel-navbar>

      <div class="flex flex-col px-8 lg:flex-row lg:gap-4">

        <div class="relative z-20 mx-auto my-8 w-full rounded-xl bg-neutral-300 py-8 shadow-xl lg:w-1/2">

          <div class="flex flex-col-reverse items-center justify-between gap-4 px-4 lg:flex-row lg:gap-0 lg:px-14">
            <h1 class="roboto-medium text-2xl text-venti-black">Add a new Issue</h1>
            <a class="rounded-xl bg-indigo-500 px-14 py-3 text-white duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
              href="{{ route('issues.index') }}">Go Back</a>
          </div>

          <div class="flex px-6 pt-8">
            <form class="mx-auto w-9/10 pr-6 text-venti-dark" action="{{ route('issues.index') }}" method="POST">
              @csrf
              <div class="mt-2 w-full">
                <label class="text-xl" for="issue_name">Issue Name :
                  <x-text-input-white class="mt-2 block w-full" id="issue_name" name='issue_name' type='text'
                    :value="old('issue_name')" placeholder='Example - Entretien'></x-text-input-white>
                  @error('issue_name')
                    <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
                  @enderror
                </label>
              </div>
              <div class="my-8 flex w-full items-center justify-end">
                <button
                  class="rounded-xl bg-indigo-500 px-14 py-3 text-white shadow-xl duration-150 hover:bg-indigo-400 active:scale-90 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
                  type="submit">Add Issue</button>
              </div>
            </form>
          </div>

        </div>

        <img class="opacity-85 relative z-20 aspect-auto w-full object-cover lg:w-1/2"
          src="{{ asset('images/tutorial-image-issue.png') }}" alt="">

      </div>

      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
