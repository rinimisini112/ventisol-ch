@extends('layouts.panel_layout')
@section('content')
  <div>

    <x-admin-side-panel :activeLink="'clients'"></x-admin-side-panel>

    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-notification-popup>Client form submited succesfully!</x-notification-popup>
      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/clients'" :breadcrumbSlot="'Clients'" :breadcrumbFinal="''">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="'/dashboard/clients'" :breadcrumbSlot="'Clients'" :breadcrumbFinal="''">
      </x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-9/10 rounded-md">
        @livewire('clients-search-table')
      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
