@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'clients'"></x-admin-side-panel>

    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>

      <x-notification-popup>Existing reference edited succesfully</x-notification-popup>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('clients.index')" :breadcrumbSlot="'Clients'" :breadcrumbFinal="'Edit Reference'"></x-panel-navbar>

      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('clients.index')" :breadcrumbSlot="'Clients'"
        :breadcrumbFinal="'Edit Reference'"></x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-4/5 rounded-xl bg-neutral-300 py-8 shadow-xl">
        <div class="flex flex-col-reverse items-center justify-between gap-3 px-4 lg:flex-row lg:gap-0 lg:px-14">
          <h1 class="roboto-medium text-2xl text-venti-black">Edit a existing Reference / Company</h1>
          <a class="rounded-xl bg-indigo-500 px-14 py-3 text-white duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
            href="{{ route('clients.index') }}">Go Back</a>

        </div>
        @livewire('clients-update-form', ['clientId' => $client->id])

      </div>

      @include('admin.partials._panel_footer')
    </div>
  </div>
@endsection
