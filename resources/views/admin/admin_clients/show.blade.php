@extends('layouts.panel_layout')
@section('content')
  <div>
    <x-admin-side-panel :activeLink="'clients'"></x-admin-side-panel>
    <div class="relative ml-auto bg-neutral-300 shadow-2xl transition-all duration-150" id="content">
      <div class="absolute -top-[10%] left-0 h-[350px] w-full -skew-y-6 bg-neutral-400 bg-opacity-70 shadow-xl"></div>
      <x-notification-popup>Client form submited succesfully!</x-notification-popup>

      <x-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('clients.index')" :breadcrumbSlot="'Clients'" :breadcrumbFinal="'Reference'">
      </x-panel-navbar>
      <x-mobile-panel-navbar :messageNumber='$unreadMessages' :breadcrumbSlotRoute="route('clients.index')" :breadcrumbSlot="'Clients'" :breadcrumbFinal="'Reference'">
      </x-mobile-panel-navbar>

      <div class="relative z-20 mx-auto my-8 w-[95%] rounded-xl bg-neutral-300 py-8 shadow-xl lg:w-4/5">
        <div class="flex flex-col-reverse items-center justify-between gap-3 px-8 lg:flex-row lg:gap-0">
          <h1 class="roboto-medium text-2xl text-venti-black">Viewing Reference / Company Nr.{{ $client->id }}</h1>
          <div class="flex flex-col items-center gap-2 text-lg text-white lg:flex-row lg:gap-4">
            <a class="rounded-xl bg-indigo-500 px-12 py-2 shadow-lg duration-150 hover:bg-indigo-400 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
              href="/dashboard/clients/{{ $client->id }}/edit">Edit Reference</a>
            @livewire('confirm-client-deletion', ['clientId' => $client->id])
          </div>
        </div>
        <div class="flex flex-col gap-4 px-6 pt-8 lg:flex-row lg:gap-0">

          <form class="w-full text-venti-dark lg:w-1/2 lg:pr-6">
            @csrf
            <div class="mt-5 w-full">
              <label class="text-xl" for="company_name">Company / Reference Name :
                <x-text-input-white class="mt-0.5 block w-full" id="company_name" name='company_name' type='text'
                  value="{{ $client->company_name }}" readonly
                  placeholder='Enter Client / Reference name'></x-text-input-white>
              </label>
            </div>
            <div class="mt-8 w-full">
              <label class="text-xl" for="company_website_url">Company / Reference Website Link / Url
                <x-text-input-white class="mt-0.5 block w-full" id="company_website_url" name='company_website_url'
                  type='text' value="{{ $client->company_website_url }}" readonly autofocus></x-text-input-white>
              </label>
            </div>
          </form>

          <div
            class="bg-opacity-65 min-h-36 flex w-full flex-col items-center gap-4 rounded-xl bg-neutral-400 px-3 pb-8 pt-4 shadow-inner drop-shadow-lg lg:w-1/2">
            <p class="text-lg text-venti-dark">Current client logo here!</p>
            @if ($client->company_logo)
              <img class="aspect-auto w-full object-cover" src="{{ asset($client->company_logo) }}"
                alt="Company logo preview">
            @endif
          </div>

        </div>

      </div>
      @include('admin.partials._panel_footer')

    </div>
  </div>
@endsection
