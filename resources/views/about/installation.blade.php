@include('partials/_head')

<body class="" style="overflow: hidden">
  @include('partials/_navbar')
  <header class="flex w-full bg-venti-blue bg-opacity-30">

    <div class="relative z-40 hidden w-1/5 bg-venti-blue lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>

    <section class="relative w-full overflow-x-clip lg:w-4/5" id="advantages-section"
      style="background-image: url('{{ asset('images/soft-wallpaper.png') }}')">

      <div class="relative h-[70vh] w-full lg:h-[80vh]">
        <img class="h-full w-full object-cover" id="img-banner" src="{{ asset('images/installation.webp') }}"
          alt="La bannière de la partie installation du site web représente un homme qui fixe un conduit d'aération dans un grenier.">

        <div
          class="absolute left-0 top-0 flex h-full w-full items-center bg-gradient-to-tr from-venti-blue via-[#235889af] to-[#23588922]"
          id='side-curtain'>
          <div class="pl-4 text-2xl text-white md:pl-12 md:text-5xl">
            <h1 class="roboto-light" id="about-company-title">Ventisol</h1>
            <p class="roboto-medium xs:text-2xl pt-6 text-4xl md:text-6xl" id="about-main-title">À propos des
              installations</p>
            <div class="pt-5">
              <a class="roboto-medium inline-block rounded-3xl bg-white px-10 py-2 text-xl text-venti-blue shadow-round duration-200 hover:bg-venti-blue hover:text-white hover:shadow-white md:px-14 md:py-3"
                id="quote-btn" href="/about" style="opacity: 0;transform:translateY(20)">Retour à propos</a>
            </div>
          </div>
        </div>
      </div>

      <div class="w-full pb-8">
        <img class="mx-auto w-4/5 py-8" id="services-title" src="{{ asset('images/installation-title.png') }}"
          alt="Le titre du service d'installation">
        <div class="text-pretty roboto mr-auto w-4/5 max-w-3xl pl-4 text-lg md:mx-auto md:px-8 md:pl-0">
          <div class="relative">
            <img
              class="absolute -right-[15%] top-0 -z-10 h-full w-3/5 object-cover opacity-50 md:-right-[15%] md:w-2/5 md:opacity-75"
              src="{{ asset('images/installation-2.jpeg') }}"
              alt="Travailleur réparant un conduit d'air de ventilation">
            <p class="py-6 text-lg">Chez <span class="roboto-medium text-venti-blue">Ventisol</span>, nous nous
              spécialisons dans l'installation de systèmes de ventilation de qualité commerciale. Grâce à notre
              engagement indéfectible envers la qualité et la précision, nous sommes fiers d'être un chef de file dans
              l'industrie, répondant exclusivement aux besoins exigeants des projets commerciaux à grande échelle..</p>
            <p class="roboto-medium pt-4 text-3xl text-venti-blue">Nos services comprennent:</p>
            <p><span class="roboto-medium block pt-5 text-xl">Des solutions sur mesure:</span>
              Notre équipe spécialisée travaille en étroite collaboration avec nos clients pour comprendre les exigences
              uniques de leurs espaces commerciaux. Nous proposons des solutions de ventilation personnalisées conçues
              pour optimiser le flux d'air, améliorer l'efficacité énergétique et garantir un environnement intérieur
              confortable.</p>
            <p><span class="roboto-medium block pt-5 text-xl">Évaluation professionnelle :</span>
              Avant chaque projet d'installation, Ventisol procède à une évaluation complète du site. Nos experts
              analysent méticuleusement l'espace, en tenant compte de facteurs tels que l'agencement, la taille et les
              besoins spécifiques en matière de ventilation. Cela permet de s'assurer que nos plans d'installation sont
              adaptés aux exigences précises de chaque client.</p>
          </div>

          <p><span class="roboto-medium block pt-5 text-xl">Une technologie de pointe :</span>
            Grâce à une technologie de pointe et à des pratiques d'avant-garde, Ventisol s'assure que votre système de
            ventilation est installé avec la plus grande précision. Nous employons des techniques de pointe pour
            garantir des performances optimales et la longévité de votre système.</p>
          <p><span class="roboto-medium block pt-5 text-xl">Un savoir-faire de qualité :</span>
            Avec une équipe de professionnels qualifiés, Ventisol est fière de fournir un travail de qualité pour chaque
            installation. Nos techniciens sont formés selon les normes les plus strictes, garantissant que chaque
            composant est installé avec précision et expertise..</p>
          <div class="relative">
            <img class="absolute -right-[20%] top-0 -z-10 h-full w-3/5 object-cover opacity-75 md:-right-[15%] md:w-1/2"
              src="{{ asset('images/installation.png') }}" alt="Photo d'un homme installant une ventilation">
            <p><span class="roboto-medium block pt-5 text-xl">Conformité et sécurité :</span>
              Ventisol s'engage à respecter les normes les plus strictes en matière de sécurité et de conformité. Nos
              installations sont conformes aux réglementations de l'industrie et nous accordons la priorité à la
              sécurité de notre équipe et de nos clients tout au long du processus..</p>
            {{-- ---- why us title part ---- --}}
            <p class="roboto-medium py-6 text-3xl text-venti-blue">Pourquoi choisir Ventisol pour l'installation de la
              ventilation?</p>
            <p class="roboto-medium pt-4 text-xl">Expertise de l'industrie :
            </p>
            <p>Avec des années d'expérience en ventilation commerciale, Ventisol est une autorité reconnue dans le
              domaine.</p>
            <p class="roboto-medium pt-4 text-xl">Fiabilité :</p>
            <p>Les clients choisissent Ventisol pour la fiabilité de nos installations. Nous respectons les délais et le
              budget, ce qui garantit une expérience sans faille.</p>
            <p class="roboto-medium pt-4 text-xl">Engagement envers l'excellence :</p>
            <p>Notre engagement envers l'excellence se reflète dans chaque projet que nous entreprenons. Ventisol
              établit la norme en matière d'installation de ventilation de qualité.</p>
          </div>

        </div>
        <div
          class="mx-auto mb-8 mt-28 flex w-4/5 flex-col rounded-2xl bg-venti-blue bg-opacity-90 px-8 py-6 text-xl text-white md:items-end md:px-20">
          <p class="mb-4">Améliorez votre espace commercial grâce aux services experts d'installation de ventilation
            de Ventisol. de Ventisol. Contactez-nous dès aujourd'hui pour discuter des exigences de votre projet et
            faire l'expérience de la différence Ventisol.</p>
          <div class="flex flex-col items-start gap-4 md:flex-row md:items-center">
            <a class="robot-medium inline-block rounded-2xl bg-white px-6 py-2 text-venti-blue duration-200 hover:shadow-round-big hover:shadow-blue-400 active:scale-90 active:bg-opacity-70 md:px-10"
              href="{{ route('about.production') }}">Info Production</a>
            <a class="robot-medium inline-block rounded-2xl bg-white px-6 py-2 text-venti-blue duration-200 hover:shadow-round-big hover:shadow-blue-400 active:scale-90 active:bg-opacity-70 md:px-10"
              href="{{ route('contact') }}">Nous contacter</a>
          </div>
        </div>
      </div>
    </section>
  </header>
  <div id="full-curtain"></div>
  @include('partials._footer')
  <script src="{{ asset('baguettebox.js/dist/baguetteBox.min.js') }}"></script>
  <script src="{{ asset('/js/about.js') }}" defer></script>
  <script></script>
</body>

</html>
