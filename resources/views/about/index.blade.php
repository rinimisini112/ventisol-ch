@include('partials/_head')

<body class="" style="overflow: hidden">
  @include('partials/_navbar')
  <header class="flex w-full bg-venti-blue bg-opacity-30">
    <div class="relative z-40 hidden w-1/5 bg-venti-blue lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <section class="relative w-full overflow-x-clip lg:w-4/5" id="advantages-section"
      style="background-image: url('{{ asset('images/soft-wallpaper.png') }}')">
      <div class="relative h-[70vh] w-full lg:h-screen">
        <img class="h-full w-full object-cover" id="img-banner" src="{{ asset('images/gallery3.jpg') }}"
          alt="Bannière de la page d'accueil une salle de ventilation bleue">
        <div class="absolute left-0 top-0 flex h-full w-full items-center bg-venti-blue" id='full-curtain'>
        </div>
        <div
          class="absolute left-0 top-0 flex h-full w-full items-center bg-gradient-to-tr from-venti-blue via-[#235889af] to-[#23588922]"
          id='side-curtain'>
          <div class="pl-4 text-2xl text-white md:pl-12 md:text-5xl">
            <h1 class="roboto-light" id="about-company-title">Ventisol</h1>
            <p class="roboto-medium xs:text-2xl pt-6 text-4xl md:text-6xl" id="about-main-title">A propos de notre
              entreprise</p>
            <div class="flex items-center gap-8 pb-4 pt-6 text-xl md:text-2xl" id="about-quality-paragraph">
              <div class="flex items-center gap-1">
                <svg id="Layer_2" style="enable-background:new 0 0 246.9 236.6;" width='24px' height='24px'
                  version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                  y="0px" viewBox="0 0 246.9 236.6" xml:space="preserve">
                  <polyline style="fill:none;stroke:#fff;stroke-width:12;stroke-linecap:round;stroke-linejoin:round;"
                    points="81.1,104.8 
	117.7,141.2 235.3,23.5 " />
                  <path style="fill:white;stroke:#fff;stroke-width:8;stroke-linecap:round;stroke-linejoin:round;" d="M164.6,15.4l0-0.2
 c0-1.8-1.5-3.2-3.3-3.2L33.9,12c0,0-17.8-0.4-21.9,20.1c0,0.1,0,0.2,0,0.3l-0.2,166.6c0,0-1.3,22.8,22.3,24.8c0.2,0,0.3,0,0.5,0
 c24.1,1.8,166.8,0,166.8,0s20.3-0.9,21.6-23.5c0,0,0-0.1,0-0.1l0-78.7c0-2-1.6-3.5-3.5-3.5h-0.6c-2.1,0-3.9,1.7-3.8,3.9l0.5,78.9
 c0,0,0,0.1,0,0.1c-0.1,0.9-1.1,14.9-13.9,16.2c0,0-0.1,0-0.1,0c-3.7,0-161.8-0.4-166.1-0.4c-0.1,0-0.1,0-0.2,0
 c-1.3-0.2-15.3-3-16.2-17.4c0,0,0-0.1,0-0.1c0-3.1,0.3-160.9,0.4-166.4c0-0.1,0-0.2,0-0.3c0.4-1.5,3.7-12.8,14.5-12.8
 c10.3,0,108.4-0.7,127.6-0.9C163.3,18.7,164.7,17.2,164.6,15.4z" />
                </svg>
                <span>Commercial</span>
              </div>
              <div class="flex items-center gap-1">
                <svg id="Layer_2" style="enable-background:new 0 0 246.9 236.6;" width='24px' height='24px'
                  version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                  y="0px" viewBox="0 0 246.9 236.6" xml:space="preserve">
                  <polyline style="fill:none;stroke:#fff;stroke-width:12;stroke-linecap:round;stroke-linejoin:round;"
                    points="81.1,104.8 
	117.7,141.2 235.3,23.5 " />
                  <path style="fill:white;stroke:#fff;stroke-width:8;stroke-linecap:round;stroke-linejoin:round;" d="M164.6,15.4l0-0.2
 c0-1.8-1.5-3.2-3.3-3.2L33.9,12c0,0-17.8-0.4-21.9,20.1c0,0.1,0,0.2,0,0.3l-0.2,166.6c0,0-1.3,22.8,22.3,24.8c0.2,0,0.3,0,0.5,0
 c24.1,1.8,166.8,0,166.8,0s20.3-0.9,21.6-23.5c0,0,0-0.1,0-0.1l0-78.7c0-2-1.6-3.5-3.5-3.5h-0.6c-2.1,0-3.9,1.7-3.8,3.9l0.5,78.9
 c0,0,0,0.1,0,0.1c-0.1,0.9-1.1,14.9-13.9,16.2c0,0-0.1,0-0.1,0c-3.7,0-161.8-0.4-166.1-0.4c-0.1,0-0.1,0-0.2,0
 c-1.3-0.2-15.3-3-16.2-17.4c0,0,0-0.1,0-0.1c0-3.1,0.3-160.9,0.4-166.4c0-0.1,0-0.2,0-0.3c0.4-1.5,3.7-12.8,14.5-12.8
 c10.3,0,108.4-0.7,127.6-0.9C163.3,18.7,164.7,17.2,164.6,15.4z" />
                </svg>
                <span>Fabrication</span>
              </div>
            </div>
            <div>
              <a class="roboto-medium inline scale-100 transform rounded-3xl bg-white px-10 py-2 text-xl text-venti-blue shadow-round duration-200 hover:bg-venti-blue hover:text-white hover:shadow-white md:px-14 md:py-3"
                id="quote-btn" href="{{ route('contact') }}" style="opacity: 0;transform:translateX(-150)">Obtenir un
                devis</a>
            </div>
          </div>
        </div>
      </div>
      <div class="w-full pb-8">
        <img class="mx-auto w-3/5 py-8" id="services-title" src="{{ asset('images/services-title.png') }}"
          alt="Titre du service">
        <div class="flex w-full flex-col items-center gap-10 lg:flex-row lg:justify-center">
          <div
            class="service-box w-4/5 cursor-default overflow-clip rounded-xl shadow-2xl backdrop-blur-sm backdrop-opacity-75 duration-300 md:w-3/5 lg:w-[27%]">
            <img class="h-[200px] w-full object-cover" src="{{ asset('images/installation.webp') }}"
              alt="Homme installant une ventilation dans le grenier">
            <h3 class="roboto-medium py-3 text-center text-2xl text-venti-blue">Installation</h3>
            <p class="roboto px-4 text-venti-black">Chez Ventisol, nous nous spécialisons dans la fourniture d'une
              expertise inégalée dans l'installation de...
            </p>
            <div class="flex w-full items-center justify-center py-4">
              <a class="rounded-lg bg-venti-blue px-6 py-1.5 text-white duration-200 hover:scale-105 hover:shadow-round-big hover:shadow-blue-500"
                href="{{ route('about.installation') }}">En savoir plus</a>
            </div>
          </div>
          <div
            class="service-box w-4/5 cursor-default overflow-clip rounded-xl shadow-2xl backdrop-blur-sm backdrop-opacity-75 duration-300 md:w-3/5 lg:w-[27%]">
            <img class="h-[200px] w-full object-cover" src="{{ asset('images/production.jpeg') }}"
              alt="Travailleurs dans une usine de ventilation industrielle">
            <h3 class="roboto-medium py-3 text-center text-2xl text-venti-blue">Production commerciale</h3>
            <p class="roboto px-4 text-venti-black">Ventisol exploite son propre site de production à la pointe de la
              technologie, ce qui lui permet d'assurer un contrôle total...
            </p>
            <div class="flex w-full items-center justify-center py-4">
              <a class="rounded-lg bg-venti-blue px-6 py-1.5 text-white duration-200 hover:scale-105 hover:shadow-round-big hover:shadow-blue-500"
                href="{{ route('about.production') }}">En savoir plus</a>
            </div>
          </div>
          <div
            class="service-box w-4/5 cursor-default overflow-clip rounded-xl shadow-2xl backdrop-blur-sm backdrop-opacity-75 duration-300 md:w-3/5 lg:w-[27%]">
            <img class="h-[200px] w-full object-cover" src="{{ asset('images/hvac-maintenance-2.jpg') }}"
              alt="Homme fixant une échelle pour réparer un conduit de ventilation">
            <h3 class="roboto-medium py-3 text-center text-2xl text-venti-blue">Entretien</h3>
            <p class="roboto px-4 text-venti-black">Optimisez vos résultats en investissant dans l'efficacité de votre
              système de ventilation commerciale...
            </p>
            <div class="flex w-full items-center justify-center py-4">
              <a class="rounded-lg bg-venti-blue px-6 py-1.5 text-white duration-200 hover:scale-105 hover:shadow-round-big hover:shadow-blue-500"
                href="/maintenance">En savoir plus</a>
            </div>
          </div>
        </div>
      </div>
      <div class="relative z-10 mx-auto mt-10 w-9/10">
        <div
          class="flex flex-col rounded-lg bg-white bg-opacity-70 pb-4 shadow-2xl lg:h-[440px] lg:flex-row lg:justify-end lg:px-8 lg:pb-0">
          <img
            class="z-10 mx-0 w-full md:mx-auto md:w-9/10 lg:absolute lg:left-3 lg:top-6 lg:mx-0 lg:h-[470px] lg:w-1/2"
            src="{{ asset('images/about.png') }}" alt="">
          <div class="w-full lg:w-1/2">
            <img class="pt-4 md:mx-auto lg:mx-0 lg:-translate-x-16" src="{{ asset('images/about-title.png') }}"
              alt="Le titre de la page 'à propos de'.">
            <p class="roboto-medium px-3 pt-4 text-xl text-venti-blue md:px-12 lg:pl-4 lg:text-2xl">Définir la norme en
              matière de ventilation
            </p>
            <p class="roboto px-3 pt-3 text-base text-venti-black md:px-12 lg:px-0 lg:pl-4 lg:text-lg">
              Bienvenue chez Ventisol, où nous redéfinissons les références en matière de solutions de ventilation.
              Grâce à notre engagement en faveur d'une fabrication de précision, d'une production de qualité
              professionnelle, d'installations expertes et d'une maintenance fiable, nous sommes devenus synonymes
              d'excellence dans l'industrie.
            </p>
            <div class="px-3 pt-2 md:px-12 lg:pl-4">
              <p class="roboto-medium text-lg text-venti-blue lg:text-xl">Notre engagement :</p>
              <p class="roboto flex items-center text-venti-black">
                <svg class="feather feather-circle mr-0.5" xmlns="http://www.w3.org/2000/svg" width="12"
                  height="12" viewBox="0 0 24 24" fill="#235789" stroke="none" stroke-width="2"
                  stroke-linecap="round" stroke-linejoin="round">
                  <circle cx="12" cy="12" r="10"></circle>
                </svg> Précision
                <svg class="feather feather-circle ml-3 mr-0.5" xmlns="http://www.w3.org/2000/svg" width="12"
                  height="12" viewBox="0 0 24 24" fill="#235789" stroke="none" stroke-width="2"
                  stroke-linecap="round" stroke-linejoin="round">
                  <circle cx="12" cy="12" r="10"></circle>
                </svg>Performance
                <svg class="feather feather-circle ml-3 mr-0.5" xmlns="http://www.w3.org/2000/svg" width="12"
                  height="12" viewBox="0 0 24 24" fill="#235789" stroke="none" stroke-width="2"
                  stroke-linecap="round" stroke-linejoin="round">
                  <circle cx="12" cy="12" r="10"></circle>
                </svg>Perfection
              </p>
            </div>
          </div>
        </div>
        <div
          class="mx-auto flex w-[95%] flex-col overflow-clip bg-white bg-opacity-40 shadow-2xl backdrop-blur-sm md:flex-row lg:pl-12">
          <div class="relative w-full pb-10 pt-8 md:w-3/5 lg:w-3/4 lg:pt-20">
            <div
              class="absolute bottom-[10%] h-1/5 w-full skew-y-[45deg] bg-venti-blue bg-opacity-20 lg:-bottom-[40%] lg:-left-12">
            </div>
            <div
              class="absolute bottom-1/2 h-1/5 w-full skew-y-[45deg] bg-venti-blue bg-opacity-20 lg:-left-12 lg:bottom-0">
            </div>
            <h3 class="roboto-medium pl-4 text-2xl text-venti-blue md:pt-12 lg:pl-0 lg:pt-0 lg:text-3xl">
              Une production de qualité professionnelle
            </h3>
            <p class="roboto px-4 pt-4 text-lg text-venti-black lg:pr-20">Ventisol est fière de fournir des produits de
              ventilation de qualité professionnelle. Nos installations de fabrication de pointe respectent les normes
              les plus strictes, garantissant que chaque composant répond aux exigences de précision et aux critères de
              qualité.</p>
            <h3 class="roboto-medium pl-4 pt-8 text-2xl text-venti-blue md:pt-32 lg:pl-0 lg:pt-24 lg:text-3xl">
              Installations d'experts</h3>
            <p class="roboto px-4 pt-4 text-lg text-venti-black lg:pr-20">
              Nos experts chevronnés apportent une expérience inégalée à chaque projet d'installation. Qu'il s'agisse
              d'espaces commerciaux ou de complexes industriels, Ventisol excelle dans la réalisation d'installations
              sans faille, optimisant le flux d'air, l'efficacité énergétique et la performance globale du système.</p>
            <h3 class="roboto-medium pl-4 pt-8 text-2xl text-venti-blue md:pt-32 lg:pl-0 lg:pt-24 lg:text-3xl">Des
              services de maintenance fiables
            </h3>
            <p class="roboto px-4 pt-4 text-lg text-venti-black lg:pr-20">
              Assurer la longévité et l'efficacité de vos systèmes de ventilation est notre priorité absolue. Nos
              services d'entretien complets sont conçus pour que vos systèmes fonctionnent de manière optimale,
              garantissant ainsi un environnement intérieur confortable et sain.</p>
          </div>
          <div class="flex w-full flex-col gap-4 bg-venti-blue px-4 pt-3 shadow-2xl md:w-2/5 lg:w-1/4 lg:px-0"
            style="background-image: url('{{ asset('images/batthern.png') }}')">
            <img src="{{ asset('SVG/air-vent.svg') }}" alt="Image vectorielle d'un conduit de ventilation">
            <img src="{{ asset('SVG/man-fixing-vent.svg') }}"
              alt="Vector Image dans un ventisol t-shirt perçant une bouche d'aération">
            <img src="{{ asset('SVG/man-fixing-pump.svg') }}"
              alt="Image vectorielle d'une personne portant un T-shirt ventilé regardant à travers une bouche d'aération.">
          </div>
        </div>
        <div class="relative z-10 w-full rounded-lg bg-white bg-opacity-70 shadow-2xl">
          <h3 class="roboto-bold px-3 py-4 text-3xl text-venti-blue lg:px-8 lg:text-5xl">Pourquoi choisir Ventisol ?
          </h3>
          <div class="flex w-full flex-col items-center justify-center gap-8 px-4 pb-8 pt-4 md:flex-row">
            <div class="relative w-full rounded-2xl bg-white pb-8 pt-20 shadow-2xl md:w-3/10">
              <span
                class="roboto-bold absolute -top-[20%] right-0 h-fit p-4 text-[15rem] leading-snug text-venti-blue opacity-25">1</span>
              <p class="roboto-bold pl-4 text-xl text-venti-blue">Précision suisse :</p>
              <p class="roboto pl-4 pt-12 text-lg text-venti-black">Notre engagement en faveur de la précision suisse
                garantit des produits et des services qui dépassent les attentes de l'industrie.
              </p>
            </div>
            <div class="relative w-full rounded-2xl bg-white pb-8 pt-20 shadow-2xl md:w-3/10">
              <span
                class="roboto-bold absolute -top-[20%] right-0 h-fit p-4 text-[15rem] leading-snug text-venti-blue opacity-25">2</span>
              <p class="roboto-bold pl-4 text-xl text-venti-blue">Des solutions sur mesure :</p>
              <p class="roboto pl-4 pt-12 text-lg text-venti-black">Nous comprenons que chaque projet est unique.
                Ventisol propose des solutions de ventilation personnalisées pour répondre à vos besoins spécifiques.
              </p>
            </div>
            <div class="relative w-full rounded-2xl bg-white pb-8 pt-20 shadow-2xl md:w-3/10">
              <span
                class="roboto-bold absolute -top-[20%] right-0 h-fit p-4 text-[15rem] leading-snug text-venti-blue opacity-25">3</span>
              <p class="roboto-bold pl-4 text-xl text-venti-blue">Engagement pour l'excellence :</p>
              <p class="roboto pl-4 pt-12 text-lg text-venti-black">Chez Ventisol, l'excellence n'est pas seulement un
                objectif, c'est notre norme. Nous sommes déterminés à placer la barre très haut et à la maintenir.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="mx-auto my-24 w-[95%] rounded-3xl bg-venti-blue p-8 text-white shadow-2xl md:w-9/10 lg:w-3/4">
        <p class="roboto text-pretty text-xl text-white lg:text-3xl">Rejoignez-nous pour établir la norme d'excellence
          en matière de ventilation. Choisissez <span class="roboto-bold font-bold">Ventisol</span> pour une qualité,
          une fiabilité et une innovation inégalées dans chaque solution de ventilation.</p>
        <div class="mt-8 flex flex-col items-center justify-center gap-6 md:flex-row">
          <a class="roboto-medium inline-flex w-4/5 justify-center rounded-2xl bg-white py-3 text-xl text-venti-blue duration-200 hover:scale-105 hover:shadow-round hover:shadow-white active:scale-90 active:bg-venti-blue md:w-auto md:px-12"
            href="{{ route('advantages') }}">Voir les avantages</a>
          <a class="roboto-medium inline-flex w-4/5 justify-center rounded-2xl bg-white py-3 text-xl text-venti-blue duration-200 hover:scale-105 hover:shadow-round hover:shadow-white active:scale-90 active:bg-venti-blue md:w-auto md:px-12"
            href="{{ route('contact') }}">Nous contacter</a>
        </div>
      </div>
    </section>
  </header>
  @include('partials._footer')
  <script src="{{ asset('baguettebox.js/dist/baguetteBox.min.js') }}"></script>
  <script src="{{ asset('js/about.js') }}" defer></script>
  <script></script>
</body>

</html>
