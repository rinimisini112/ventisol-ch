@include('partials/_head')

<body class="" style="overflow: hidden">
  @include('partials/_navbar')
  <header class="flex w-full bg-venti-blue bg-opacity-30">
    <div class="relative z-40 hidden w-1/5 bg-venti-blue lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <section class="relative w-full overflow-x-clip lg:w-4/5" id="advantages-section"
      style="background-image: url('{{ asset('images/soft-wallpaper.png') }}')">
      <div class="relative h-[70vh] w-full lg:h-[80vh]">
        <img class="h-full w-full object-cover" id="img-banner" src="{{ asset('images/production.jpeg') }}"
          alt="Travailleurs dans une usine de ventilation industrielle">
        <div
          class="absolute left-0 top-0 flex h-full w-full items-center bg-gradient-to-tr from-venti-blue via-[#235889af] to-[#23588922]"
          id='side-curtain'>
          <div class="pl-4 text-2xl text-white md:pl-12 md:text-5xl">
            <h1 class="roboto-light" id="about-company-title">Ventisol</h1>
            <p class="roboto-medium xs:text-2xl pt-6 text-4xl md:text-6xl" id="about-main-title">À propos de la
              production</p>
            <div class="pt-5">
              <a class="roboto-medium inline-block rounded-3xl bg-white px-10 py-2 text-xl text-venti-blue shadow-round duration-200 hover:bg-venti-blue hover:text-white hover:shadow-white md:px-14 md:py-3"
                id="quote-btn" href="/about" style="opacity: 0;transform:translateY(20)">Retour à propos</a>
            </div>
          </div>
        </div>
      </div>
      <div class="w-full pb-8">
        <img class="mx-auto w-4/5 py-8" id="services-title" src="{{ asset('images/production-title.png') }}"
          alt="Image de conduits de ventilation dans un entrepôt de stockage et pendant la production">
        <div class="text-pretty roboto mx-auto px-12 text-lg md:px-20 lg:max-w-3xl lg:px-8">
          <div class="relative">
            <img class="h-full w-full rounded-2xl object-cover opacity-75"
              src="{{ asset('images/facility-production.webp') }}" alt="Picture of a man installing a ventilation">
            <p class="py-6 text-lg"><span class="roboto-medium text-venti-blue">Ventisol</span> est fière de son site de
              production dédié, où nous fabriquons des conduits et des évents de qualité supérieure pour toute une série
              d'applications commerciales. En mettant l'accent sur la précision de la fabrication, nous fournissons des
              produits qui répondent aux normes les plus élevées de l'industrie.</p>
            <p class="roboto-medium pt-4 text-3xl text-venti-blue">Nos points forts en matière de production :</p>
            {{-- ---- highlights --- --}}
            <p><span class="roboto-medium block pt-5 text-xl">Fabrication interne :</span>
              Ventisol exploite ses propres installations de production à la pointe de la technologie, ce qui lui permet
              d'exercer un contrôle total sur le processus de fabrication. De la conception à la création, notre équipe
              supervise chaque étape pour garantir la qualité du produit final.</p>
            <p><span class="roboto-medium block pt-5 text-xl">Solutions de conduits sur mesure :</span>
              Nous comprenons que chaque projet a des exigences uniques. Chez Ventisol, nous proposons des solutions de
              conduits sur mesure, adaptées à vos spécifications. Notre processus de fabrication permet une grande
              flexibilité, garantissant que chaque produit s'aligne parfaitement sur les besoins de votre projet.
              .</p>
          </div>
          <p><span class="roboto-medium block pt-5 text-xl">Technologie de pointe :</span>
            Équipé des dernières technologies, notre site de production utilise des machines de pointe pour créer des
            conduits et des évents avec précision et efficacité. Cet engagement en faveur du progrès technologique
            garantit la fiabilité et la longévité de nos produits.</p>
          <p><span class="roboto-medium block pt-5 text-xl">Assurance qualité :</span>
            Ventisol accorde une grande importance à l'assurance qualité. Des protocoles rigoureux de test et
            d'inspection sont intégrés à notre processus de production, garantissant que chaque conduit et évent
            quittant nos installations répond aux normes les plus strictes.</p>
          <p><span class="roboto-medium block pt-5 text-xl">Livraison dans les délais : </span>
            Nous comprenons l'importance des délais dans l'industrie de la construction et de la ventilation. Ventisol
            s'engage à livrer vos commandes à temps, assurant ainsi que votre projet reste sur la bonne voie..</p>
          <p class="roboto-medium py-8 text-3xl text-venti-blue">Choisissez Ventisol pour vos besoins de production de
            conduits</p>

          <div class="w-full items-start gap-8 lg:flex">
            {{-- ---- why us title part ---- --}}
            <img class="mx-auto aspect-video w-full object-cover opacity-90 lg:w-3/5"
              src="{{ asset('images/facility-production-1.webp') }}"
              alt="La production de conduits de ventilation industriels">
            <div class="w-full lg:w-2/5">
              <p class="roboto-medium pt-6 text-xl">Expertise interne :
              </p>
              <p class=""><span class="roboto-medium text-venti-blue">Ventisol</span> offre des solutions sur
                mesure pour répondre aux spécifications uniques de votre projet, offrant une flexibilité dans la
                conception et la fonctionnalité.
              </p>
            </div>
          </div>
          <p class="roboto-medium pt-4 text-xl">Solutions sur mesure :</p>
          <p class="">Grâce à une technologie de pointe, nous assurons l'efficacité et la précision de notre
            processus de fabrication, ce qui se traduit par des produits de haute qualité.
          </p>
          <p class="roboto-medium pt-4 text-xl">Efficacité axée sur la technologie :</p>
          <p class="">
            Notre engagement envers l'excellence se reflète dans chaque projet que nous entreprenons. <span
              class="roboto-medium text-venti-blue">Ventisol</span> établit la norme en matière d'installation de
            ventilation de qualité.</p>
          <p class="roboto-medium pt-4 text-xl">Fiabilité et rapidité :</p>
          <p class="">Les clients choisissent <span class="roboto-medium text-venti-blue">Ventisol</span> pour la
            fiabilité de nos produits et notre engagement à livrer les commandes à temps, contribuant ainsi à la
            réussite de vos projets.</p>
        </div>
        <div
          class="mx-auto mb-8 mt-28 flex w-4/5 flex-col rounded-2xl bg-venti-blue bg-opacity-90 px-8 py-6 text-xl text-white md:items-end lg:px-20">
          <p class="mb-4">Pour une production de gaines de premier ordre, choisissez Ventisol - là où la précision
            rencontre la qualité. Contactez-nous dès aujourd'hui pour discuter de vos besoins spécifiques et découvrir
            l'excellence de la fabrication Ventisol.</p>
          <div class="flex flex-col items-start gap-4 md:flex-row md:items-center">
            <a class="robot-medium inline-block rounded-2xl bg-white px-6 py-2 text-venti-blue duration-200 hover:shadow-round-big hover:shadow-blue-400 active:scale-90 active:bg-opacity-70 lg:px-10"
              href="{{ route('about.installation') }}">Info Installation</a>
            <a class="robot-medium inline-block rounded-2xl bg-white px-6 py-2 text-venti-blue duration-200 hover:shadow-round-big hover:shadow-blue-400 active:scale-90 active:bg-opacity-70 lg:px-10"
              href="{{ route('contact') }}">Nous contacter</a>
          </div>
        </div>
      </div>
    </section>
  </header>
  <div id="full-curtain"></div>
  @include('partials._footer')
  <script src="{{ asset('baguettebox.js/dist/baguetteBox.min.js') }}"></script>
  <script src="{{ asset('/js/about.js') }}" defer></script>
  <script></script>
</body>

</html>
