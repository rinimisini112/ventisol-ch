<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ventisol CH</title>
  <meta name="description"
    content="Ventisol is a ventilation company that deals in manifacture of industrial HVAC units, installation, and maintenance.">
  <meta name="keywords" content="HVAC, Ventilation, HVAC Installation, HVAC Maintenance">
  <meta property="og:title" content="Ventisol">
  <meta property="og:description"
    content="Ventisol is a ventilation company that deals in manifacture of industrial HVAC units, installation, and maintenance..">
  <meta property="og:url" content="https://www.example.com/your-page">
  <meta name="robots" content="index, follow">
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "LocalBusiness",
      "name": "Ventisol",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "123 Main Street",
        "addressLocality": "City",
        "addressRegion": "State",
        "postalCode": "12345",
        "addressCountry": "Country"
      },
      "telephone": "123-456-7890",
      "url": "https://www.example.com"
    }
    </script>
  @vite('resources/css/app.css')
  <link href="{{ asset('/baguettebox.js/dist/baguetteBox.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
  <script src="{{ asset('/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('/gsap/dist/gsap.min.js') }}" defer></script>
  <script src="{{ asset('/gsap/dist/ScrollTrigger.min.js') }}" defer></script>
  <script src="{{ asset('/js/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('/pristinejs/dist/pristine.min.js') }}"></script>
  <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-DYB9V8LGG3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-DYB9V8LGG3');
  </script>

</head>
