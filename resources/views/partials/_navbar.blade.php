<div class="fixed left-0 top-0 z-[1000] flex h-full w-full items-center justify-center bg-white" id="loading-overlay">
  <div class="flex flex-col items-center">
    <h1 class="pb-2 text-5xl font-bold text-venti-blue">VENTISOL</h1>
    <p class="translate-y-2 text-xl font-normal">Loading</p>
    <div class="lds-facebook">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
</div>
<span
  class="fixed left-4 top-4 z-[120] flex h-[55px] w-[55px] items-center justify-center rounded-full bg-black bg-opacity-60 duration-300 ease-linear lg:hidden"
  id="phone-menu-icon">
  <svg class="feather feather-menu" xmlns="http://www.w3.org/2000/svg" width="45" height="45" viewBox="0 0 24 24"
    fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
    <line class="menu_line duration-200" x1="5" y1="12" x2="23" y2="12"></line>
    <line class="menu_line duration-200" x1="3" y1="6" x2="21" y2="6"></line>
    <line class="menu_line duration-200" x1="3" y1="18" x2="21" y2="18"></line>
  </svg>
</span>
<nav
  class="fixed left-0 top-0 z-[110] flex h-full items-center justify-center overflow-clip bg-venti-blue bg-opacity-80 backdrop-blur-sm"
  id="phone-nav" style="width:0;">
  <div class="pl-2">
    <ul class="z-[110] font-mono text-2xl text-white duration-75 md:text-4xl" id="phone-items">
      <div class="relative flex flex-col items-center">
        <a class="mb-8 inline-block text-5xl font-extrabold uppercase" href="{{ route('index') }}">Ventisol</a>

      </div>
      <!-- Your navigation links -->
      <div class="z-[200]" id="phone-links">
        <a class="duration-300 active:scale-110 active:text-venti-blue" href="{{ route('advantages') }}">
          <li class="cursor-pointer py-4 font-bold duration-300" id="keyAdvantages">
            Avantages Clés
          </li>
        </a>
        <a class="duration-300 active:scale-110 active:text-venti-blue" href="{{ route('about') }}">
          <li class="cursor-pointer py-4 font-bold duration-300" id="aboutUs">
            Nous concernant
          </li>
        </a>
        <a class="duration-300 active:scale-110 active:text-venti-blue" href="/projects">
          <li class="cursor-pointer py-4 font-bold duration-300" id="projects">
            Projets
          </li>
        </a>
        <a class="duration-300 active:scale-110 active:text-venti-blue" href="{{ route('contact') }}">
          <li class="cursor-pointer py-4 font-bold duration-300" id="contactUs">
            Contactez-nous
          </li>
        </a>
      </div>
    </ul>
  </div>
</nav>
<nav class="fixed left-0 top-0 z-50 hidden h-full items-center justify-center lg:flex" id="side-nav"
  style="transform: translateX(-100%);width:20%;">
  <div class="pl-2">
    <ul class="z-50 font-mono text-3xl text-venti-navy-blue duration-75" id="nav-items">
      <div class="relative flex flex-col items-center">
        <a class="group mb-8 inline-flex items-center text-5xl font-extrabold uppercase duration-200 active:scale-90 active:text-venti-blue"
          href="{{ route('index') }}">
          Ventisol
          <img class="animate-spin opacity-60 duration-500 group-hover:animate-none"
            src="{{ asset('SVG/air-fan.svg') }}" width="40" height="40">
        </a>
      </div>
      <div id="nav-links">
        <a href="{{ route('advantages') }}">
          <li
            class="list-group group cursor-pointer py-5 font-bold duration-300 hover:scale-105 active:scale-90 active:text-venti-blue active:duration-150"
            id="keyAdvantages">
            Avantages Clés
          </li>
        </a>
        <a href="{{ route('about') }}">
          <li
            class="list-group group cursor-pointer py-5 font-bold duration-300 hover:scale-105 active:scale-90 active:text-venti-blue active:duration-150"
            id="aboutUs">
            Nous concernant
          </li>
        </a>
        <a href="/projects">
          <li
            class="list-group group cursor-pointer py-5 font-bold duration-300 hover:scale-105 active:scale-90 active:text-venti-blue active:duration-150"
            id="projects">
            Projets
          </li>
        </a>
        <a href="{{ route('contact') }}">
          <li
            class="list-group group cursor-pointer py-5 font-bold duration-300 hover:scale-105 active:scale-105 active:text-venti-blue active:duration-150"
            id="contactUs">
            Contactez-nous
          </li>
        </a>
      </div>

    </ul>
  </div>
</nav>
