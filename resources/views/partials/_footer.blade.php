<footer class="relative z-[100] w-full bg-venti-black text-white" id="footer"
  style="font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;">
  <div class="flex h-full flex-col-reverse gap-14 px-4 pb-3 pt-6 lg:flex-row lg:px-10 lg:pt-10">
    <div class="flex flex-col items-start justify-between gap-8">
      <div class="flex flex-wrap items-center gap-6 lg:flex-nowrap lg:gap-24">
        <ul class="text-lg text-neutral-400">
          <h1 class="cursor-default pb-3 text-2xl font-bold text-white">Entreprise</h1>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a
              href="{{ route('about') }}">À propos de
              nous</a>
          </li>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a
              href="{{ route('advantages') }}">Clé
              Avantages</a></li>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a
              href="{{ route('advantages') }}#maintenance">Entretien</a></li>
        </ul>
        <ul class="text-lg text-neutral-400">
          <h1 class="cursor-default pb-3 text-2xl font-bold text-white">Notre travail</h1>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a
              href="{{ route('projects.all') }}">Projets</a>
          </li>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a
              href="{{ route('projects.index') }}">Clients</a>
          </li>
        </ul>
        <ul class="text-lg text-neutral-400">
          <h1 class="cursor-default pb-3 text-2xl font-bold text-white">Aide</h1>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a
              href="{{ route('contact') }}">Contact</a>
          </li>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a href="/privacy">Vie privée
              Politique</a></li>
          <li class="py-1 duration-200 hover:scale-105 hover:font-medium hover:text-white"><a href="">FAQ</a>
          </li>
        </ul>
      </div>
      <div class="cursor-default">
        <p>Ventisol | Geneva, Switzerland</p>
        <p>© <span class="text-blue-600">2023</span> <span class="font-bold">Ventisol CH&trade;</span> Tous droits
          réservés.
        </p>
      </div>
    </div>
    <div class="w-full bg-venti-blue bg-opacity-60 pb-1 shadow-xl lg:w-0.5 lg:pb-4"></div>
    <div class="flex flex-col gap-12 md:flex-row">
      <div class="flex w-full flex-col items-center md:w-2/5 lg:block lg:w-auto">
        <h1 class="cursor-default pb-6 pt-2 text-2xl font-bold lg:pt-0">Médias sociaux</h1>
        <div class="flex flex-row items-center gap-6 md:flex-col md:items-start">
          <a href="">
            <svg
              class="feather feather-facebook duration-100 hover:scale-105 hover:fill-white hover:stroke-[#4267B2] hover:shadow-2xl"
              xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none"
              stroke="#aaa" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
              <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
            </svg>
          </a>
          <a href="">
            <i class='bx bxl-instagram' id="instagramIcon"></i>
          </a>
        </div>
      </div>
      <div class="flex flex-col items-start justify-between px-0 md:px-4 lg:px-0">
        <div class="relative">
          <a class="group mb-6 flex flex-row-reverse items-center text-3xl font-extrabold uppercase duration-200 hover:text-venti-blue lg:text-5xl"
            href="index.php">
            <img class="animate-spin opacity-50 duration-500 group-hover:animate-none"
              src="{{ asset('SVG/air-fan.svg') }}" width="70" height="70">
            Ventisol</a>
        </div>
        <div class="">
          <p class="cursor-default">Website and design by : <a
              class="font-bold text-blue-500 duration-100 hover:text-blue-600" href="">Rini Misini</a></p>
          <p class="cursor-default">Images conçues par <a
              class="font-bold text-blue-500 duration-100 hover:text-blue-600"
              href="http://www.freepik.com/">Freepik</a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('baguettebox.js/dist/baguetteBox.min.js') }}"></script>
