@include('partials._head')

<body style="overflow: hidden;">
  @if (session('success'))
    <div
      class="success-notification fixed bottom-3 right-4 z-[350] flex h-[120px] w-full flex-col overflow-clip rounded-2xl bg-green-600 bg-opacity-60 text-2xl text-white backdrop-blur-md backdrop-opacity-95 md:w-2/5">
      <h1 class="w-full bg-green-600 bg-opacity-60 px-4 py-2">Message envoyé avec succès</h1>
      <p class="px-6 pt-5 text-base">{{ session('success') }}</p>
      <svg class="feather feather-x absolute right-3 top-3 cursor-pointer" xmlns="http://www.w3.org/2000/svg"
        width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
        stroke-linecap="round" stroke-linejoin="round">
        <line x1="18" y1="6" x2="6" y2="18"></line>
        <line x1="6" y1="6" x2="18" y2="18"></line>
      </svg>
    </div>
  @endif
  @if (session('error'))
    <div
      class="error-notification fixed bottom-3 right-4 z-[350] flex h-[120px] w-full flex-col overflow-clip rounded-2xl bg-red-600 bg-opacity-60 text-2xl text-white backdrop-blur-md backdrop-opacity-95 md:w-2/5">
      <h1 class="w-full bg-red-600 bg-opacity-60 px-4 py-2">Le message n'a pas pu être envoyé</h1>
      <p class="px-6 pt-5 text-base">{{ session('error') }}</p>
      <svg class="feather feather-x absolute right-3 top-3 cursor-pointer" xmlns="http://www.w3.org/2000/svg"
        width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
        stroke-linecap="round" stroke-linejoin="round">
        <line x1="18" y1="6" x2="6" y2="18"></line>
        <line x1="6" y1="6" x2="18" y2="18"></line>
      </svg>
    </div>
  @endif
  <section class="flex" style="background-image: url('{{ asset('images/contact-bg2.png') }}');background-size:cover;">
    <div class="relative z-40 hidden w-1/5 bg-venti-blue bg-opacity-40 backdrop-blur-md lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
      <div class="h-[180px] w-full bg-gradient-to-b from-venti-blue via-[#235889ee] to-transparent"></div>
    </div>
    <div class="w-[95%] lg:w-4/5">
      <div
        class="ml-auto flex w-[95%] flex-col-reverse bg-venti-blue bg-opacity-40 shadow-2xl backdrop-blur-md md:flex-row lg:w-[85%]"
        style="background-image: url('{{ asset('images/batthern.png') }}');">
        <article class="relative w-full pb-24 lg:w-[65%]">
          <div
            class="absolute left-0 top-0 -z-10 h-[180px] w-full bg-gradient-to-b from-venti-blue via-[#235889ee] to-transparent">
          </div>
          <h1 class="roboto-bold p-8 text-5xl text-white">Contact</h1>
          <div class="relative mx-auto h-auto w-full pb-12 pt-6 md:w-[95%]">
            <img class="absolute left-0 top-0 h-full w-full opacity-95" src="{{ asset('images/papper-form.png') }}"
              alt="">
            <form class="relative z-10 mx-auto mt-8 w-[95%] md:w-[90%]" id="contact-form"
              action="{{ route('submit.contact') }}" method="post">
              @csrf
              <label class="w-full" for="issue">
                <span class="block">Sélectionnez votre question</span>
                <select
                  class="w-full border border-neutral-500 py-1.5 text-[#999] shadow-inner duration-200 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                  id="issue" name="issue" onchange="changeTextColor(this)">
                  <option class="text-neutral-700" value="0">Sélectionnez votre question</option>
                  @foreach ($issues as $issue)
                    <option class="text-neutral-700" value="{{ $issue->id }}">{{ $issue->issue_name }}</option>
                  @endforeach
                </select>
              </label>
              <div class="flex w-full flex-col items-center md:flex-row md:justify-between">
                <label class="w-full md:w-1/2" for="region">
                  <span class="block">Région</span>
                  <select
                    class="w-full border border-neutral-500 py-1.5 text-[#999] shadow-inner duration-200 ease-in-out focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                    id="region" name="region" onchange="changeTextColor(this)">
                    <option class="placeholder-neutral-700" value="0">Sélectionner une région</option>
                    @foreach ($regions as $region)
                      <option class="text-neutral-700" value="{{ $region->id }}">{{ $region->region_short }}</option>
                    @endforeach
                  </select>
                </label>
                <label class="w-full md:w-[47%]" for="company">
                  <span class="block">Entreprise</span>
                  <input
                    class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                    id="company" name="company" type="text" placeholder="Nom de l'entreprise - facultatif">
                </label>
              </div>
              <div class="flex w-full flex-col items-center md:flex-row md:justify-between">
                <label class="w-full md:w-1/2" for="name">
                  <span class="block">Prénom</span>
                  <input
                    class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                    id="name" name="name" type="text" placeholder="Prénom">
                </label>
                <label class="w-full md:w-[47%]" for="last_name">
                  <span class="block">Nom</span>
                  <input
                    class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                    id="last_name" name="last_name" type="text" placeholder="Nom">
                </label>
              </div>
              <label class="w-full" for="address">
                <span class="block">Adresse</span>
                <input
                  class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                  id="address" name="address" type="text" placeholder="Entrez votre adresse">
              </label>
              <div class="flex w-full items-center justify-between gap-4">
                <label class="w-1/5" for="postal_code">
                  <span class="block">NPA</span>
                  <input
                    class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                    id="postal_code" name="postal_code" type="text" placeholder="NPA">
                </label>
                <label class="w-4/5" for="city">
                  <span class="block">Localite</span>
                  <input
                    class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                    id="city" name="city" type="text" placeholder="Entrez votre ville">
                </label>
              </div>
              <label class="w-full" for="telephone">
                <span class="block">Téléphone</span>
                <input
                  class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:text-red-500 invalid:shadow-round invalid:shadow-red-500 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                  id="telephone" name="telephone" type="tel" placeholder="Numéro de téléphone">
              </label>
              <label class="w-full" for="email">
                <span class="block">E-mail</span>
                <input
                  class="w-full border border-neutral-500 shadow-inner duration-200 ease-in-out invalid:border-red-600 invalid:shadow-round-big invalid:shadow-red-600 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                  id="email" name="email" type="email" placeholder="user@example.com">
              </label>
              <label class="w-full" for="subject">
                <span class="block">Objet de votre message</span>
                <input
                  class="w-full border border-neutral-500 pl-1 shadow-inner duration-200 ease-in-out focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                  id="subject" name="subject" type="text" placeholder="L'objet de votre message">
              </label>
              <textarea
                class="mt-2 w-full min-w-full max-w-full rounded-md border border-neutral-500 shadow-inner outline-none duration-200 focus:border-blue-600 focus:shadow-round-big focus:shadow-blue-600"
                id="message" name="message" placeholder="Votre message"></textarea>
              <button
                class="roboto-medium mt-4 rounded-2xl bg-venti-blue bg-opacity-70 px-12 py-1.5 text-xl text-white duration-200 hover:bg-opacity-100"
                id="submitButton" type="submit"><span class="">Envoyer</span></button>
            </form>
          </div>
        </article>
        <div class="bg-opacity-55 w-full bg-white shadow-xl md:w-[35%]">
          <div class="sticky top-0 mx-auto flex w-[90%] flex-col justify-between pt-8">
            <div>
              <h2 class="roboto-bold text-3xl text-venti-blue">Adresse postale</h2>
              <h3 class="roboto-medium pt-3 text-xl">Nr.1</h3>
              <p class="text-lg italic text-neutral-700">
                <span class="block">To the Channey 40</span>
                CH-1077 Servion
              </p>
              <h3 class="roboto-medium pt-3 text-xl">Nr.2</h3>
              <p class="text-lg italic text-neutral-700">
                <span class="block">Fin-de-Praz 14</span>
                CH-2024 St-Aubin-Sauges
              </p>
              <h2 class="roboto-bold pt-4 text-3xl text-venti-blue">Téléphone</h2>
              <p class="roboto pb-1 pt-3 text-lg text-neutral-700">
                <span class="roboto-bold text-black">VD-VS-FR:</span> 021 559 58 72
              </p>
              <p class="roboto py-1 text-lg text-neutral-700">
                <span class="roboto-bold text-black">NE-JU-FR:</span> 032 525 20 25
              </p>
              <p class="roboto py-1 text-lg text-neutral-700">
                <span class="roboto-bold text-black">GE:</span> 022 559 62 60
              </p>
              <h2 class="roboto-bold pt-4 text-3xl text-venti-blue">E-Mail</h2>
              <a class="roboto py-4 text-lg text-blue-600 duration-150 ease-in hover:text-blue-700 hover:underline hover:decoration-blue-600 hover:underline-offset-2"
                href="mailto:support@ventisol.ch">
                support@ventisol.ch
              </a>
            </div>
            <div class="flex items-center justify-center gap-6 py-8">
              <a href="">
                <svg
                  class="feather feather-facebook duration-100 hover:scale-105 hover:fill-white hover:stroke-[#4267B2] hover:shadow-2xl"
                  xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24"
                  fill="none" stroke="#235789" stroke-width="1.5" stroke-linecap="round"
                  stroke-linejoin="round">
                  <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                </svg>
              </a>
              <a href="">
                <i class='bx bxl-instagram' id="instagramIcon"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @include('partials._navbar')
  @include('partials._footer')
  <script src="{{ asset('js/contact.js') }}" async></script>
</body>

</html>
