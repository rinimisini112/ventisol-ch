<a href="{{ $route }}" {{ $attributes->merge(['class' => $classes()]) }}>
  <li class="flex items-center gap-3">{{ $slot }}</li>
</a>
