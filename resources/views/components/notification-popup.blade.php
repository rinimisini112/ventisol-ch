@if (session('success'))
  <div
    class="fixed bottom-3 right-3 z-[1000] h-[150px] w-9/10 overflow-clip rounded-xl bg-green-500 bg-opacity-75 text-white backdrop-blur-sm lg:w-2/5"
    id="notification">
    <div class="w-full bg-green-500 py-2 pl-3">
      {{ $slot }}
    </div>
    <p class="p-3">{{ session('success') }}</p>
  </div>
@elseif (session('error'))
  <div
    class="fixed bottom-3 right-3 z-[1000] h-[150px] w-9/10 overflow-clip rounded-xl bg-red-500 bg-opacity-75 text-white backdrop-blur-sm lg:w-2/5"
    id="notification">
    <div class="w-full bg-red-500 py-2 pl-3">
      {{ $slot }}
    </div>
    <p class="p-3">{{ session('error') }}</p>
  </div>
@endif
<script>
  $(document).ready(function() {
    setTimeout(function() {
      // Slide out animation
      $("#notification").animate({
        right: '-500px'
      }, 'slow', function() {
        $(this).remove();
      });
    }, 4000);
  });
</script>
