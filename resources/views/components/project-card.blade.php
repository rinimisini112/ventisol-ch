@props(['project', 'flexDirectionStyle' => 'md:flex-row'])
@php
  $imageUrls = json_decode($project->images, true);
  $firstImageUrl = isset($imageUrls[0]) ? asset($imageUrls[0]) : null;

  if (isset($flexDirectionStyle)) {
      $flexDirectionStyle = $flexDirectionStyle;
  } else {
      $flexDirectionStyle = 'md:flex-row';
  }
@endphp

<div
  class="{{ $flexDirectionStyle }} mx-auto my-6 flex h-full w-[90%] flex-col rounded-2xl bg-white bg-opacity-5 p-4 shadow-2xl backdrop-blur-sm lg:w-4/5">
  <div class="w-full lg:w-3/5">
    @if ($firstImageUrl)
      <a class="inline-block h-full w-full" href="{{ asset($firstImageUrl) }}">
        <img class="aspect-auto w-full object-cover shadow-2xl" src="{{ $firstImageUrl }}" alt="">
      </a>
    @endif
  </div>
  <div class="flex w-full flex-col justify-between px-4 lg:w-2/5">
    <p class="roboto-bold text-2xl text-venti-blue">{{ $project->title }}</p>
    <div class="flex h-full flex-col justify-between">
      <div class="flex flex-col justify-between pt-3">
        <p class="flex items-center gap-2 pt-2">
          <svg class="feather feather-map-pin flex-shrink-0" xmlns="http://www.w3.org/2000/svg" width="20"
            height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
            stroke-linecap="round" stroke-linejoin="round">
            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
            <circle cx="12" cy="10" r="3"></circle>
          </svg>{{ $project->location }}
        </p>
        <p class="flex items-center gap-2 pt-2">
          <svg class="feather feather-briefcase flex-shrink-0" xmlns="http://www.w3.org/2000/svg" width="20"
            height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
            stroke-linecap="round" stroke-linejoin="round">
            <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
            <path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
          </svg>{{ $project->company }}
        </p>
      </div>
      <div class="flex items-center justify-between">
        <p class="flex items-center gap-2 text-xl text-venti-blue">
          <svg class="feather feather-calendar flex-shrink-0" xmlns="http://www.w3.org/2000/svg" width="20"
            height="20" viewBox="0 0 24 24" fill="none" stroke="black" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round">
            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
            <line x1="16" y1="2" x2="16" y2="6"></line>
            <line x1="8" y1="2" x2="8" y2="6"></line>
            <line x1="3" y1="10" x2="21" y2="10"></line>
          </svg>{{ \Carbon\Carbon::parse($project->finished_at)->format('d-m-Y') }}
        </p>
        <a class="roboto-medium rounded-3xl bg-venti-blue px-4 py-2 text-white opacity-90 duration-200 hover:scale-105 hover:opacity-100 active:bg-white active:ring active:ring-white"
          href="/project/{{ $project->id }}" style="background-image: url('{{ asset('images/worn-dots.png') }}')">Voir
          projet</a>
      </div>
    </div>
  </div>
</div>
