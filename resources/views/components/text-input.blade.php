@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge([
    'class' =>
        'border-gray-300 duration-200 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-venti-blue dark:focus:border-venti-blue focus:ring-venti-blue dark:focus:ring-venti-blue rounded-lg shadow-md',
]) !!}>
