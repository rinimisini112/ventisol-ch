@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge([
    'class' =>
        'border-indigo-400 duration-200 bg-gray-300 py-1.5 shadow-inner text-venti-dark focus:border-indigo-400 dark:focus:border-indigo-400 focus:ring-indigo-400 dark:focus:ring-indigo-400 rounded-md',
]) !!}>
