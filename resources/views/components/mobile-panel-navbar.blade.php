<div
  class="relative z-30 flex w-full flex-col-reverse justify-between gap-3 bg-neutral-400 py-3 pl-3 pr-6 shadow-xl lg:hidden lg:flex-row lg:items-center lg:gap-0">
  <div class="flex gap-6 text-base text-venti-dark-compliment" id="breadcrumb">

    <a class="duration-150 hover:text-neutral-700" href="{{ route('admin.dashboard') }}">Dashboard</a>
    /
    @if ($breadcrumbSlot)
      <a class="duration-150 hover:text-neutral-700" href="{{ $breadcrumbSlotRoute }}"> {{ $breadcrumbSlot }}</a>
    @endif

    @if ($breadcrumbFinal)
      / <span class="cursor-default text-neutral-500">{{ $breadcrumbFinal }}</span>
    @endif
  </div>
  <div class="roboto-medium flex justify-between gap-5 text-venti-dark">
    <div id="toggle-mobile-side-panel">
      <svg
        class="feather feather-align-left cursor-pointer duration-100 hover:stroke-venti-dark-compliment active:scale-90"
        xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none"
        stroke="#21222c" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
        <line x1="17" y1="10" x2="3" y2="10"></line>
        <line x1="21" y1="6" x2="3" y2="6"></line>
        <line x1="21" y1="14" x2="3" y2="14"></line>
        <line x1="17" y1="18" x2="3" y2="18"></line>
      </svg>
    </div>
    <div class="flex items-center gap-3 pl-3">
      <div>
        @livewire('notification-bell')
      </div>
      @livewire('profile-dropdown')

      <form action="/admin/logout" method="POST">
        @csrf
        <button
          class="rounded-xl bg-venti-dark px-6 py-1.5 text-gray-300 duration-150 hover:bg-venti-dark-tint active:scale-90 active:ring-2 active:ring-indigo-500"
          type="submit">Logout</button>
      </form>
    </div>
  </div>
</div>
