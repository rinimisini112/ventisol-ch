<div class="fixed left-0 top-0 z-50 h-full overflow-clip bg-venti-black text-gray-200 shadow-2xl duration-150"
  id="sidebar" style="background-image: url('{{ asset('images/egg-shell.png') }}')">
  <div class="relative flex h-full flex-col justify-between">
    <h1
      class="roboto-medium flex justify-between bg-venti-dark px-4 py-4 text-center text-3xl text-white drop-shadow-lg lg:block lg:px-0">
      Ventisol Panel
      <svg
        class="feather feather-align-left inline cursor-pointer duration-100 hover:stroke-gray-300 active:scale-90 lg:hidden"
        id="close-mobile-panel" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24"
        fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
        <line x1="17" y1="10" x2="3" y2="10"></line>
        <line x1="21" y1="6" x2="3" y2="6"></line>
        <line x1="21" y1="14" x2="3" y2="14"></line>
        <line x1="17" y1="18" x2="3" y2="18"></line>
      </svg>
    </h1>
    <ul class="mb-16 mt-6 flex flex-col gap-2 px-6 text-2xl">
      <x-side-nav-link :route="route('admin.dashboard')" :section="'admin.dashboard'" :slot="'Dashboard'" :isActive="$activeLink === 'dashboard'">
        <svg class="feather feather-home" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
          viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round">
          <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
          <polyline points="9 22 9 12 15 12 15 22"></polyline>
        </svg>
        Dashboard
      </x-side-nav-link>
      <x-side-nav-link :route="route('admin.messages')" :section="'admin.messages'" :slot="'Messages'" :isActive="$activeLink === 'messages'"><svg
          class="feather feather-message-square" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
          viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round">
          <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
        </svg>Messages</x-side-nav-link>
      <x-side-nav-link :route="route('projects.index')" :section="'projects.all'" :slot="'Projects'" :isActive="$activeLink === 'projects'"><svg
          class="feather feather-archive" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
          viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round">
          <polyline points="21 8 21 21 3 21 3 8"></polyline>
          <rect x="1" y="3" width="22" height="5"></rect>
          <line x1="10" y1="12" x2="14" y2="12"></line>
        </svg>Projects</x-side-nav-link>
      <x-side-nav-link :route="route('clients.index')" :section="'clients.index'" :slot="'Clients'" :isActive="$activeLink === 'clients'">
        <svg class="feather feather-users" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
          viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round">
          <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
          <circle cx="9" cy="7" r="4"></circle>
          <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
          <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
        </svg>Clients</x-side-nav-link>
      <x-side-nav-link :route="route('regions.index')" :section="'regions.index'" :slot="'Regions'" :isActive="$activeLink === 'regions'"><svg
          class="feather feather-map-pin" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
          viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round">
          <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
          <circle cx="12" cy="10" r="3"></circle>
        </svg>Regions</x-side-nav-link>
      <x-side-nav-link :route="route('issues.index')" :section="'issues.index'" :slot="'Issues'" :isActive="$activeLink === 'issues'">
        <svg class="feather feather-alert-circle" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
          viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round">
          <circle cx="12" cy="12" r="10"></circle>
          <line x1="12" y1="8" x2="12" y2="12"></line>
          <line x1="12" y1="16" x2="12.01" y2="16"></line>
        </svg>Issues</x-side-nav-link>
    </ul>
  </div>
</div>
