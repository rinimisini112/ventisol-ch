<div>
  <div
    class="flex w-full flex-col items-center justify-between gap-4 rounded-xl bg-white px-4 py-4 shadow-2xl md:flex-row md:gap-12 lg:gap-0">
    <div class="w-full lg:w-auto">
      <label class="roboto-medium mr-4 text-lg text-venti-dark" for="search">Search Projects</label>
      <input class="h-9 w-full border-b-2 border-white border-b-venti-dark lg:w-auto" name="search" type="text"
        wire:model.live="search" placeholder="Filter" />
    </div>
    <div class="w-full lg:w-auto">
      <a class="flex w-full items-center justify-center rounded-lg bg-green-500 py-3 text-white duration-150 hover:bg-green-400 active:bg-green-600 active:shadow-round active:shadow-green-500 lg:inline lg:w-auto lg:px-10"
        href="/dashboard/projects/create">+ Add Project</a>
    </div>
  </div>
  <div class="relative mb-8 mt-6 overflow-x-scroll rounded-xl shadow-xl lg:overflow-clip">
    <table class="w-full table-auto overflow-x-auto whitespace-nowrap lg:table-fixed lg:whitespace-normal">
      <thead class="roboto-medium bg-venti-dark text-lg text-white">
        <td class="py-3 pl-3">Image</td>
        <td class="py-3 pl-3">Title</td>
        <td class="py-3 pl-3">Location</td>
        <td class="py-3 pl-3">Company</td>
        <td class="bg-venti-blue py-3 pl-3 text-center">Edit</td>
      </thead>
      <tbody class="rounded-xl">
        @forelse ($projects as $project)
          <tr
            class="{{ $loop->iteration % 2 == 0 ? 'bg-neutral-200' : 'bg-white' }} w-full rounded-xl px-4 py-2 text-venti-black shadow-2xl">
            @php
              $imageUrls = json_decode($project->images, true);
              $firstImageUrl = isset($imageUrls[0]) ? asset($imageUrls[0]) : null;
            @endphp
            <td class="py-3 pl-3"><img class="aspect-auto w-full object-cover" src="{{ $firstImageUrl }}"
                alt=""></td>
            <td class="py-3 pl-3">{{ $project->title }}</td>
            <td class="py-3 pl-3">{{ $project->location }}</td>
            <td class="py-3 pl-3">{{ $project->company }}</td>
            <td class="py-3 pl-3">
              <a class="group flex items-center justify-center gap-1 duration-150 hover:text-green-600"
                href="{{ route('projects.show', [$project->id]) }}">Edit<svg
                  class="feather feather-eye duration-150 group-hover:stroke-green-600"
                  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                  stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                  <circle class="duration-150 group-hover:fill-green-600" cx="12" cy="12" r="3"
                    fill='black'></circle>
                </svg>
              </a>
            </td>
          </tr>
        @empty
          <div class="w-full bg-venti-dark-tint px-4 py-3 text-white shadow-2xl">
            <p class="pl-3">No projects found</p>
          </div>
          <tr class="w-full rounded-xl bg-white px-4 py-2 shadow-2xl">
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
          </tr>
        @endforelse
      </tbody>
      <tfoot class="bg-venti-dark text-venti-dark">
        <tr class="">
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
        </tr>
      </tfoot>
    </table>
    <div class="absolute bottom-3 left-0 flex w-full items-center justify-between px-5 text-white">
      <p>Click 'Edit' to edit a project, - Click + Add Project to add a new one.</p>
      <div>
        {{ $projects->links() }}
      </div>
    </div>
  </div>
</div>
