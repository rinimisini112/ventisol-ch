<div>
  <div class="flex w-full flex-col items-center justify-between rounded-xl bg-white px-4 py-1 shadow-2xl md:flex-row">
    <div class="w-full lg:w-auto">
      <label class="roboto-medium text-md mr-4 text-venti-dark" for="search">Search Messages</label>
      <input class="h-9 w-full border-b-2 border-white border-b-venti-dark md:w-auto" name="search" type="text"
        wire:model.live="search" placeholder="Filter" />
    </div>
    <div class="my-4 w-full lg:w-auto">
      <label class="roboto-medium text-md mr-4 text-venti-dark" for="viewedFilter">Filter viewed messages:</label>
      <select class="w-full md:w-auto" id="viewedFilter" wire:model="viewedFilter" wire:change="applyFilter">
        <option value="2">All</option>
        <option value="0">Not Viewed</option>
        <option value="1">Viewed</option>
      </select>
    </div>

  </div>
  <div class="relative mb-8 mt-6 overflow-x-scroll rounded-xl shadow-xl lg:overflow-clip">
    <table class="w-full table-auto overflow-x-scroll whitespace-nowrap lg:overflow-auto">
      <thead class="roboto-medium bg-venti-dark text-lg text-white">
        <td class="py-3 pl-3">Name</td>
        <td class="py-3 pl-3">Issue</td>
        <td class="py-3 pl-3">Region</td>
        <td class="py-3 pl-3">Phone</td>
        <td class="py-3 pl-3">Email</td>
        <td class="py-3 pl-3">Status</td>
        <td class="bg-venti-blue py-3 pl-3">View</td>
      </thead>
      <tbody class="rounded-xl">
        @forelse ($messages as $message)
          <tr
            class="{{ $loop->iteration % 2 == 0 ? 'bg-neutral-200' : 'bg-white' }} w-full rounded-xl px-4 py-2 text-venti-black shadow-2xl">
            <td class="py-3 pl-3">{{ $message->first_name . ' ' . $message->last_name }}</td>
            <td class="py-3 pl-3">{{ $message->issue->issue_name }}</td>
            <td class="py-3 pl-3">{{ $message->region->region_short }}</td>
            <td class="py-3 pl-3">{{ $message->phone_number }}</td>
            <td class="py-3 pl-3">{{ $message->email }}</td>
            @if ($message->is_viewed === 0)
              <td class="py-3 pl-3 text-red-600">Not Viewed</td>
            @else
              <td class="py-3 pl-3 text-green-600">Viewed</td>
            @endif
            <td class="py-3 pl-3 text-right">
              <a class="group flex items-center gap-1 duration-150 hover:text-green-600"
                href="/dashboard/message/{{ $message->id }}">View<svg
                  class="feather feather-eye duration-150 group-hover:stroke-green-600"
                  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                  stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                  <circle class="duration-150 group-hover:fill-green-600" cx="12" cy="12" r="3"
                    fill='black'></circle>
                </svg>
              </a>
            </td>
          </tr>
        @empty
          <div class="w-full bg-venti-dark-tint px-4 py-3 text-white shadow-2xl">
            <p class="pl-3">No Messages found matching your search</p>
          </div>
          <tr class="w-full rounded-xl bg-white px-4 py-2 shadow-2xl">
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
          </tr>
        @endforelse
      </tbody>
      <tfoot class="bg-venti-dark text-venti-dark">
        <tr class="">
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
        </tr>
      </tfoot>
    </table>
    <div class="absolute bottom-3 left-0 flex w-full items-center justify-between px-5 text-white">
      <p>Click 'View' to see the full message!</p>
      <div>
        {{ $messages->links() }}
      </div>
    </div>
  </div>
</div>
