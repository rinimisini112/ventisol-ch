<div>
  <div
    class="flex w-full flex-col items-center justify-between gap-2 rounded-xl bg-white px-4 py-4 shadow-2xl lg:flex-row lg:gap-0">
    <div class="w-full lg:w-auto">
      <label class="roboto-medium mr-4 text-lg text-venti-dark" for="search">Search Messages</label>
      <input class="h-9 w-full border-b-2 border-white border-b-venti-dark lg:w-auto" name="search" type="text"
        wire:model.live="search" placeholder="Filter" />
    </div>
    <div class="w-full lg:w-auto">
      <a class="inline-block w-full rounded-lg bg-green-500 px-10 py-3 text-center text-white duration-150 hover:bg-green-400 active:bg-green-600 active:shadow-round active:shadow-green-500 lg:inline lg:w-auto"
        href="{{ route('clients.create') }}">+ Add Reference</a>
    </div>

  </div>
  <div class="relative mb-8 mt-6 overflow-x-scroll rounded-xl shadow-xl lg:overflow-clip">
    <table class="w-full overflow-x-scroll whitespace-nowrap lg:table-fixed">
      <thead class="roboto-medium bg-venti-dark text-lg text-white">
        <td class="py-3 pl-3">Company Logo</td>
        <td class="py-3 pl-3">Company Name</td>
        <td class="py-3 pl-3">Company Website Link / Url</td>
        <td class="bg-venti-blue py-3 pl-3 text-center">View</td>
      </thead>
      <tbody class="rounded-xl">
        @forelse ($clients as $client)
          <tr
            class="{{ $loop->iteration % 2 == 0 ? 'bg-neutral-200' : 'bg-white' }} w-full rounded-xl px-4 py-2 text-venti-black shadow-2xl">
            <td class="py-3 pl-3"><img class="w-3/5" src="{{ asset($client->company_logo) }}" alt=""></td>
            <td class="py-3 pl-3">{{ $client->company_name }}</td>
            <td class="py-3 pl-3"><a href="{{ $client->company_website_url }}"
                target="_blank">{{ $client->company_website_url }}</a>
            </td>
            <td class="py-3 pl-3 text-right">
              <a class="group flex items-center justify-center gap-1 duration-150 hover:text-green-600"
                href="{{ route('clients.show', [$client->id]) }}">View<svg
                  class="feather feather-eye duration-150 group-hover:stroke-green-600"
                  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                  stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                  <circle class="duration-150 group-hover:fill-green-600" cx="12" cy="12" r="3"
                    fill='black'></circle>
                </svg>
              </a>
            </td>
          </tr>
        @empty
          <div class="w-full bg-venti-dark-tint px-4 py-3 text-white shadow-2xl">
            <p class="pl-3">No Reference Companies found</p>
          </div>
          <tr class="w-full rounded-xl bg-white px-4 py-2 shadow-2xl">
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
            <td class="py-6 pl-3"></td>
          </tr>
        @endforelse
      </tbody>
      <tfoot class="bg-venti-dark text-venti-dark">
        <tr class="">
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
          <td class="py-8"></td>
        </tr>
      </tfoot>
    </table>
    <div class="absolute bottom-3 left-0 flex w-full items-center justify-between px-5 text-white">
      <p>Click 'Edit' to edit existing reference, Click 'Add' to add a new reference / client!</p>
      <div>
        {{ $clients->links() }}
      </div>
    </div>
  </div>
</div>
