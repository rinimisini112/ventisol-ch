<div
  class="group group relative z-30 flex h-10 w-10 cursor-pointer select-none items-center justify-center rounded-full duration-150 hover:bg-neutral-500 active:bg-emerald-500"
  wire:click="toggleNotifications" x-data="{ isOpen: @entangle('isOpen') }" x-on:click.away="isOpen=false">
  @if ($unreadMessages > 0)
    <span
      class="absolute -right-1.5 -top-1.5 z-10 flex h-5 w-5 cursor-pointer items-center justify-center rounded-full bg-red-600 text-sm text-white duration-100 group-active:h-4 group-active:w-4">
      {{ $unreadMessages }}
    </span>
  @endif
  <svg class="feather feather-bell cursor-pointer duration-150 group-active:scale-90 group-active:stroke-white"
    xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none"
    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
    <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
    <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
  </svg>
  <div
    class="absolute -left-48 top-12 z-40 h-[450px] w-[400px] overflow-y-scroll rounded-b-xl bg-neutral-500 shadow-2xl"
    x-show="isOpen" x-transition:enter="transition ease-out duration-200"
    x-transition:enter-start="opacity-0 transform scale-90" x-transition:enter-end="opacity-100 transform scale-100"
    x-transition:leave="transition ease-in duration-100" x-transition:leave-start="opacity-100 transform scale-100"
    x-transition:leave-end="opacity-0 transform scale-90" x-on:click.stop>
    @forelse ($messageNotifications as $notification)
      @if ($notification->is_viewed === 0)
        <a href="/dashboard/message/{{ $notification->id }}">
          <div class="border-b border-b-neutral-300 bg-neutral-700 px-4 py-2 duration-100 hover:bg-neutral-600">
            <span class="flex items-center justify-between text-gray-300"><span>You have a new message</span>
              •{{ $notification->created_at->diffForHumans() }}</span>
            <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}</p>
            <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
          </div>
        </a>
      @else
        <a href="/dashboard/message/{{ $notification->id }}">
          <div class="border-b border-b-neutral-300 bg-neutral-500 px-4 py-2 duration-100 hover:bg-neutral-600">
            <span class="flex items-center justify-between text-gray-300"><span>You have read this message</span>
              •{{ $notification->created_at->diffForHumans() }}</span>
            <p class="text-white">Message from : {{ $notification->first_name . ' ' . $notification->last_name }}
            </p>
            <p class="text-white">Phone Number : {{ $notification->phone_number }}</p>
          </div>
        </a>
      @endif
    @empty
      <p class="text-gray-300">No new notifications</p>
    @endforelse
  </div>
</div>
