<div class="flex flex-col px-6 pt-8 lg:flex-row">

  <form class="w-full text-venti-dark lg:w-3/5 lg:pr-6" wire:submit.prevent="store" action="" method="POST">
    @csrf
    <div class="mt-2 w-full">
      <label class="text-xl" for="title">Project Title :
        <x-text-input-white class="mt-0.5 block w-full" id="title" name='title' type='text'
          wire:model.live="title" :value="old('title')" placeholder='Enter project title'></x-text-input-white>
        @error('title')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-2 w-full">
      <label class="text-xl" for="location">Location
        <x-text-input-white class="mt-0.5 block w-full lg:w-4/5" id="location" name='location' type='text'
          placeholder="Project location" wire:model.live="location" :value="old('location')" autofocus></x-text-input-white>
        @error('location')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-2 w-full">
      <label class="text-xl" for="company">Company / Client name
        <x-text-input-white class="mt-0.5 block w-full lg:w-4/5" id="company" name='company' type='text'
          placeholder="Project company / client" wire:model.live="company" :value="old('company')"
          autofocus></x-text-input-white>
        @error('company')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-2 w-full">
      <label class="text-xl" for="company_url">Company Website Link / Url
        <x-text-input-white class="mt-0.5 block w-full lg:w-4/5" id="company_url" name='company_url' type='text'
          placeholder="Company website link" wire:model.live="company_url" :value="old('company_url')"
          autofocus></x-text-input-white>
        @error('company_url')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-2 w-full">
      <label class="text-xl" for="finished_at">Project finished at :
        <x-text-input-white class="mt-0.5 block w-full border border-indigo-400 pl-2 text-base lg:w-4/5"
          id="finished_at" name='finished_at' type='datetime-local' wire:model.live="finished_at" :value="old('finished_at')"
          autofocus></x-text-input-white>
        @error('finished_at')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-2 w-full">
      <label class="text-xl" for="content">Content
        <textarea
          class="rounded-md' min-h-40 block w-full min-w-full max-w-full border-indigo-400 bg-gray-300 text-venti-dark shadow-inner duration-200 focus:border-indigo-400 focus:ring-indigo-400 dark:focus:border-indigo-400 dark:focus:ring-indigo-400"
          id="content" name="content" placeholder="Project content" wire:model.live="content"></textarea>
        @error('content')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-4 w-full">
      <p class="text-xl">Images for project, Images will appear in website in order from image Nr.1 to image Nr.5
        when uploaded :
      </p>
      <div class="flex items-center justify-between px-4 pt-4">
        <label class="block" for="image1">
          <img class="w-12 cursor-pointer duration-150 hover:scale-110 active:scale-90"
            src="{{ asset('SVG/image-1-input.svg') }}" alt="">
          <input class="hidden" id="image1" name="image1" type="file" wire:model="image1">

        </label>
        <label class="block" for="image2">
          <img class="w-12 cursor-pointer duration-150 hover:scale-110 active:scale-90"
            src="{{ asset('SVG/image-2-input.svg') }}" alt="">
          <input class="hidden" id="image2" name="image2" type="file" wire:model="image2">
        </label>
        <label class="block" for="image3">
          <img class="w-12 cursor-pointer duration-150 hover:scale-110 active:scale-90"
            src="{{ asset('SVG/image-3-input.svg') }}" alt="">
          <input class="hidden" id="image3" name="image3" type="file" wire:model="image3">
        </label>
        <label class="block" for="image4">
          <img class="w-12 cursor-pointer duration-150 hover:scale-110 active:scale-90"
            src="{{ asset('SVG/image-4-input.svg') }}" alt="">
          <input class="hidden" id="image4" name="image4" type="file" wire:model="image4">
        </label>
        <label class="block" for="image5">
          <img class="w-12 cursor-pointer duration-150 hover:scale-110 active:scale-90"
            src="{{ asset('SVG/image-5-input.svg') }}" alt="">
          <input class="hidden" id="image5" name="image5" type="file" wire:model="image5">
        </label>
      </div>
      @error('image1')
        <span class="block text-red-500">{{ $message }}</span>
      @enderror
      @error('image2')
        <span class="block text-red-500">{{ $message }}</span>
      @enderror
      @error('image3')
        <span class="block text-red-500">{{ $message }}</span>
      @enderror
      @error('image4')
        <span class="block text-red-500">{{ $message }}</span>
      @enderror
      @error('image5')
        <span class="block text-red-500">{{ $message }}</span>
      @enderror
    </div>
    <div class="my-8 flex w-full items-center justify-end">
      <button
        class="rounded-xl bg-indigo-500 px-14 py-3 text-white shadow-xl duration-150 hover:bg-indigo-400 active:scale-90 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
        type="submit">Add Project</button>
    </div>
  </form>

  <div
    class="bg-opacity-65 flex min-h-screen w-full flex-col items-center gap-4 rounded-xl bg-neutral-400 pb-8 pt-4 shadow-inner drop-shadow-lg lg:w-2/5">
    @if ($image1)
      <p>Preview for image 1</p>
      <img class="max-h-[250px] w-9/10 object-cover" src="{{ $image1->temporaryUrl() }}" alt="Image 1 Preview">
    @endif
    @if ($image2)
      <p>Preview for image 2</p>
      <img class="max-h-[250px] w-9/10 object-cover" src="{{ $image2->temporaryUrl() }}" alt="Image 2 Preview">
    @endif
    @if ($image3)
      <p>Preview for image 3</p>
      <img class="max-h-[250px] w-9/10 object-cover" src="{{ $image3->temporaryUrl() }}" alt="Image 3 Preview">
    @endif
    @if ($image4)
      <p>Preview for image 4</p>
      <img class="max-h-[250px] w-9/10 object-cover" src="{{ $image4->temporaryUrl() }}" alt="Image 4 Preview">
    @endif
    @if ($image5)
      <p>Preview for image 5</p>
      <img class="max-h-[250px] w-9/10 object-cover" src="{{ $image5->temporaryUrl() }}" alt="Image 5 Preview">
    @endif
  </div>

</div>
