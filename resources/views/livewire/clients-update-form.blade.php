<div class="flex flex-col gap-4 px-6 pt-8 lg:flex-row lg:gap-0">

  <form class="w-full text-venti-dark lg:w-1/2 lg:pr-6" wire:submit.prevent="update" action="" method="POST">
    @csrf
    <div class="mt-2 w-full">
      <label class="text-xl" for="company_name">Company / Reference Name :
        <x-text-input-white class="mt-0.5 block w-full" id="company_name" name='company_name' type='text'
          wire:model.live="company_name" :value="old('company_name')"
          placeholder='Enter Client / Reference name'></x-text-input-white>
        @error('company_name')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-4 w-full">
      <label class="text-xl" for="company_website_url">Company / Reference Website Link / Url
        <x-text-input-white class="mt-0.5 block w-full" id="company_website_url" name='company_website_url'
          type='text' placeholder="Company website link" wire:model.live="company_website_url" :value="old('company_website_url')"
          autofocus></x-text-input-white>
        @error('company_website_url')
          <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
        @enderror
      </label>
    </div>
    <div class="mt-4 w-full">
      <p class="text-xl">Add Company Logo :
      </p>
      <div class="flex items-center justify-between px-4 pt-4">
        <label class="flex items-center gap-3" for="company_logo">
          Add logo -
          <svg class="feather feather-image cursor-pointer duration-150 hover:scale-110 active:scale-90"
            xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24" fill="none"
            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
            <circle cx="8.5" cy="8.5" r="1.5"></circle>
            <polyline points="21 15 16 10 5 21"></polyline>
          </svg>
          <input class="hidden" id="company_logo" name="company_logo" type="file" wire:model="company_logo">
          @error('company_logo')
            <span class="pt-0.5 text-base text-red-600">{{ $message }}</span>
          @enderror
        </label>
      </div>
    </div>
    <div class="my-8 flex w-full items-center justify-end">
      <button
        class="rounded-xl bg-indigo-500 px-14 py-3 text-white shadow-xl duration-150 hover:bg-indigo-400 active:scale-90 active:bg-indigo-600 active:shadow-round active:shadow-indigo-500"
        type="submit">Edit Reference</button>
    </div>
  </form>

  <div
    class="bg-opacity-65 min-h-36 flex w-full flex-col items-center gap-4 rounded-xl bg-neutral-400 px-3 pb-8 pt-4 shadow-inner drop-shadow-lg lg:w-1/2">
    <p class="text-lg text-venti-dark">Preview for existing client logo will be shown here!</p>
    @if ($current_company_logo)
      <img class="aspect-auto w-full object-cover" src="{{ asset($current_company_logo) }}" alt="Company logo preview">
    @endif
    @if ($company_logo)
      <p class="text-lg text-venti-dark">Preview for newly uploaded logo shown here!</p>
      <img class="aspect-auto w-full object-cover" src="{{ $company_logo->temporaryUrl() }}" alt="Company logo preview">
    @endif
  </div>

</div>
