<div x-data="{ isOpen: false }" x-init="$watch('isOpen', value => { if (value) $refs.modal.focus() })">
  <form wire:submit.prevent="confirmDelete" method="POST">
    @csrf
    @method('DELETE')
    <button
      class="rounded-xl bg-red-500 px-14 py-2 shadow-lg duration-150 hover:bg-red-400 active:bg-red-600 active:shadow-round active:shadow-red-500"
      type="submit" @click="isOpen = true">Delete</button>
  </form>
  <div tabindex="0" x-show="isOpen" x-ref="modal" @keydown.escape.window="isOpen = false">
    <div class="fixed inset-0 z-[150] bg-venti-black opacity-70 duration-300" @click="isOpen = false"></div>

    <div
      class="fixed left-1/2 top-1/2 z-[200] -translate-x-1/2 -translate-y-1/2 transform rounded-xl bg-neutral-300 px-10 py-8 text-venti-dark duration-300"
      @click.away="isOpen = false">
      <p>Are you sure you want to delete this Region?</p>

      <div class="mt-4 flex justify-end space-x-4 text-white">
        <button class="rounded-xl bg-red-500 px-10 py-1.5 duration-150 hover:bg-red-400 active:bg-red-600"
          wire:click="delete">Yes</button>
        <button class="rounded-xl bg-indigo-500 px-10 py-1.5 duration-150 hover:bg-indigo-400 active:bg-indigo-600"
          @click="isOpen = false">No</button>
      </div>
    </div>
  </div>
</div>
