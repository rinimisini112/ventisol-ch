@if ($paginator->hasPages())
  <nav class="my-4 flex justify-center gap-6" role="navigation" aria-label="Pagination Navigation">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
      <span
        class="relative inline-flex w-32 cursor-default items-center justify-center rounded-md border border-gray-300 bg-venti-blue bg-opacity-80 py-2 text-sm font-medium leading-5 text-white">
        {!! __('pagination.previous') !!}
      </span>
    @else
      <a class="relative inline-flex w-32 items-center justify-center rounded-md border border-gray-300 bg-venti-blue py-2 text-sm font-medium leading-5 text-white transition duration-200 ease-in-out hover:shadow-round-big hover:shadow-venti-blue focus:border-blue-300 focus:outline-none focus:ring active:bg-gray-100 active:text-white"
        href="{{ $paginator->previousPageUrl() }}" rel="prev">
        {!! __('pagination.previous') !!}
      </a>
    @endif

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
      <a class="relative inline-flex w-32 items-center justify-center rounded-md border border-gray-300 bg-venti-blue py-2 text-sm font-medium leading-5 text-white transition duration-200 ease-in-out hover:shadow-round-big hover:shadow-venti-blue focus:border-blue-300 focus:outline-none focus:ring active:bg-gray-100 active:text-white"
        href="{{ $paginator->nextPageUrl() }}" rel="next">
        {!! __('pagination.next') !!}
      </a>
    @else
      <span
        class="relative inline-flex w-32 cursor-default items-center justify-center rounded-md border border-gray-300 bg-venti-blue bg-opacity-80 py-2 text-sm font-medium leading-5 text-white">
        {!! __('pagination.next') !!}
      </span>
    @endif
  </nav>
@endif
