@include('partials._head')
<style>
  .error {
    color: red !important;
    font-size: 14px !important;
    font-weight: 200 !important;
    color: red;
  }

  .error:focus {
    color: red;
    border-color: red !important;
    font-size: 14px !important;
    color: red;
  }
</style>

<body style="overflow: hidden;">

  <main class="relative flex h-screen w-full items-center justify-center bg-venti-black" style="">
    <img class="absolute -bottom-[40%] -right-[40%] z-0 hidden w-full opacity-40 md:block"
      src="{{ asset('images/venti-title-bold.png') }}">
    <div class="absolute left-6 top-4 flex w-full items-center gap-8 text-xl text-gray-300">
      <h1 class="roboto-medium text-4xl text-venti-blue">Ventisol</h1>
      <div class="flex items-center gap-5">
        <a class="duration-150 hover:text-gray-400 active:scale-90 active:text-venti-black"
          href="{{ url()->previous() }}">Go
          Back</a>
        <a class="duration-150 hover:text-gray-400 active:scale-90 active:text-venti-black"
          href="{{ route('index') }}">Home</a>
      </div>
    </div>
    <div class="relative z-10 w-9/10 rounded-xl bg-[#2e2f3d] px-6 py-12 shadow-2xl md:w-3/5 lg:w-[35%]">

      <x-auth-session-status class="mb-4" :status="session('status')" />

      <form id="vnts-ap" method="POST" action="">
        @csrf
        <x-input-error class="mb-2 mt-2 text-center" :messages="$errors->get('email')" />

        <div>
          <x-input-label for="email" :value="__('Email')" />
          <x-text-input class="mt-1 block w-full" id="email" name="email" type="email" :value="old('email')"
            required autofocus autocomplete="email" />

        </div>

        <div class="mt-4">
          <x-input-label for="password" :value="__('Password')" />

          <x-text-input class="mt-1 block w-full" id="password" name="password" type="password" required required
            autocomplete="current-password" />
        </div>

        <div class="mt-4 block">
          <label class="inline-flex items-center" for="remember_me">
            <input
              class="rounded border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:border-gray-700 dark:bg-gray-900 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800"
              id="remember_me" name="remember" type="checkbox">
            <span class="ms-2 text-sm text-gray-600 dark:text-gray-400">{{ __('Remember me') }}</span>
          </label>
        </div>
        <div class="mt-4 flex items-center justify-end">

          <x-primary-button class="ms-3">
            <span>{{ __('Log in') }}</span>
          </x-primary-button>
        </div>
      </form>
    </div>
  </main>
  <script>
    $(document).ready(function() {
      $("#vnts-ap").validate({
        rules: {
          email: {
            required: true,
            email: true,
          },
          password: {
            required: true,
          }
        },
        messages: {
          email: {
            required: "This field is required",
          },
          password: {
            required: "This field is required",
          },
        },
        invalidHandler: function() {
          $("#submitBtn").html("<span>Envoyer</span>");
        },

        errorPlacement: function(error, element) {
          element.addClass("error_input");

          element.removeClass("focus:shadow-blue-600 focus:shadow-round-big");

          error.insertAfter(element);
        },
        success: function(label, element) {
          $(element).removeClass("error_input");

          $(element).addClass("focus:shadow-blue-600 focus:shadow-round-big");
        },
      });
      $("#submitBtn").click(function() {
        if ($("#vnts-ap").valid()) {
          // If the form is valid, change button content to the spinner icon
          $(this).html('<i class="bx bx-loader-alt animate-spin"></i>');
          // Disable the button to prevent multiple submissions
          $(this).prop("disabled", true);
          // Submit the form
          $("#vnts-ap").submit();
        }
      });
    });
  </script>
</body>

</html>
