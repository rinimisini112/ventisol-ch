@include('partials/_head')

<body class="" style="overflow:hidden;">
  @include('partials/_navbar')
  <header class="flex w-full bg-venti-blue bg-opacity-30">
    <div class="relative z-40 hidden w-1/5 bg-venti-blue lg:block"
      style="background-image: url('{{ asset('images/batthern.png') }}');">
    </div>
    <section class="relative w-full overflow-x-clip lg:w-4/5" id="advantages-section">
      <div
        style="background-image: url('{{ asset('images/jean-philippe-delberghe-75xPHEQBmvA-unsplash.png') }}'); background-size: cover; background-repeat: no-repeat;">
        <div
          class="absolute left-0 top-0 h-[400px] w-full bg-gradient-to-t from-transparent via-[#23588991] to-[#235789]"
          id="vent-blur"></div>
        <img class="opacity-85 relative -right-1/4 -top-0 w-3/4 -translate-y-1/4 object-cover" id="vent-duct"
          src="{{ asset('images/vent-duct.png') }}" alt="">
        <h1
          class="roboto mx-auto -mt-4 w-full px-4 text-center text-2xl font-normal tracking-wide md:-mt-16 md:text-4xl lg:-mt-20 lg:w-[90%] lg:px-0"
          id="vent-title">Ventilation <span class="roboto-bold">(ingénierie)</span>, processus consistant à <span
            class="roboto-medium text-venti-blue">’’changer,,</span> ou remplacer l'air dans un espace afin d'assurer
          une
          bonne qualité de l'air intérieur.</h1>
        <h2
          class="roboto-medium flex gap-4 px-4 pb-12 pt-6 text-3xl font-semibold text-venti-blue md:pl-16 md:pt-12 md:text-5xl lg:pl-20 lg:pt-12"
          id="vent-advantages-title">
          <div class="w-[6px] rounded-2xl bg-venti-blue"></div>
          Avantages
        </h2>
        <div class="cursor-default px-8 text-[20px] md:px-16 lg:w-[90%] lg:pl-20">
          <p class="roboto lg:w-3/5" id="vent-content-1">Dans la danse complexe de l'architecture et de l'ingénierie,
            la ventilation apparaît comme un maestro silencieux qui orchestre la qualité de l'air que nous respirons
            dans les espaces clos.
            espaces clos. Au-delà de son rôle fonctionnel, la ventilation est un art, une symphonie méticuleuse de flux
            d'air et d'harmonie environnementale.
            l'harmonie de l'environnement.
          </p>
          <p class='roboto pt-12 lg:ml-auto lg:w-3/5' id="vent-content-2">La ventilation est l'échange intentionnel et
            systématique
            systématique de l'air intérieur et extérieur à l'intérieur d'un espace. Son objectif premier est de
            maintenir et d'améliorer la qualité de l'air
            qualité de l'air intérieur en éliminant les polluants, en contrôlant l'humidité et en assurant un flux
            constant d'air frais. En
            En guidant stratégiquement les courants d'air, la ventilation contribue à un environnement de vie ou de
            travail confortable et sain.
            de vie ou de travail confortable et sain.
          </p>
          <h1 class="roboto-medium flex gap-4 pb-12 pt-12 text-3xl font-thin text-venti-blue md:text-5xl lg:pt-4"
            id="vent-benefits-title">
            <div class="w-[6px] rounded-2xl bg-venti-blue"></div>
            Bénéfices
          </h1>
          <ol id="vent-benefits-list">
            <li class="flex items-start gap-4 py-4 lg:items-center lg:p-5">
              <span class="roboto-bold text-6xl font-black text-venti-blue lg:text-9xl">1</span>
              <div>
                <span class="roboto-bold block text-2xl text-venti-blue">Amélioration de la qualité de l'air :</span>
                La ventilation agit comme un gardien de l'air intérieur, le débarrassant des impuretés telles que les
                allergènes, les polluants et les odeurs. Cela favorise une atmosphère propice au bien-être, à la
                productivité et à la concentration.
              </div>
            </li>
            <li class="flex items-start gap-4 py-4 lg:items-center lg:p-5">
              <span class="roboto-bold text-6xl font-black text-venti-blue lg:text-9xl">2</span>
              <div>
                <span class="roboto-bold block text-2xl text-venti-blue">Contrôle de la température :</span>
                Au-delà de la simple circulation de l'air, la ventilation joue un rôle essentiel dans la régulation de
                la température. Qu'il s'agisse de rafraîchir pendant les étés torrides ou de réchauffer pendant les
                hivers froids, un système de ventilation bien conçu assure un confort thermique optimal.
              </div>
            </li>
            <li class="flex items-start gap-4 py-4 lg:items-center lg:p-5">
              <span class="roboto-bold text-6xl font-black text-venti-blue lg:text-9xl">3</span>
              <div>
                <span class="roboto-bold block text-2xl text-venti-blue">Gestion de l'humidité : </span>
                L'humidité excessive peut favoriser l'apparition de moisissures et compromettre l'intégrité
                structurelle. Les systèmes de ventilation agissent comme des maîtres de l'humidité, empêchant
                l'accumulation d'humidité et protégeant contre les risques potentiels.
              </div>
            </li>
            <li class="flex items-start gap-4 py-4 lg:items-center lg:p-5">
              <span class="roboto-bold text-6xl font-black text-venti-blue lg:text-9xl">4</span>
              <div>
                <span class="roboto-bold block text-2xl text-venti-blue">Efficacité énergétique :</span>
                Une conception réfléchie de la ventilation ne se limite pas à l'échange d'air ; il s'agit de le faire de
                manière efficace. En intégrant des technologies à haut rendement énergétique, les systèmes de
                ventilation contribuent à la durabilité, en réduisant la consommation d'énergie et les coûts associés.
              </div>
            </li>
          </ol>
        </div>
        <div
          class="flex h-[200px] w-full items-end bg-gradient-to-b from-transparent via-[#235889b5] to-[#235789] md:h-[300px]">
        </div>
      </div>
      <div class="w-full bg-venti-blue" style="background-image: url('{{ asset('images/batthern.png') }}');">
        <div class="relative">
          <div
            class="absolute left-0 top-0 flex h-[200px] w-full items-end bg-gradient-to-t from-transparent via-[#235889b5] to-[#235789] md:h-[150px]">
          </div>
          <img class="relative z-10 mx-auto w-full -translate-y-1/4 object-cover" id="conditioner"
            src="{{ asset('images/proffesonal-hvac.png') }}" alt="">
          <div
            class="relative z-10 flex h-[200px] w-full -translate-y-3/4 items-center justify-center bg-gradient-to-b from-transparent via-[#235889] to-venti-blue md:h-[250px] md:-translate-y-[110%] lg:h-[200px] lg:-translate-y-[140%]">
            <h1
              class="roboto z-20 w-full px-0 pt-4 text-center text-lg leading-9 text-white md:px-16 md:text-4xl lg:w-[60%] lg:px-0"
              id="air-conditioner-title-inside">"The Business Advantage: Unveiling the Benefits of <span
                class="roboto-bold">Commercial Ventilation</span>"</h1>

          </div>
        </div>
        <div
          class="relative z-10 mx-auto -mt-4 -translate-y-[10%] px-4 text-xl text-white md:-translate-y-[20%] md:px-12 lg:w-3/4 lg:-translate-y-[10%] lg:px-4">
          <h1 class="roboto-medium flex gap-4 pb-12 pt-4 text-3xl font-thin text-white md:text-5xl"
            id="conditioner-advantages-title">
            <div class="w-[6px] rounded-2xl bg-white"></div>
            Advantages
          </h1>
          <p class="roboto" id="conditioner-content-1">Explorez l'avantage concurrentiel qu'un système CVC commercial
            bien conçu et géré par des professionnels apporte à votre entreprise. De l'amélioration de la productivité
            des employés à l'efficacité énergétique, découvrez comment l'investissement dans la ventilation commerciale
            est rentable.</p>
          <ul class="mt-16 flex flex-wrap items-center justify-between gap-y-12">
            <div
              class="benefitListItem w-[95%] skew-x-3 skew-y-3 bg-gradient-to-br from-white via-sky-300 to-white p-1 text-white md:w-[45%]">
              <div
                class="relative h-full w-full -skew-x-3 -skew-y-3 bg-gradient-to-tr from-venti-blue via-[#2c6da9] to-[#2f76b8] p-4 px-3 lg:px-10">
                <span class="roboto-bold absolute right-4 top-4 text-9xl opacity-30">01</span>
                <h3 class="roboto-bold pb-4 pt-16 text-2xl">Productivité accrue :</h3>
                <p class="text-md roboto text-blue-100">Un climat intérieur optimisé favorise un environnement de
                  travail productif,
                  et permet à votre équipe de travailler avec une efficacité maximale.</p>
              </div>
            </div>
            <div
              class="benefitListItem w-[95%] skew-x-3 skew-y-3 bg-gradient-to-bl from-white via-sky-300 to-white p-1 text-white md:w-[45%]">
              <div
                class="relative h-full w-full -skew-x-3 -skew-y-3 bg-gradient-to-bl from-venti-blue via-[#2c6da9] to-[#2f76b8] p-4 px-3 lg:px-10">
                <span class="roboto-bold absolute right-4 top-4 text-9xl opacity-30">02</span>
                <h3 class="roboto-bold pb-4 pt-16 text-2xl">Efficacité énergétique :</h3>
                <p class="text-md roboto text-blue-100"> Les systèmes CVC commerciaux de pointe ne se contentent pas
                  d'assurer le confort
                  mais contribuent également à des économies d'énergie substantielles, réduisant ainsi les coûts
                  d'exploitation.</p>
              </div>
            </div>
            <div
              class="benefitListItem w-[95%] skew-x-3 skew-y-3 bg-gradient-to-t from-white via-sky-300 to-white p-1 text-white md:w-[45%]">
              <div
                class="relative h-full w-full -skew-x-3 -skew-y-3 bg-gradient-to-t from-venti-blue via-[#2c6da9] to-[#2f76b8] p-4 px-3 lg:px-10">
                <span class="roboto-bold absolute right-4 top-4 text-9xl opacity-30">03</span>
                <h3 class="roboto-bold pb-4 pt-16 text-2xl">Opérations durables :</h3>
                <p class="text-md roboto text-blue-100">Adoptez des pratiques respectueuses de l'environnement avec des
                  solutions
                  et démontrez ainsi votre engagement en faveur de la durabilité.</p>
              </div>
            </div>
            <div
              class="benefitListItem w-[95%] skew-x-3 skew-y-3 bg-gradient-to-b from-white via-sky-300 to-white p-1 text-white md:w-[45%]">
              <div
                class="relative h-full w-full -skew-x-3 -skew-y-3 bg-gradient-to-b from-venti-blue via-[#2c6da9] to-[#2f76b8] p-4 px-3 lg:px-10">
                <span class="roboto-bold absolute right-4 top-4 text-9xl opacity-30">04</span>
                <h3 class="roboto-bold pb-4 pt-16 text-2xl">Longévité de l'équipement :</h3>
                <p class="text-md roboto text-blue-100">Un entretien régulier et des soins spécialisés prolongent la
                  durée de vie de votre équipement de CVC.
                  HVAC, offrant ainsi une solution fiable et rentable à long terme.</p>
              </div>
            </div>
          </ul>
        </div>
        <div
          class="relative z-10 flex h-[100px] w-full items-start bg-gradient-to-b from-[#23588958] via-[#235889de] to-[#235789] lg:h-[200px]">
        </div>
      </div>
      <div>
        <div class="w-full text-venti-blue" id="maintenance">
          <div
            style="background-image: url('{{ asset('images/jean-philippe-delberghe-75xPHEQBmvA-unsplash.png') }}'); background-size: cover; background-repeat: no-repeat;">
            <div
              class="relative z-10 flex h-[250px] w-full items-start bg-gradient-to-t from-transparent via-[#235889fd] to-[#235789] lg:h-[200px]">
              <h1
                class="roboto-bold absolute left-1/2 top-0 z-10 -mt-6 -translate-x-1/2 -translate-y-1/2 py-3 text-center text-5xl text-white md:text-6xl"
                id="maintenance-title">Entretien</h1>
            </div>
            <div class="-translate-y-20" style="background-image: url('{{ asset('images/worn-dots.png') }}');">
              <img class="relative aspect-auto object-cover" id="maintenance-img"
                src="{{ asset('images/conditioner-maintenance.png') }}" alt="">
              <h2
                class="roboto-bold relative z-10 -mt-14 -skew-y-[5deg] bg-venti-blue py-8 text-center text-2xl text-white md:text-4xl"
                id="maintenance-why-title" style="background-image: url('{{ asset('images/worn-dots.png') }}');">
                Pourquoi dois-je
                dois-je entretenir mon appareil de ventilation ?</h2>
            </div>
            <div class="relative z-0 w-full">
              <div class="gallery relative z-0 h-full">
                <div class="panel flex w-full flex-col gap-1 bg-venti-blue py-6 lg:gap-8"
                  style="background-image: url('{{ asset('images/worn-dots.png') }}');">
                  <h1
                    class="horizontal-scroll-title roboto-bold flex h-[15%] items-center justify-center text-2xl text-white lg:text-5xl">
                    Soutenir la continuité des activités</h1>
                  <div class="flex h-[90%] w-full flex-col items-center lg:flex-row">
                    <a class="inline-block h-[80%] w-full lg:w-1/2"
                      data-caption="The Impact of Professional Commercial HVAC Maintenance"
                      href="{{ asset('images/hvac-maintenance-1.jpg') }}">
                      <img class="horizontal-scroll-image h-full w-full cursor-zoom-in object-cover lg:aspect-auto"
                        src="{{ asset('images/hvac-maintenance-1.jpg') }}" alt="">
                    </a>
                    <div class="horizontal-scroll-text w-full px-4 text-white lg:w-1/2">

                      <p class="pt-3 text-lg">Dans le monde rapide des opérations commerciales, le maintien d'un
                        environnement intérieur confortable et contrôlé est primordial.
                        d'un environnement intérieur confortable et contrôlé est primordial. Nos services professionnels
                        de ventilation
                        commerciaux professionnels sont conçus pour garantir que votre entreprise ne perde pas de temps.
                      </p>
                      <p class="pt-3 text-lg">
                        Qu'il s'agisse d'espaces
                        des espaces de vente au détail aux complexes de bureaux, nous comprenons les exigences uniques
                        des environnements commerciaux. Notre équipe
                        Notre équipe de maintenance dévouée travaille avec diligence pour prévenir les pannes
                        inattendues, garantissant un fonctionnement ininterrompu et préservant la continuité de votre
                        entreprise.
                        des opérations ininterrompues et de préserver la continuité de votre activité.</p>
                    </div>
                  </div>
                </div>
                <div
                  class="panel flex flex-col gap-1 bg-white bg-opacity-75 py-6 text-venti-blue backdrop-blur-md backdrop-opacity-60 lg:gap-8"
                  id="panel-1" style="background-image: url('{{ asset('images/worn-dots.png') }}');">
                  <h1
                    class="horizontal-scroll-title roboto-bold flex h-[15%] items-center justify-center text-2xl lg:text-5xl">
                    L'efficacité est synonyme d'économies</h1>
                  <div class="flex h-[90%] w-full flex-col items-center lg:flex-row">
                    <a class="inline-block h-[80%] w-full lg:w-3/5"
                      data-caption="The Financial Advantages of Regular Maintenance"
                      href="{{ asset('images/hvac-maintenance-2.jpg') }}">
                      <img class="horizontal-scroll-image h-full w-full cursor-zoom-in object-cover lg:aspect-auto"
                        src="{{ asset('images/hvac-maintenance-2.jpg') }}" alt="">
                    </a>
                    <div class="horizontal-scroll-text w-full px-4 lg:w-2/5">
                      <p class="pt-3 text-lg text-black">Optimisez vos résultats en investissant dans l'efficacité de
                        votre système de
                        votre système CVC commercial. Nos services d'entretien professionnels sont axés sur
                        l'optimisation
                        de l'énergie, ce qui permet de réduire les coûts d'exploitation. Un système CVC bien entretenu
                        fonctionne plus efficacement, consomme moins d'énergie et offre un confort constant.
                        un système CVC bien entretenu fonctionne plus efficacement, consomme moins d'énergie tout en
                        offrant un confort constant.
                      </p>
                      <p class="pt-3 text-lg text-black">
                        Grâce à des inspections
                        inspections, nettoyages et ajustements de routine, nous vous aidons à économiser sur vos
                        factures d'électricité, ce qui permet à votre entreprise d'allouer ses ressources là où elles
                        sont le plus importantes.
                        d'allouer les ressources là où elles sont les plus importantes.</p>
                    </div>
                  </div>
                </div>
                <div class="panel flex flex-col items-center justify-center gap-6 bg-venti-blue py-6" id="panel-2"
                  style="background-image: url('{{ asset('images/worn-dots.png') }}');">
                  <h1 class="roboto-medium text-4xl text-white">Vidéo d'information</h1>
                  <iframe src="https://www.youtube.com/embed/X77oxKsQ3H4"
                    title="Comment choisir une unité de traitement d&#39;air? (FR)" width="853" height="480"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <div>
              <h1 class="px-4 pt-28 text-xl font-black leading-loose lg:px-20 lg:text-3xl">"Veillez à ce que votre
                système de
                fonctionne de manière fiable. Faites appel à des professionnels du chauffage, de la ventilation et de la
                climatisation et prévoyez des contrôles réguliers. Agissez maintenant, appelez-nous
                pour une maintenance proactive de votre système de chauffage, de ventilation et de climatisation.</h1>
              <div class="flex w-full items-center justify-center pb-16 pt-4">
                <a class="rounded-3xl bg-venti-blue px-8 py-2 text-3xl text-white duration-200 ease-in-out hover:bg-white hover:text-venti-blue hover:ring hover:ring-venti-blue active:scale-90 active:opacity-70"
                  href="{{ route('contact') }}"
                  style="background-image:url('{{ asset('images/white-diamond.png') }}')">Nous contacter</a>
              </div>
            </div>
          </div>
        </div>
    </section>
  </header>
  @include('partials._footer')
  <script src="{{ asset('js/advantages.js') }}" defer></script>
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      var queue = new createjs.LoadQueue();

      queue.loadManifest([{
          id: "vent-duct",
          src: "resources/images/vent-duct.png",
          priority: 2,
        },
        {
          id: "conditioner",
          src: "resources/images/air-duct-two.png",
          priority: 1
        },
        {
          id: "air",
          src: "resources/images/air.png",
          priority: 0
        },
      ]);

      queue.addEventListener("complete", handleComplete);

      function handleComplete() {
        console.log("Welcome developers ;)");
      }
    });
  </script>
</body>

</html>
