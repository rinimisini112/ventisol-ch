@include('partials._head')

<body class="flex h-screen w-full items-center justify-center overflow-clip bg-venti-blue bg-opacity-20">
  <div class="flex flex-col items-center px-4 md:px-0 lg:flex-shrink-0">
    <p class="roboto-medium text-xl md:text-3xl">Nous sommes désolés de ne pas avoir trouvé ce que vous cherchiez.</p>
    <img class="mt-8 w-2/5 md:mt-0" id="error-image" src="{{ asset('SVG/404.svg') }}" srcset="" alt="">
    <p class="roboto pt-4 text-xl md:pt-0 md:text-3xl">Voici quelques liens qui vous ramèneront en arrière</p>
    <div class="flex items-center gap-8 pt-2 text-2xl">
      <a class="text-blue-500 underline" href="{{ route('index') }}">Homepage</a>
      <a class="text-blue-500 underline" href="{{ route('contact') }}">Contact</a>
    </div>
  </div>
</body>

</html>
