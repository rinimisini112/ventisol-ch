<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AdminPasswordVerified
{
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('password_verified')) {
            return $next($request);
        }

        return redirect()->route('admin.verify')->with('error', 'Please verify your password first.');
    }
}
