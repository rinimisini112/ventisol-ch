<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AdminDataMiddleware
{
    public function handle($request, Closure $next)
    {

        if (Auth::guard('admin')) {
            $messageNotifications = Contact::orderBy('created_at', 'desc')->get();
            $unreadMessages = Contact::where('is_viewed', 0)->count();


            View::share('messageNotifications', $messageNotifications);
            View::share('unreadMessages', $unreadMessages);
        }
        return $next($request);
    }
}
