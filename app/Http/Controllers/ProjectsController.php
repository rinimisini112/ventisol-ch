<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectsController extends Controller
{
    public function index()
    {
        $projects = Project::orderBy('finished_at', 'desc')->take(3)->get();
        $references = Client::all();

        return view('public_projects.index', [
            'projects' => $projects,
            'references' => $references
        ]);
    }

    public function showAll()
    {

        $projects = Project::orderBy('finished_at', 'desc')->simplePaginate(5);

        return view('public_projects.all', ['projects' => $projects]);
    }

    public function show($id)
    {
        $project = Project::find($id);

        if (!$project) {
            abort(404);
        }

        return view('public_projects.show', ['project' => $project]);
    }
    public function search(Request $request)
    {
        $searchQuery = $request->input('q');

        $searchResults = Project::when($searchQuery, function ($query) use ($searchQuery) {
            return $query->where('title', 'like', '%' . $searchQuery . '%')
                ->orWhere('location', 'like', '%' . $searchQuery . '%')
                ->orWhere('company', 'like', '%' . $searchQuery . '%');
        })->simplePaginate(5);
        return view('public_projects.search', ['projects' => $searchResults, 'searchQuery' => $searchQuery]);
    }
}
