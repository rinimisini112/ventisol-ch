<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MaintenanceController extends Controller
{
    /**
     * Return the maintenance view
     *
     * @return void
     */
    public function index()
    {
        return \view('/maintenance');
    }
}
