<?php

namespace App\Http\Controllers;

use App\Models\Issue;
use App\Models\Contact;
use Illuminate\Http\Request;

class IssueController extends Controller
{

    public function index()
    {

        $issues = Issue::orderBy('id', 'desc')->paginate(5);

        return \view('admin.admin_issues.index', \compact('issues'));
    }

    public function create()
    {

        return \view('admin.admin_issues.create');
    }


    public function store(Request $request)
    {
        Issue::store($request);

        return redirect()->route('issues.index')->with('success', 'Form submited succesfully, New Issue just created!');
    }


    public function show(string $id)
    {
        $issue = Issue::find($id);

        return \view('admin.admin_issues.show', \compact('issue'));
    }

    public function edit(string $id)
    {
        $issue = Issue::find($id);

        return \view('admin.admin_issues.edit', \compact('issue'));
    }

    public function update(Request $request, string $id)
    {
        Issue::updateIssue($request, $id);

        return redirect()->route('issues.show', [$id])->with('success', 'Issue updated successfully');
    }
}
