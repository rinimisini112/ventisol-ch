<?php

namespace App\Http\Controllers;

use App\Mail\ContactNotification;
use App\Mail\ContactRecieved;
use App\Models\Issue;
use App\Models\Region;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{

    public function index()
    {
        $issues = Issue::all();
        $regions = Region::all();

        return \view('contact', \compact('issues', 'regions'));
    }


    public function store(Request $request)
    {
        try {
            Contact::store($request);

            return redirect()->route('contact')->with('success', 'Message envoyé avec succès, nous vous répondrons dès que possible !');
        } catch (\Exception $e) {

            return redirect()->route('contact')->with('error', "Nous n'avons pas pu traiter votre demande pour le moment. Veuillez nous contacter par téléphone ou par e-mail.");
        }
    }
}
