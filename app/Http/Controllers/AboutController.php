<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    // Return the index view of about us
    public function index()
    {
        return \view('about.index');
    }
    // Return the installation information page
    public function installation()
    {
        return \view('about.installation');
    }
    // Return the production information page
    public function production()
    {
        return \view('about.production');
    }
}
