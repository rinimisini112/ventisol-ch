<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Contact;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $clients = Client::all();

        $messageNotifications = Contact::orderBy('created_at', 'desc')->get();

        $unreadMessages = Contact::where('is_viewed', 0)->get()->count();

        return \view('admin.admin_clients.index', \compact('clients', 'unreadMessages', 'messageNotifications'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $messageNotifications = Contact::orderBy('created_at', 'desc')->get();

        $unreadMessages = Contact::where('is_viewed', 0)->get()->count();

        return \view('admin.admin_clients.create', \compact('messageNotifications', 'unreadMessages'));
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $client = Client::find($id);

        $messageNotifications = Contact::orderBy('created_at', 'desc')->get();

        $unreadMessages = Contact::where('is_viewed', 0)->get()->count();

        return \view('admin.admin_clients.show', \compact('client', 'unreadMessages', 'messageNotifications'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $client = Client::find($id);

        $messageNotifications = Contact::orderBy('created_at', 'desc')->get();

        $unreadMessages = Contact::where('is_viewed', 0)->get()->count();

        return \view('admin.admin_clients.edit', \compact('client', 'unreadMessages', 'messageNotifications'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
