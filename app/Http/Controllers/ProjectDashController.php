<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectDashController extends Controller
{

    public function index()
    {

        return \view('admin.admin_projects.index');
    }

    public function create()
    {
        return \view('admin.admin_projects.create');
    }

    public function show(string $id)
    {
        $project = Project::find($id);

        return \view('admin.admin_projects.show', \compact('project'));
    }

    public function edit(string $id)
    {
        $project = Project::find($id);

        return \view('admin.admin_projects.edit', \compact('project'));
    }
}
