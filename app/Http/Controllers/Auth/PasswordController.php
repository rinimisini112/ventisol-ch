<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\Rules\Password;

class PasswordController extends Controller
{
    public function confirm()
    {
        return view('admin.profile.confirm_password');
    }

    /**
     * Verifies the password before allowing access to change password
     *
     * @param  mixed $request
     * @return void
     */
    public function checkPassword(Request $request)
    {
        $user = Auth::user();

        $credentials = [
            'password' => $request->input('password'),
        ];

        if (Hash::check($credentials['password'], $user->password)) {

            session(['password_verified' => true]);

            return redirect()->route('admin.change_password');
        } else {
            return redirect()->route('admin.password')->with('error', 'Incorrect password, try again');
        }
    }

    public function edit()
    {
        return view('admin.profile.change_password');
    }

    public function update(Request $request): RedirectResponse
    {
        $user = $request->user();

        $validated = $request->validateWithBag('updatePassword', [
            'current_password' => ['required', 'current_password'],
            'password' => [
                'required',
                Password::defaults(),
                'confirmed',
                Rule::notIn([$user->password])
            ],
        ]);

        $user->update([
            'password' => Hash::make($validated['password']),
        ]);

        return back()->with('success', 'Password updated successfully');
    }
}
