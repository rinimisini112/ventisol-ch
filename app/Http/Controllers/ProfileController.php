<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function show(): View
    {
        $user = auth()->user();

        return view('admin.profile.profile', [
            'admin' => $user
        ]);
    }
    public function edit(): View
    {
        $user = auth()->user();

        return view('admin.profile.edit', [
            'admin' => $user
        ]);
    }
    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        $request->user()->save();

        return Redirect::route('admin.profile')->with('success', 'Profile Updated Succesfully.');
    }
}
