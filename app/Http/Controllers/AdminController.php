<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Contracts\View\View as ViewView;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Analytics\Facades\Analytics;
use Illuminate\Database\Eloquent\Collection;

class AdminController extends Controller
{

    public function index(Request $request): mixed
    {
        /*  \dd(\session()); */

        $timePeriod = $request->input('timePeriod', 7);

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'timePeriod' => 'required|in:7,30,180',
            ]);

            return redirect()->route('admin.dashboard', compact('timePeriod'))->withInput();
        }

        $newVsReturningUsers = Analytics::fetchUserTypes(Period::days($timePeriod));

        $analyticsData = Analytics::fetchVisitorsAndPageViewsByDate(Period::days($timePeriod));

        $countries = Analytics::fetchTopCountries(Period::days($timePeriod), $maxResults = 7);

        $getTotalVisitors = Analytics::fetchVisitorsAndPageViewsByDate(Period::days($timePeriod));

        $totalVisitors = $getTotalVisitors->transform(function ($singleData) {
            return $singleData['activeUsers'];
        })->sum();

        $mostViewedAll = Analytics::fetchMostVisitedPages(Period::days($timePeriod));

        $pagesData = $mostViewedAll->take(5);

        $pagesData->transform(function ($singlePage) {
            $shortenedUrl = str_replace('ventisol-ver1.test/', '', $singlePage['fullPageUrl']);
            if (str_contains($shortenedUrl, '') && $shortenedUrl === '') {
                $shortenedUrl .= 'home';
            }
            $singlePage['fullPageUrl'] = $shortenedUrl;

            return $singlePage;
        });

        return view('admin.dashboard', compact('analyticsData', 'pagesData', 'countries', 'totalVisitors', 'newVsReturningUsers', 'timePeriod'));
    }

    public function messagesIndex()
    {

        return \view('admin.messages.index');
    }

    /**
     * Updates the is viewed field in database 
     * when the user views the specific message
     * 
     * @param  string $id
     * @return View
     */
    public function messageGet(string $id): View
    {
        $contact = Contact::with('region', 'issue')->findOrFail($id);

        if (!$contact->is_viewed) {
            $contact->update(['is_viewed' => true]);
        }


        if (!$contact) {
            abort(404, 'Contact not found');
        }

        return view('admin.messages.get', compact('contact'));
    }
}
