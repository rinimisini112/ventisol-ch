<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{

    public function index()
    {
        $regions = Region::orderBy('id', 'desc')->paginate(5);

        return \view('admin.admin_regions.index', \compact('regions'));
    }

    public function create()
    {

        return \view('admin.admin_regions.create');
    }

    public function store(Request $request)
    {
        Region::store($request);

        return redirect()->route('regions.index')->with('success', 'Form submited succesfully, New Region just created!');
    }

    public function show(string $id)
    {
        $region = Region::find($id);

        return \view('admin.admin_regions.show', \compact('region'));
    }

    public function edit(string $id)
    {
        $region = Region::find($id);

        return \view('admin.admin_regions.edit', \compact('region'));
    }

    public function update(Request $request, string $id)
    {
        Region::updateRegion($request, $id);

        return redirect()->route('regions.show', [$id])->with('success', 'Region updated successfully');
    }
}
