<?php

namespace App\Livewire;

use App\Models\Client;
use App\Models\Issue;
use App\Models\Region;
use Livewire\Component;

class ConfirmIssueDeletion extends Component
{
    public $issueId;

    protected $listeners = ['confirmDelete'];

    public function mount($issueId)
    {
        $this->issueId = $issueId;
    }

    public function render()
    {
        return view('livewire.confirm-issue-deletion');
    }

    public function confirmDelete()
    {
        $this->dispatch('openDeleteConfirmationModal');
    }

    public function delete()
    {
        $issue = Issue::find($this->issueId);

        if ($issue) {
            $issue->delete();
            session()->flash('success', 'Issue deleted successfully.');
        } else {
            session()->flash('error', 'Issue not found.');
        }

        $this->dispatch('closeDeleteConfirmationModal');

        return redirect()->to('/dashboard/issues');
    }
}
