<?php

namespace App\Livewire;

use App\Models\Project;
use Livewire\Component;
use Livewire\WithPagination;

class ProjectSearch extends Component
{
    use WithPagination;

    public $search = '';


    public function render()
    {

        $projects = Project::where(function ($q) {
            $q->where('title', 'like', '%' . $this->search . '%')
                ->orWhere('location', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%')
                ->orWhere('content', 'like', '%' . $this->search . '%')
                ->orWhere('company_url', 'like', '%' . $this->search . '%');
        })->orderBy('created_at', 'desc')->paginate(5);

        return view('livewire.project-search', compact('projects'));
    }
}
