<?php

namespace App\Livewire;

use App\Models\Contact;
use Livewire\Component;
use Livewire\WithPagination;

class MessageContactSearch extends Component
{
    use WithPagination;

    public $search = '';
    public $viewedFilter = '2';


    public function applyFilter()
    {
        $this->resetPage();
    }
    public function render()
    {
        $query = Contact::with(['region', 'issue']);

        if ($this->viewedFilter != '2') {
            $query->where('is_viewed', $this->viewedFilter);
        }


        $messages = $query->where(function ($q) {
            $q->where('first_name', 'like', '%' . $this->search . '%')
                ->orWhere('last_name', 'like', '%' . $this->search . '%')
                ->orWhere('email', 'like', '%' . $this->search . '%')
                ->orWhere('phone_number', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%');
        })->paginate(10);

        return view('livewire.message-contact-search', compact('messages'));
    }
}
