<?php

namespace App\Livewire;

use Livewire\Component;

class ProfileDropdown extends Component
{
    public $profileIsOpen = false;

    public function toggleDropdown()
    {
        $this->profileIsOpen = !$this->profileIsOpen;
    }

    public function render()
    {
        return view('livewire.profile-dropdown');
    }
}
