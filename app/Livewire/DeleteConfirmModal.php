<?php

namespace App\Livewire;

use App\Models\Project;
use Livewire\Component;

class DeleteConfirmModal extends Component
{
    public $projectId;

    protected $listeners = ['confirmDelete'];

    public function mount($projectId)
    {
        $this->projectId = $projectId;
    }

    public function render()
    {
        return view('livewire.delete-confirm-modal');
    }

    public function confirmDelete()
    {
        $this->dispatch('openDeleteConfirmationModal');
    }

    public function delete()
    {
        $project = Project::find($this->projectId);

        if ($project) {
            $project->delete();
            session()->flash('success', 'Project deleted successfully.');
        } else {
            session()->flash('error', 'Project not found.');
        }

        $this->dispatch('closeDeleteConfirmationModal');

        return redirect()->to('/dashboard/projects');
    }
}
