<?php

namespace App\Livewire;

use App\Models\Client;
use Livewire\Component;
use Livewire\WithPagination;

class ClientsSearchTable extends Component
{
    use WithPagination;

    public $search = '';


    public function render()
    {

        $clients = Client::where(function ($q) {
            $q->where('company_name', 'like', '%' . $this->search . '%')
                ->orWhere('company_website_url', 'like', '%' . $this->search . '%');
        })->orderBy('created_at', 'desc')->paginate(5);

        return view('livewire.clients-search-table', compact('clients'));
    }
}
