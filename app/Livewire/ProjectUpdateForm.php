<?php

namespace App\Livewire;

use App\Models\Project;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class ProjectUpdateForm extends Component
{
    use WithFileUploads;

    public $projectId;
    public $title;
    public $location;
    public $company;
    public $company_url;
    public $finished_at;
    public $content;
    public $image1;
    public $image2;
    public $image3;
    public $image4;
    public $image5;
    public $images_db;

    protected $rules = [
        'title' => 'required',
        'location' => 'required',
        'company' => 'required',
        'company_url' => 'required|url',
        'finished_at' => 'required|date',
        'content' => 'required',
    ];

    public function updated($propertyName)
    {
        if (Str::startsWith($propertyName, 'image')) {
            $this->validateOnly($propertyName);
        }
    }
    public function mount($projectId)
    {
        $this->projectId = $projectId;
        $project = Project::find($projectId);

        $this->title = $project->title;
        $this->location = $project->location;
        $this->company = $project->company;
        $this->company_url = $project->company_url;
        $this->finished_at = $project->finished_at;
        $this->content = $project->content;

        $this->images_db = \json_decode($project->images);
    }
    public function render()
    {
        return view('livewire.project-update-form');
    }

    public function update()
    {
        $this->validate($this->rules);

        $project = Project::find($this->projectId);


        $project->update([
            'title' => $this->title,
            'location' => $this->location,
            'company' => $this->company,
            'company_url' => $this->company_url,
            'finished_at' => $this->finished_at,
            'content' => $this->content,
        ]);

        $this->saveImage($this->image1, $project);
        $this->saveImage($this->image2, $project);
        $this->saveImage($this->image3, $project);
        $this->saveImage($this->image4, $project);
        $this->saveImage($this->image5, $project);

        session()->flash('success', 'Form submitted! Project edited successfully.');
        return redirect()->route('projects.show', ['project' => $this->projectId]);
    }

    private function saveImage($image, $project)
    {
        if ($image) {
            $path = $image->store('public/images');

            $images = json_decode($project->images, true) ?? [];

            $images[] = asset('storage/' . str_replace('public/', '', $path));

            $project->update(['images' => json_encode($images)]);
        }
    }
}
