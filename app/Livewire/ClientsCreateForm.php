<?php

namespace App\Livewire;

use App\Models\Client;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class ClientsCreateForm extends Component
{
    use WithFileUploads;


    public $company_name;
    public $company_website_url;
    public $company_logo;

    protected $rules = [
        'company_name' => 'required',
        'company_website_url' => 'sometimes|url',
        'company_logo' => 'required|image|mimes:jpg,jpeg,png,gif,webp|max:8000',
    ];

    public function updated($propertyName)
    {
        if (Str::startsWith($propertyName, 'image')) {
            $this->validateOnly($propertyName);
        }
    }

    public function render()
    {
        return view('livewire.clients-create-form');
    }

    public function store()
    {
        $this->validate($this->rules);

        $client = Client::create([
            'company_name' => $this->company_name,
            'company_website_url' => $this->company_website_url,
        ]);

        $this->saveImage($this->company_logo, $client);

        session()->flash('success', 'Form submitted! Reference / Client added successfully.');
        return redirect()->route('clients.index');
    }

    private function saveImage($image, $client)
    {
        if ($image) {
            $path = $image->store('public/images');
            $client->update(['company_logo' => asset('storage/' . str_replace('public/', '', $path))]);
        }
    }
}
