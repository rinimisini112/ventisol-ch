<?php

namespace App\Livewire;

use App\Models\Project;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Livewire\Attributes\Validate;

class ImagePreviewBox extends Component
{
    use WithFileUploads;


    public $title;
    public $location;
    public $company;
    public $company_url;
    public $finished_at;
    public $content;
    public $image1;
    public $image2;
    public $image3;
    public $image4;
    public $image5;

    protected $rules = [
        'title' => 'required',
        'location' => 'required',
        'company' => 'required',
        'company_url' => 'required|url',
        'finished_at' => 'required|date',
        'content' => 'required',
        'image1' => 'required|mimes:jpeg,png,jpg,gif,webp|max:8000',
        'image2' => 'required|mimes:jpeg,png,jpg,gif,webp|max:8000',
        'image3' => '',
        'image4' => '',
        'image5' => '',
    ];

    public function updated($propertyName)
    {
        if (Str::startsWith($propertyName, 'image')) {
            $this->validateOnly($propertyName);
        }
    }

    public function render()
    {
        return view('livewire.image-preview-box');
    }

    public function store()
    {
        $this->validate($this->rules);

        $project = Project::create([
            'title' => $this->title,
            'location' => $this->location,
            'company' => $this->company,
            'company_url' => $this->company_url,
            'finished_at' => $this->finished_at,
            'content' => $this->content,
        ]);

        $this->saveImage($this->image1, $project);
        $this->saveImage($this->image2, $project);
        $this->saveImage($this->image3, $project);
        $this->saveImage($this->image4, $project);
        $this->saveImage($this->image5, $project);

        session()->flash('success', 'Form submitted! Project added successfully.');
        return redirect()->route('projects.index');
    }

    private function saveImage($image, $project)
    {
        if ($image) {
            $path = $image->store('public/images');

            $images = json_decode($project->images, true) ?? [];

            $images[] = asset('storage/' . str_replace('public/', '', $path));

            $project->update(['images' => json_encode($images)]);
        }
    }
}
