<?php

namespace App\Livewire;

use App\Models\Client;
use App\Models\Region;
use Livewire\Component;

class ConfirmRegionDeletion extends Component
{
    public $regionId;

    protected $listeners = ['confirmDelete'];

    public function mount($regionId)
    {
        $this->regionId = $regionId;
    }

    public function render()
    {
        return view('livewire.confirm-region-deletion');
    }

    public function confirmDelete()
    {
        $this->dispatch('openDeleteConfirmationModal');
    }

    public function delete()
    {
        $region = Region::find($this->regionId);

        if ($region) {
            $region->delete();
            session()->flash('success', 'Region deleted successfully.');
        } else {
            session()->flash('error', 'Region not found.');
        }

        $this->dispatch('closeDeleteConfirmationModal');

        return redirect()->to('/dashboard/regions');
    }
}
