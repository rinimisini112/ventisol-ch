<?php

namespace App\Livewire;

use App\Models\Client;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class ClientsUpdateForm extends Component
{
    use WithFileUploads;

    public $clientId;
    public $company_name;
    public $company_website_url;
    public $company_logo;
    public $current_company_logo;

    protected $rules = [
        'company_name' => 'required',
        'company_website_url' => 'sometimes|url',
    ];

    public function updated($propertyName)
    {
        if (Str::startsWith($propertyName, 'image')) {
            $this->validateOnly($propertyName);
        }
    }

    public function mount($clientId)
    {
        $this->clientId = $clientId;
        $client = Client::find($clientId);

        $this->company_name = $client->company_name;
        $this->company_website_url = $client->company_website_url;
        $this->current_company_logo = $client->company_logo;
    }
    public function render()
    {
        return view('livewire.clients-update-form');
    }

    public function update()
    {
        $this->validate($this->rules);

        $client = Client::find($this->clientId);

        $client->update([
            'company_name' => $this->company_name,
            'company_website_url' => $this->company_website_url,
        ]);

        $this->saveImage($this->company_logo, $client);

        session()->flash('success', 'Form submitted! Reference edited successfully.');
        return redirect()->route('clients.show', ['client' => $this->clientId]);
    }

    private function saveImage($image, $client)
    {
        if ($image instanceof \Illuminate\Http\UploadedFile) {
            $path = $image->store('public/images');
            $client->update(['company_logo' => asset('storage/' . str_replace('public/', '', $path))]);
        }
    }
}
