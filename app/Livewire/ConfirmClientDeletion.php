<?php

namespace App\Livewire;

use App\Models\Client;
use Livewire\Component;

class ConfirmClientDeletion extends Component
{
    public $clientId;

    protected $listeners = ['confirmDelete'];

    public function mount($clientId)
    {
        $this->clientId = $clientId;
    }

    public function render()
    {
        return view('livewire.delete-confirm-modal');
    }

    public function confirmDelete()
    {
        $this->dispatch('openDeleteConfirmationModal');
    }

    public function delete()
    {
        $client = Client::find($this->clientId);

        if ($client) {
            $client->delete();
            session()->flash('success', 'Reference deleted successfully.');
        } else {
            session()->flash('error', 'Reference not found.');
        }

        $this->dispatch('closeDeleteConfirmationModal');

        return redirect()->to('/dashboard/clients');
    }
}
