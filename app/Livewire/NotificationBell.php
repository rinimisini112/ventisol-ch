<?php

namespace App\Livewire;

use App\Models\Contact;
use Livewire\Component;

class NotificationBell extends Component
{
    public $messageNotifications;
    public $unreadMessages = 0;
    public $isOpen = false;

    public function mount()
    {
        $this->messageNotifications = Contact::orderBy('created_at', 'desc')->get();
        $this->unreadMessages = Contact::where('is_viewed', 0)->count();
    }

    public function toggleNotifications()
    {
        $this->isOpen = !$this->isOpen;
    }

    public function render()
    {
        return view('livewire.notification-bell');
    }
}
