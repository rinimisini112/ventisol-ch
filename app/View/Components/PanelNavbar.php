<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PanelNavbar extends Component
{
    public int $messageNumber;
    public string $breadcrumbSlot = "";
    public string $breadcrumbSlotRoute = "";
    public string $breadcrumbFinal = "";
    /**
     * Create a new component instance.
     */
    public function __construct(int $messageNumber, string $breadcrumbSlotRoute, string $breadcrumbSlot, string $breadcrumbFinal)
    {
        $this->messageNumber = $messageNumber;
        $this->breadcrumbSlot = $breadcrumbSlot;
        $this->breadcrumbSlotRoute = $breadcrumbSlotRoute;
        $this->breadcrumbFinal = $breadcrumbFinal;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.panel-navbar');
    }
}
