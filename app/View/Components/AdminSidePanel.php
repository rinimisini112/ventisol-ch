<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class AdminSidePanel extends Component
{
    public string $activeLink;

    /**
     * Create a new component instance.
     */
    public function __construct(string $activeLink)
    {
        $this->activeLink = $activeLink;
    }
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.admin-side-panel');
    }
}
