<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SideNavLink extends Component
{
    public string $route;
    public string $section;
    public string $slot;
    public bool $isActive;

    /**
     * Create a new component instance.
     */
    public function __construct(string $route, string $section, string $slot, bool $isActive = false)
    {
        $this->route = $route;
        $this->section = $section;
        $this->slot = $slot;
        $this->isActive = $isActive;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.side-nav-link');
    }

    /**
     * Get the classes for the component.
     */
    public function classes(): string
    {
        $classes = 'mb-5 mt-5 flex w-full items-center justify-start pl-5 rounded-xl bg-venti-dark py-2 shadow-2xl duration-150 hover:bg-venti-dark-tint active:ring-2 active:ring-indigo-500';

        if ($this->isActive) {
            $classes .= ' bg-venti-dark-tint py-3';
        }

        return $classes;
    }
}
