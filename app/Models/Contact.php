<?php

namespace App\Models;

use App\Models\Issue;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Mail\ContactRecieved;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'issue_id', 'region_id', 'company', 'first_name', 'last_name', 'address',
        'postal_code', 'city', 'phone_number', 'email', 'subject', 'message', 'is_viewed'
    ];
    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function issue()
    {
        return $this->belongsTo(Issue::class, 'issue_id');
    }

    public function setPhoneNumberAttribute($value)
    {
        $this->attributes['phone_number'] = str_replace(' ', '', $value);
    }

    public static function store(Request $request)
    {
        $form_fields = $request->validate([
            "issue" => 'required',
            "region" => 'required',
            "company" => 'nullable|string',
            "name" => 'required',
            "last_name" => 'required',
            "address" => 'required',
            "postal_code" => 'required',
            "city" => 'required',
            "telephone" => 'required',
            "email" => 'required',
            "subject" => 'required',
            "message" => 'required',
        ]);

        $contact = new Contact([
            "issue_id" => $request->input('issue'),
            "region_id" => $request->input('region'),
            "company" => $request->input('company'),
            "first_name" => $request->input('name'),
            "last_name" => $request->input('last_name'),
            "address" => $request->input('address'),
            "postal_code" => $request->input('postal_code'),
            "city" => $request->input('city'),
            "phone_number" => $request->input('telephone'),
            "email" => $request->input('email'),
            "subject" => $request->input('subject'),
            "message" => $request->input('message'),
        ]);
        $contact->save();

        Mail::to('misinirini@gmail.com')->send(new ContactRecieved($contact));
    }
}
