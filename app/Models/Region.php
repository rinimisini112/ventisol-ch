<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

class Region extends Model
{
    protected $fillable = [
        'region_short',
        'region_long'
    ];

    public $timestamps = false;
    public function contact(): HasOne
    {
        return $this->hasOne(Contact::class);
    }

    public static function store(Request $request)
    {
        $request->validate(
            [
                'region_short' => 'required',
                'region_long' => 'required',
            ]
        );
        $region = new Region([
            'region_short' => $request->input('region_short'),
            'region_long' => $request->input('region_long')
        ]);
        $region->save();
    }

    public static function updateRegion(Request $request, string $id)
    {
        $region = Region::find($id);

        $request->validate([
            'region_short' => 'required',
            'region_long' => 'required',
        ]);

        $region->update([
            'region_short' => $request->input('region_short'),
            'region_long' => $request->input('region_long'),
        ]);
    }
}
