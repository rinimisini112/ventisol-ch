<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

class Issue extends Model
{
    protected $fillable = ['issue_name'];
    public $timestamps = false;

    /**
     * Establish one to one relation with Contact
     *
     * @return HasOne
     */
    public function contact(): HasOne
    {
        return $this->hasOne(Contact::class);
    }

    public static function store(Request $request)
    {
        $request->validate(
            [
                'issue_name' => 'required',
            ]
        );
        $issue = new Issue([
            'issue_name' => $request->input('issue_name'),
        ]);

        $issue->save();
    }

    public static function updateIssue(Request $request, string $id)
    {
        $issue = Issue::find($id);

        $request->validate([
            'issue_name' => 'required',
        ]);

        $issue->update([
            'issue_name' => $request->input('issue_name'),
        ]);

        return true;
    }
}
