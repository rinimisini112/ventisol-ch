<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdvantagesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('index');
Route::get('/advantages', [AdvantagesController::class, 'index'])->name('advantages');
Route::get('/projects', [ProjectsController::class, 'index'])->name('projects.main');
Route::get('/projects/all', [ProjectsController::class, 'showAll'])->name('projects.all');
Route::get('/project/{id}', [ProjectsController::class, 'show'])->name('projects.show');

Route::get('/about', [AboutController::class, 'index'])->name('about');
Route::get('/about/production', [AboutController::class, 'production'])->name('about.production');
Route::get('/about/installation', [AboutController::class, 'installation'])->name('about.installation');
Route::get('/contact', [ContactsController::class, 'index'])->name('contact');
Route::post('/contact', [ContactsController::class, 'store'])->name('submit.contact');

Route::get(
    '/dashboard',
    [AdminController::class, 'index']
)->middleware(['auth', 'verified'])->name('dashboard');



require __DIR__ . '/auth.php';
