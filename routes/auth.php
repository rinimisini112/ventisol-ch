<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\IssueController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectDashController;
use App\Http\Controllers\RegionController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {

    Route::get('/panel', [AuthenticatedSessionController::class, 'create'])->name('admin.create');
    Route::post('/panel', [AuthenticatedSessionController::class, 'store'])->name('admin.store');
});

Route::middleware('auth:admin')->group(function () {

    Route::get('/dashboard', [AdminController::class, 'index'])->name('admin.dashboard')->middleware('auth:admin');
    Route::post('/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

    Route::get('/dashboard/profile', [ProfileController::class, 'show'])->name('admin.profile');
    Route::get('/dashboard/profile/edit', [ProfileController::class, 'edit'])->name('admin.edit');
    Route::put('/dashboard/profile/edit', [ProfileController::class, 'update'])->name('admin.update');
    Route::get('/dashboard/profile/password', [PasswordController::class, 'confirm'])->name('admin.password');

    Route::post('/dashboard/profile/password', [PasswordController::class, 'checkPassword'])->name('admin.verify');

    Route::middleware(['admin.password_verified'])->group(function () {
        Route::get('/dashboard/profile/password/change', [PasswordController::class, 'edit'])->name('admin.change_password');
        Route::put('/dashboard/profile/password/change', [PasswordController::class, 'update'])->name('admin.finalize_password');
    });

    Route::get('/dashboard/messages', [AdminController::class, 'messagesIndex'])->name('admin.messages');
    Route::get('/dashboard/message/{id}', [AdminController::class, 'messageGet'])->name('admin.message');

    Route::resource('/dashboard/projects', ProjectDashController::class)->except([
        'store', 'update', 'destroy'
    ]);
    Route::resource('/dashboard/clients', ClientController::class)->except([
        'store', 'update', 'destroy'
    ]);
    Route::resource('/dashboard/regions', RegionController::class)->except([
        'destroy'
    ]);
    Route::resource('/dashboard/issues', IssueController::class)->except([
        'destroy'
    ]);
    Route::post('/dashboard/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
});
