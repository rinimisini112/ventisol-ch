import defaultTheme from "tailwindcss/defaultTheme";
import forms from "@tailwindcss/forms";

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
    "./storage/framework/views/*.php",
    "./resources/views/**/*.blade.php",
  ],

  theme: {
    extend: {
      fontFamily: {
        sans: ["Figtree", ...defaultTheme.fontFamily.sans],
      },
      spacing: {
        "9/10": "90%",
        "3/10": "30%",
      },
      colors: {
        "venti-ash-gray": "#BBCBCB",
        "venti-teal-blue": "#9AC6C5",
        "venti-blue": "#235789",
        "venti-navy-blue": "#03045e",
        "venti-black": "#21222c",
        "venti-dark": "#2f3141",
        "venti-dark-tint": "#454862",
        "venti-dark-compliment": "#2c2b21",
      },
      boxShadow: {
        round: "0 0 6px ",
        "round-big": "1px 1px 8px ",
      },
    },
  },

  plugins: [forms],
};
